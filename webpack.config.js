require('dotenv').config();

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const dev = process.env.NODE_ENV !== 'production';
const UAT = process.env.UAT;

const devEntries = dev ?
  [
    'babel-polyfill',
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:8008',
    'webpack/hot/only-dev-server'
  ]
  :
  ['babel-polyfill'];

// global variables in modules
const globals = {
  'process.env.NODE_ENV': dev ? '"development"' : '"production"',
  __DEV__: dev,
  'process.env.ROLLBAR_TOKEN': JSON.stringify(process.env.ROLLBAR_TOKEN)
};

const cssModulesLoader = dev ?
  [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        modules: true,
        importLoaders: 1,
        localIdentName: '[name]__[local]___[hash:base64:4]'
      }
    },
    'postcss-loader'
  ]
  :
  ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          modules: true,
          importLoaders: 1,
          localIdentName: '[name]__[local]___[hash:base64:4]'
        }
      },
      'postcss-loader'
    ]
  });

const cssLoader = dev ?
  ['style-loader', 'css-loader']
  :
  ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: 'css-loader'
  });

const devPlugins = [
  new webpack.DefinePlugin(Object.assign({}, globals, { 'process.env.BROWSER': true })),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin(),
  new webpack.ProvidePlugin({
    Rollbar: [require.resolve('./vendor/rollbar/init.js'), 'default']
  })
  // prints more readable module names in the browser console on HMR updates
];

const buildPlugins = [
  new webpack.ProvidePlugin({
    Rollbar: [require.resolve('./vendor/rollbar/init.js'), 'default']
  }),
  new webpack.DefinePlugin(Object.assign({}, globals, {
    'process.env': {
      BROWSER: true,
      // This has effect on the react lib size
      NODE_ENV: JSON.stringify('production'),
    },
  })),
  new webpack.optimize.UglifyJsPlugin({
    sourceMap: true
  }),
  new webpack.optimize.AggressiveMergingPlugin(),
  new ExtractTextPlugin('styles/styles.css')
];

module.exports = {
  entry: []
    .concat(devEntries)
    .concat('./app/index.js'),
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  externals: {
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['react', 'es2015', 'stage-1'],
            plugins: ['transform-decorators-legacy']
          }
        }
      },
      {
        test: /\.css$/,
        include: /(node_modules|style)/,
        use: cssLoader
      },
      {
        test: /\.css$/,
        include: /app/,
        use: cssModulesLoader
      },
      {
        test: /\.(jpg|png|svg)$/,
        exclude: /node_modules/,
        use: {
          loader: 'url-loader',
          options: {
            name: '[path][name].[hash].[ext]'
          }
        }
      }
    ]
  },
  plugins: dev ? devPlugins : buildPlugins,
  resolve: {
    extensions: ['.js', '.jsx', '.json']
  },
  devServer: {
    historyApiFallback: {
      rewrites: [
        {
          from: /^\/login\/.*$/,
          to: function () {
            return 'index.html';
          }
        }
      ]
    },
    contentBase: './',
    hot: true,
    inline: true,
    port: 8008
  }
};
