import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercise,
  resetCurrentExercise,
  updateCurrentSettings,
  showOverlay,
  updateTests,
  saveResults,
  resetLastExerciseData,
  resetCurrentExerciseState,
  addExerciseSevenState
} from '../state/actions';

//components
import ExerciseSeven from '../components/ExerciseSeven/';
import Spinner from '../../common/components/spinner';
import NoAccessPage from './NoAccessPage';

//utils, data
import { mergeSettingsOptions, shuffleArray, patientExpiredCheck } from '../helpers';
import { exerciseSevenSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';

class ExerciseSevenView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      timelineState: {
        completed: [],
        tests: [],
        correct: [],
        incorrect: [],
      }
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.addResultsToExerciseState = this.addResultsToExerciseState.bind(this);
  }

  componentWillMount() {
    const { locale, getExercise, updateCurrentSettings, match: { params: { id } } } = this.props;
    this.props.resetLastExerciseData();
    getExercise(getQuery(id, 'exercise7'), getQuery(id, 'exercise7Views'))
      .then(exercise => {
        const { settings } = exercise;
        const { updateTests, tests } = this.props;
        const payload = mergeSettingsOptions(settings, exerciseSevenSettings[locale]);

        updateCurrentSettings(payload);
        if (settings.isRandom) {
          updateTests(shuffleArray(tests));
        }
      });
  }

  componentDidMount() {
    this.props.resetCurrentExerciseState();
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
    this.setState({
      timelineState: {
        tests: [],
        completed: [],
        correct: [],
        incorrect: [],
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    const { tests } = nextProps;
    const { updateTests, backupTests, intl: { formatMessage } } = this.props;
    if (this.props.settings.length && nextProps.settings[1].value !== this.props.settings[1].value) {
      if (nextProps.settings[1].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
        updateTests(shuffleArray(tests));
      } else {
        updateTests(backupTests);
      }
      this.setState({
        currentTest: 0,
      });
    }
    this.setState({
      timelineState: {
        tests: tests.map(test => test.order),
        completed: this.state.timelineState.completed,
        correct: this.state.timelineState.correct,
        incorrect: this.state.timelineState.incorrect,
      }
    });
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  showNextTest(answer, nextNumber) {
    const { total, id, patientId, saveResults, settings, history: { push }, showOverlay, tests, intl: { formatMessage } } = this.props;
    const { currentTest, timelineState } = this.state;
    const autoValidation = settings[3].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase();

    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    if (!autoValidation) {
      results.variables.input.correct = answer;
    }

    const cut = tests.slice(currentTest + 1, tests.length);
    const next1 = cut.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);
    const next2 = tests.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);

    const nextTest = (typeof nextNumber === 'number')
      ? nextNumber
      : next1 || next2;

    if (typeof nextNumber !== 'number') {
      saveResults(results);
    }

    showOverlay(false);
    if (total < tests.length - 1 || total >= tests.length - 1 && typeof nextNumber === 'number') {
      this.setState({
        currentTest: nextTest,
        timelineState: (typeof nextNumber === 'number') ? timelineState : {
          tests: timelineState.tests,
          completed: [...timelineState.completed, currentTest + 1],
          correct: answer ? [...timelineState.correct, currentTest + 1] : [...timelineState.correct],
          incorrect: answer ? [...timelineState.incorrect] : [...timelineState.incorrect, currentTest + 1],
        },
      });
      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
    }
  }

  addResultsToExerciseState(answer) {
    const { addExerciseSevenState } = this.props;
    const { currentTest } = this.state;
    addExerciseSevenState(currentTest, answer);
  }

  modifyQuestions(testQuestions, settings) {
    const { formatMessage } = this.props.intl;
    const modifiedQuestions = testQuestions.split(' ');
    return settings[1].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase() ? shuffleArray(modifiedQuestions) : modifiedQuestions;
  }

  render() {
    const {
      tests,
      instructions,
      settings,
      updateCurrentSettings,
      showOverlay,
      exerciseName,
      history: { push },
      template,
      exerciseState,
      currentPatient
    } = this.props;

    if (patientExpiredCheck(currentPatient && currentPatient.expires)) {
      return (<NoAccessPage />);
    }

    const { timelineState } = this.state;

    const currentTest = tests[this.state.currentTest];

    return tests.length && settings.length ?
      <ExerciseSeven
        exerciseName={exerciseName}
        addResultsToExerciseState={this.addResultsToExerciseState}
        exerciseState={exerciseState}
        answersAmount={tests.length}
        questionTitle={instructions}
        questionImage={currentTest.image}
        testQuestions={this.modifyQuestions(currentTest.sentence, settings)}
        testAnswers={currentTest.sentence.split(' ')}
        correctAnswer={currentTest.correctAnswer}
        showNextTest={this.showNextTest}
        handleResultValue={this.handleResultValue}
        showOverlay={showOverlay}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        push={push}
        template={template}
        timelineState={timelineState}
        currentTest={this.state.currentTest}
      />
      :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  instructions: state.Exercises.currentSettings.shortCommand,
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests.map((test, index) => Object.assign({}, test, { order: index + 1 })),
  backupTests: state.Exercises.backupTests,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id,
  template: state.Exercises.currentExercise.template,
  total: state.Exercises.lastExerciseResult.total,
  exerciseName: state.Exercises.currentExercise.settings && state.Exercises.currentExercise.settings.exerciseName,
  exerciseState: state.Exercises.currentExerciseState,
  currentPatient: state.Patient.currentPatient
});

export default connect(mapStateToProps, {
  getExercise,
  resetCurrentExercise,
  updateCurrentSettings,
  showOverlay,
  updateTests,
  saveResults,
  resetLastExerciseData,
  resetCurrentExerciseState,
  addExerciseSevenState
})(injectIntl(ExerciseSevenView));
