import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import axios from 'axios';
// import { PATIENTS_URL, apiConfig } from '../../../common/api';

//redux
import { connect } from 'react-redux';
import {
  getExercises,
  // getFavoriteExercises,
  // getSuggestedExercises,
  setSections,
  modifySections,
  modifyExercises,
  modifyContent,
  modifyAccess,
  setFilters,
  // reportResults,
  resetExercises,
  resetLastExerciseData
} from '../../state/actions';
import { logout, getCurrentTutorInfo } from '../../../user/state/actions';
import { getPatients, getSuggestedExercises } from '../../../patient/state/actions';

//components
import HeaderTemplate from '../../../common/components/HeaderTemplate';
import TopBar from '../../../common/components/TopBar';
import Sidebar from '../../../common/components/Sidebar';
import ExercisesContent from '../../components/ExerciseContent';
import Spinner from '../../../common/components/spinner';
import NoExercises from '../../../common/components/NoExercises';
import PaymentReminder from '../../components/PaymentReminder';

//data, utils
import { exerciseContentSections, filterSettings } from '../../data';
import { getQuery } from '../../../common/queries';
import { patientExpiredCheck } from '../../helpers';

//styles
import styles from './exerciseGridView.css';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';
import { exercisesGridViewMessages } from '../../../common/dictionary/specific';

class ExerciseGridView extends Component {
  static propTypes = {
    exercises: PropTypes.array.isRequired,
    sections: PropTypes.array.isRequired,
    filters: PropTypes.object.isRequired,
    modifyExercises: PropTypes.func.isRequired,
    modifySections: PropTypes.func.isRequired,
    getExercises: PropTypes.func.isRequired,
    setSections: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      suggestionsLoaded: false,
      // count: 0
    };


    // this.increaseCount = this.increaseCount.bind(this); // XXX
  }


  componentWillMount() {
    const { setSections, setFilters, getExercises, getSuggestedExercises, getCurrentTutorInfo, modifyContent, locale, userRole, userId, patientId, currentPatient, token } = this.props;
    if (patientExpiredCheck(currentPatient && currentPatient.expires)) {
      // console.log('expires', currentPatient.expires)
      window.localStorage.setItem('slt2TopBarState', false);
    }

    //extract filter values into object
    const filterObj = filterSettings[locale]
      .reduce((result, el) => [...result, ...el.options.map(el => el.value)], [])
      .reduce((result, el) => Object.assign(result, { [el]: 0 }), {});

    setSections(exerciseContentSections[locale]);
    setFilters(filterObj);
    const idToFetchSuggestions = userRole < 10 ? userId : patientId;

    if (window.location.search === '' || !window.location.search.includes('?preview')) {
      getExercises(getQuery(null, 'exerciseGrid'));
    }

    getSuggestedExercises(idToFetchSuggestions, token).then(
      (data) => {
        // console.log('data', data);
        getCurrentTutorInfo(data.doctor);
        if (this.props.suggestedExercises.length !== 0) {
          modifyContent('suggestions');
        } else {
          modifyContent('all exercises');
        }
        this.setState({ suggestionsLoaded: true });
      }
    ).catch(err => {
      this.setState({ suggestionsLoaded: true });
      modifyContent('all exercises');

      console.error(err);
    });
  }

  componentDidMount() {
    const { resetLastExerciseData, accessDefaultVal } = this.props;
    resetLastExerciseData();
    window.componentHandler.upgradeDom();
  }

  componentWillUnmount() {
    this.props.resetExercises();
  }


  //XXX
  // increaseCount() {
  //   this.setState({ count: this.state.count + 1 });
  // }
  // XXX


  render() {
    const {
      contentType,
      exercises,
      favourites,
      suggestedExercises,
      sections,
      modifySections,
      modifyExercises,
      modifyContent,
      modifyAccess,
      filters,
      patientId,
      currentUser,
      userRole,
      institution,
      currentUserTutorData,
      userId,
      token,
      logout,
      locale,
      accessDefaultVal,
      history: { push },
      intl: { formatMessage },
      currentPatient
    } = this.props;

    const { suggestionsLoaded } = this.state;

    let exercisesContent = null;

    //XXXXXXXX

    // if (userRole < 10 && patientExpiredCheck(currentPatient && currentPatient.expires)) {
    // if (patientExpiredCheck(currentPatient && currentPatient.expires)) {
    //   const newExercises = exercises.filter(exercise => exercise.template === '4' || exercise.template === '4a' || exercise.template === '4b');
    //   if (contentType === 'favourites') {
    //     exercisesContent = favourites;
    //   } else if (contentType === 'suggestions') {
    //     exercisesContent = newExercises.filter(exercise => suggestedExercises && suggestedExercises.includes(exercise._id));
    //   } else {
    //     exercisesContent = newExercises;
    //   }
    //   console.log('here', exercisesContent);
    // } else if (this.state.count < 5) {
    //   const newExercises = exercises.filter(exercise => exercise.template !== '3a' && exercise.template !== '4' && exercise.template !== '4a' && exercise.template !== '4b');
    //   if (contentType === 'favourites') {
    //     exercisesContent = favourites;
    //   } else if (contentType === 'suggestions') {
    //     exercisesContent = newExercises.filter(exercise => suggestedExercises && suggestedExercises.includes(exercise._id));
    //   } else {
    //     exercisesContent = newExercises;
    //   }
    // } else if (contentType === 'favourites') {
    //   exercisesContent = favourites;
    // } else if (contentType === 'suggestions') {
    //   exercisesContent = exercises.filter(exercise => suggestedExercises && suggestedExercises.includes(exercise._id));
    // } else {
    //   exercisesContent = exercises;
    // }

    // console.log('userRole', userRole);
    // console.log('expires', currentPatient && currentPatient.expires);
    // console.log('userId', userId);
    // console.log('patientId', patientId);
    // console.log('currentPatient._id', currentPatient && currentPatient._id);

    // console.log('condition', userRole > 10 && patientExpiredCheck(currentPatient && currentPatient.expires));
    // console.log('exercisesContent', exercisesContent)
    //XXXXXXXX

    if (userRole < 10 && patientExpiredCheck(currentPatient && currentPatient.expires)) {
      const newExercises = exercises.filter(exercise => exercise.template === '4' || exercise.template === '4a' || exercise.template === '4b');
      if (contentType === 'favourites') {
        exercisesContent = favourites;
      } else if (contentType === 'suggestions') {
        exercisesContent = newExercises.filter(exercise => suggestedExercises && suggestedExercises.includes(exercise._id));
      } else {
        exercisesContent = newExercises;
      }
    } else if (contentType === 'favourites') {
      exercisesContent = favourites;
    } else if (contentType === 'suggestions') {
      exercisesContent = exercises.filter(exercise => suggestedExercises && suggestedExercises.includes(exercise._id));
    } else {
      exercisesContent = exercises;
    }


    let content;
    if (exercisesContent[0] === 'noExercises' && contentType === 'all exercises') {
      content = (
        <NoExercises>
          {formatMessage(globalMessages.noExercises)}
        </NoExercises>
      );
    } else if (suggestionsLoaded
      && (
        exercisesContent.length
        || (contentType === 'favourites')
        || (contentType === 'suggestions')
        || exercisesContent[0] === 'error')
    ) {
      content = (<ExercisesContent
        exercises={exercisesContent}
        sections={sections}
        push={push}
        contentType={contentType}
        userRole={userRole}
      />);
    } else {
      content = <Spinner />;
    }

    let topBarSubtitle = '';
    if (contentType === 'all exercises' && accessDefaultVal === 'mine') {
      topBarSubtitle = formatMessage(exercisesGridViewMessages.exercisesGridViewTopSubtitle1);
    } else if (contentType === 'all exercises' && accessDefaultVal === 'others') {
      topBarSubtitle = formatMessage(exercisesGridViewMessages.exercisesGridViewTopSubtitle2);
    } else if (contentType === 'favourites' && (accessDefaultVal === 'mine' || accessDefaultVal === 'others')) {
      topBarSubtitle = formatMessage(exercisesGridViewMessages.exercisesGridViewTopSubtitle3);
    } else if (contentType === 'suggestions' && (accessDefaultVal === 'mine' || accessDefaultVal === 'others')) {
      topBarSubtitle = formatMessage(exercisesGridViewMessages.exercisesGridViewTopSubtitle4);
    }

    return (
      <div>
        <div className={`mdl-layout mdl-js-layout mdl-layout--fixed-header ${styles.gridLayout}`}>
          <HeaderTemplate
            title={formatMessage(globalMessages.exercises)}
            userRole={userRole}
            // reportResults={reportResults}
            userId={userId}
            token={token}
            logout={logout}
          />
          <div>
            <div className="mdl-grid mdl-grid--no-spacing" style={{ backgroundColor: '#EBEFF1' }}>
              {/* <div
                style={{ position: 'fixed', height: 70, width: 270, zIndex: 999 }}
                onClick={() => { this.increaseCount(); }}
              >{}</div>
              {} */}
              <Sidebar
                filters={filters}
                modifySections={modifySections}
                modifyExercises={modifyExercises}
                modifyContent={modifyContent}
                modifyAccess={modifyAccess}
                locale={locale}
                userRole={userRole}
                accessDefaultVal={accessDefaultVal}
                contentDefaultVal={contentType}
                onlyStuttering={patientExpiredCheck(currentPatient && currentPatient.expires)}
              />
              <div className="mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet">
                <TopBar
                  title={formatMessage(globalMessages.exercises)}
                  subtitle={topBarSubtitle}
                />
                {contentType === 'suggestions' && <div className={`mdl-grid mdl-shadow ${styles.contentSection}`}>
                  {contentType === 'suggestions' && currentUserTutorData.firstname && currentUserTutorData.lastname &&
                    <div>{`${formatMessage(globalMessages.tutor)}: ${currentUserTutorData.firstname} ${currentUserTutorData.lastname} ${institution ? ' (' + institution + ')' : ''}`}
                    </div>
                  }
                </div>
                }
                {
                  userRole < 10 && patientExpiredCheck(currentPatient && currentPatient.expires) && <PaymentReminder patientId={patientId || userId} />
                  // patientExpiredCheck(currentPatient && currentPatient.expires) && <PaymentReminder />
                }
                {content}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    exercises: state.Exercises.currentExercises,
    favourites: state.Exercises.currentFavourites,
    suggestedExercises: state.Patient.suggestedExercises,
    contentType: state.Exercises.contentType,
    sections: state.Exercises.sections,
    filters: state.Exercises.filters,
    accessDefaultVal: state.Exercises.accessType,
    currentUser: state.User.currentUser,
    currentUserTutorData: state.User.currentUserTutorData,
    userRole: state.User.currentUser.role,
    locale: state.User.currentUser.locale,
    userId: state.User.currentUser.id,
    patientId: state.Patient.patientId,
    token: state.User.currentUser.token
  };
}

export default connect(mapStateToProps, {
  getExercises,
  getPatients,
  // getFavoriteExercises,
  getSuggestedExercises,
  setSections,
  modifySections,
  modifyExercises,
  modifyContent,
  modifyAccess,
  setFilters,
  // reportResults,
  resetExercises,
  logout,
  getCurrentTutorInfo,
  resetLastExerciseData
})(injectIntl(ExerciseGridView));
