import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  resetLastExerciseData,
  getExercise,
  resetCurrentExercise,
  updateCurrentSettings,
  saveResults,
  resetCurrentExerciseState,
  addExerciseSixState
} from '../state/actions';

//utils, data
import isEqual from 'lodash/isEqual';
import { mergeSettingsOptions, setExerciseDelay, patientExpiredCheck } from '../helpers';
import { exerciseSixSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//components
import ExerciseSix from '../components/ExerciseSix/';
import Spinner from '../../common/components/spinner';
import NoAccessPage from './NoAccessPage';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';

class ExerciseSixView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      answersAmount: 0,
      currentAnswers: [null],
      currentResults: [],
      timelineState: {
        completed: [],
        tests: [],
        correct: [],
        incorrect: [],
      },
      shouldNextTest: false,
    };


    this.showNextTest = this.showNextTest.bind(this);
    this.moveAnswer = this.moveAnswer.bind(this);
    this.rearrange = this.rearrange.bind(this);
    this.triggerNextTestAfterComponentUpdate = this.triggerNextTestAfterComponentUpdate.bind(this);
    this.addResultsToExerciseState = this.addResultsToExerciseState.bind(this);
  }

  componentWillMount() {
    const { locale, getExercise, updateCurrentSettings, match: { params: { id } } } = this.props;
    this.props.resetLastExerciseData();
    getExercise(getQuery(id, 'exercise6'), getQuery(id, 'exercise6Views'))
      .then(exercise => {
        const payload = mergeSettingsOptions(exercise.settings, exerciseSixSettings[locale]);
        updateCurrentSettings(payload);

        const currentAnswers = Object.assign([], this.props.tests[this.state.currentTest].answers);
        this.setState({
          currentAnswers,
          answersAmount: currentAnswers.length,
          currentResults: new Array(currentAnswers.length).fill(null),
          timelineState: Object.assign({}, this.state.timelineState, {
            tests: this.props.tests.map(test => test.order)
          })
        });
      });
  }

  componentDidMount() {
    this.props.resetCurrentExerciseState();
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
    this.setState({
      timelineState: {
        tests: [],
        completed: [],
        correct: [],
        incorrect: [],
      }
    });
  }

  componentDidUpdate() {
    const isValid = this.state.currentResults.every((el, index) => el === this.props.tests[this.state.currentTest].references[index]);
    if (this.state.currentAnswers.length === 0 && isValid && !this.state.shouldNextTest) {
      this.triggerNextTestAfterComponentUpdate();
    }
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  // For auto validation
  triggerNextTestAfterComponentUpdate() {
    const { formatMessage } = this.props.intl;
    this.setState({ shouldNextTest: true }, () => {
      const { settings } = this.props;
      const nextTestDelay = setExerciseDelay(settings[1].value);
      if (settings[3].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
        setTimeout(() => {
          this.showNextTest(true);
        }, nextTestDelay);
      }
    });
  }

  showNextTest(answer, nextNumber) {
    const { total, id, patientId, saveResults, settings, history: { push }, tests, intl: { formatMessage } } = this.props;
    const { timelineState, currentTest, currentResults } = this.state;
    const autoValidation = settings[3].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase();

    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    if (!autoValidation) {
      results.variables.input.correct = answer;
    }

    if (typeof nextNumber !== 'number') {
      this.addResultsToExerciseState(currentResults);
      saveResults(results);
    }

    if (total < tests.length - 1) {
      const cut = tests.slice(currentTest + 1, tests.length);
      const next1 = cut.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);
      const next2 = tests.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);

      const nextTest = (typeof nextNumber === 'number')
        ? nextNumber
        : next1 || next2;

      const currentAnswers = Object.assign([], tests[nextTest].answers);
      const answersAmount = currentAnswers.length;

      this.setState({
        answersAmount,
        currentAnswers,
        currentResults: new Array(currentAnswers.length).fill(null),
        currentTest: nextTest,
        shouldNextTest: false,
        timelineState: typeof nextNumber === 'number' ? timelineState : {
          tests: [...timelineState.tests],
          completed: [...timelineState.completed, currentTest + 1],
          correct: answer ? [...timelineState.correct, currentTest + 1] : [...timelineState.correct],
          incorrect: !answer ? [...timelineState.incorrect, currentTest + 1] : [...timelineState.incorrect],
        },
      });

      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
    }
  }

  backupState() {
    this.stateBackup = {};
    this.stateBackup.currentAnswers = Object.assign([], this.state.currentAnswers);
    this.stateBackup.currentResults = Object.assign([], this.state.currentResults);
  }

  restoreState() {
    const currentAnswers = Object.assign([], this.stateBackup.currentAnswers);
    const currentResults = Object.assign([], this.stateBackup.currentResults);
    if (
      !isEqual(currentAnswers, this.state.currentAnswers) ||
      !isEqual(currentResults, this.state.currentResults)
    ) {
      this.setState({ currentAnswers, currentResults });
    }
  }

  findEmpty(arr, index, answer) {
    let rightIndex;
    let leftIndex;
    for (let i = index + 1; i < arr.length; i++) {
      if (arr[i] === null || arr[i] === answer) {
        rightIndex = i;
        break;
      }
    }
    for (let i = index - 1; i >= 0; i--) {
      if (arr[i] === null || arr[i] === answer) {
        leftIndex = i;
        break;
      }
    }
    if (leftIndex === undefined) {
      return rightIndex;
    } else if (rightIndex === undefined) {
      return leftIndex;
    }
    return index - leftIndex >= rightIndex - index ? rightIndex : leftIndex;
  }

  rearrange(dropIndex, occupiedWith, { answer, inResult }) {
    const { currentResults } = this.state;
    if (occupiedWith && !inResult) {
      const emptyIndex = this.findEmpty(currentResults, dropIndex);
      if (dropIndex < emptyIndex) {
        currentResults.splice(dropIndex, 0, null);
        currentResults.splice(emptyIndex + 1, 1);
        this.setState({ currentResults });
      } else if (dropIndex > emptyIndex) {
        currentResults.splice(emptyIndex, 1);
        currentResults.splice(dropIndex, 0, null);
        this.setState({ currentResults });
      }
    } else if (occupiedWith && inResult) {
      const emptyIndex = this.findEmpty(currentResults, dropIndex, answer);
      if (dropIndex < emptyIndex) {
        currentResults.splice(dropIndex, 0, null);
        currentResults.splice(emptyIndex + 1, 1);
        this.setState({ currentResults });
      } else if (dropIndex > emptyIndex) {
        currentResults.splice(emptyIndex, 1);
        currentResults.splice(dropIndex, 0, null);
        this.setState({ currentResults });
      }
    }
  }

  moveAnswer(from, to, answer, inResult, occupiedWith) {
    const { currentAnswers, currentResults } = this.state;
    if (!inResult) {
      currentAnswers.splice(from, 1);
      currentResults[to] = answer;
      this.setState({ currentAnswers, currentResults });
    } else if (inResult && !occupiedWith) {
      if (currentResults[from] === answer) {
        currentResults.splice(from, 1, null);
      }
      currentResults[to] = answer;
      this.setState({ currentResults });
    }
  }

  addResultsToExerciseState(state) {
    const { addExerciseSixState } = this.props;
    const { currentTest } = this.state;
    addExerciseSixState(currentTest, state);
  }

  render() {
    const {
      settings,
      tests,
      updateCurrentSettings,
      instructions,
      template,
      exerciseName,
      exerciseState,
      history: { push },
      currentPatient
    } = this.props;

    if (patientExpiredCheck(currentPatient && currentPatient.expires)) {
      return (<NoAccessPage />);
    }

    const { timelineState, currentAnswers, currentResults, answersAmount } = this.state;
    const currentTest = tests[this.state.currentTest];
    return tests.length && settings.length ?
      <ExerciseSix
        exerciseName={exerciseName}
        exerciseState={exerciseState}
        questionTitle={instructions}
        questionImage={currentTest.image}
        currentAnswers={currentAnswers}
        currentResults={currentResults}
        currentReferences={currentTest.references}
        answersAmount={answersAmount}
        moveAnswer={this.moveAnswer}
        showNextTest={this.showNextTest}
        rearrange={this.rearrange}
        backupState={this.backupState.bind(this)}
        restoreState={this.restoreState.bind(this)}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        push={push}
        template={template}
        timelineState={timelineState}
        currentTest={this.state.currentTest}
      /> :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  instructions: state.Exercises.currentSettings.shortCommand,
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests.map((test, index) => Object.assign({}, test, { order: index + 1 })),
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id,
  template: state.Exercises.currentExercise.template,
  total: state.Exercises.lastExerciseResult.total,
  exerciseName: state.Exercises.currentExercise.settings && state.Exercises.currentExercise.settings.exerciseName,
  exerciseState: state.Exercises.currentExerciseState,
  currentPatient: state.Patient.currentPatient
});

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  resetCurrentExercise,
  saveResults,
  resetLastExerciseData,
  resetCurrentExerciseState,
  addExerciseSixState
})(injectIntl(ExerciseSixView));
