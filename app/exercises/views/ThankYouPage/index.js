import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import { getTotalExerciseTime, resetLastExerciseData } from '../../state/actions';

//i18n declaration
import { injectIntl } from 'react-intl';
import { thankYouPage } from '../../../common/dictionary/specific';

//components
import PieChart from 'react-minimal-pie-chart';

//utils, data
import { getThankYouQuery } from '../../../common/queries';
import { detectIE } from '../../helpers';

//styles
import styles from './thankYouPage.css';


class ThankYouPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalTime: 0
    };
    this.correctSound = new Audio('../../sounds/correct.wav');
  }

  componentWillMount() {
    const { lastExerciseId, userId, patientId, getTotalExerciseTime } = this.props;
    if (lastExerciseId) {
      getTotalExerciseTime(getThankYouQuery(patientId || userId, lastExerciseId))
        .then(totalTime => {
          this.setState({ totalTime });
        });
    }
    window.componentHandler.upgradeDom();
    this.isIE = !!detectIE();
  }

  componentDidMount() {
    // timeout set to immitate sound on charts animation end
    // may change the sound later
    setTimeout(() => { this.correctSound.play(); }, 1500);
  }

  componentWillReceiveProps(nextProps) {
    //in case exercise id wasn't present during mounting
    const { lastExerciseId } = this.props;
    if (!lastExerciseId) {
      const { lastExerciseId, userId, patientId, getTotalExerciseTime } = nextProps;
      getTotalExerciseTime(getThankYouQuery(patientId || userId, lastExerciseId))
        .then(totalTime => {
          this.setState({ totalTime });
        });
    }
  }

  componentWillUnmount() {
    this.props.resetLastExerciseData();
  }

  render() {
    console.log(this.props);
    const { totalTime } = this.state;
    if (!totalTime) {
      return null;
    }
    const {
      lastExerciseDuration,
      locale,
      results: { total, correct, incorrect, statteringPassed },
      history: { push, goBack },
      intl: { formatMessage }
    } = this.props;

    const totalTries = !statteringPassed ? total : correct + incorrect;

    const buttonClass = 'mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect';

    const minutesSpent = Math.ceil(lastExerciseDuration / 1000 / 60);
    const minutesTotal = Math.ceil(totalTime / 60);
    const spentValue = minutesSpent && minutesTotal ? (minutesSpent / minutesTotal) * 100 : minutesSpent;

    let spentMessage = formatMessage(thankYouPage.spent);
    let totalMessage = formatMessage(thankYouPage.total);
    if (locale === 'et') {
      spentMessage = spentMessage.replace('$xMinutit', `${minutesSpent} ${minutesSpent !== 1 ? 'minutit' : 'minut'}`);
      totalMessage = totalMessage.replace('$xMinutit', `${minutesTotal} ${minutesTotal !== 1 ? 'minutit' : 'minut'}`);
    } else {
      spentMessage = spentMessage.replace('$xMinutes', `${minutesSpent} ${minutesSpent !== 1 ? 'minutes' : 'minute'}`);
      totalMessage = totalMessage.replace('$xMinutes', `${minutesTotal} ${minutesTotal !== 1 ? 'minutes' : 'minute'}`);
    }

    const chartData = [
      { value: spentValue, key: 1, color: '#32887A' },
      { value: 100 - spentValue, key: 2, color: 'rgba(0,0,0, 0.2)' }
    ];


    const correctPercent = +((correct / totalTries) * 100).toFixed(1);

    const resultsChartData = [
      { value: correctPercent, key: 1, color: '#32887A' },
      { value: 100 - correctPercent, key: 2, color: 'rgba(0,0,0, 0.2)' }
    ];

    return (
      <div className={styles.wrapper}>
        <div className={`mdl-grid mdl-grid--no-spacing mdl-shadow--8dp ${styles.thankYouPage}`}>
          <div className={`mdl-cell--12-col ${styles.headingWrapper}`}>
            <h2 className={styles.heading}>{formatMessage(thankYouPage.exerciseOver)}</h2>
          </div>
          <div className={`mdl-cell--12-col ${styles.summaryWrapper}`}>
            {/* <div className={styles.summaryHeadingWrapper}>
              <span className={styles.summaryHeading}>{formatMessage(thankYouPage.summary)}</span>
            </div> */}
            <div className={styles.summarySection}>
              <div className={styles.summaryBlock}>
                <div className={`mdl-cell--12-col ${styles.resultsWrapper}`}>
                  {correct >= 0 && totalTries > 0 &&
                    <div className={styles.resutsChartWrapper}>
                      <PieChart
                        data={resultsChartData}
                        lineWidth={25}
                        startAngle={-90}
                        radius={50}
                        animate={!this.isIE}
                      />
                      <span className={styles.resultPercentage}>
                        {correctPercent}<span>%</span>
                      </span>
                    </div>
                  }
                  {
                    statteringPassed && <div className={styles.correctAnswer}>
                      <div className={`mdl-cell--12-col ${styles.timingLegend}`}>
                        <span className={styles.timingLegendDot} style={{ background: '#32887A' }} />
                        <span className={styles.timingLegendMessage}>{`${formatMessage(thankYouPage.correctTiming)} - ${correct}/${correct + incorrect}`}</span>
                      </div>
                      <div className={styles.timingLegend}>
                        <span className={styles.timingLegendDot} style={{ background: 'rgba(0,0,0, 0.2)' }} />
                        <span className={styles.timingLegendMessage}>{formatMessage(thankYouPage.incorrectTiming)}</span>
                      </div>
                    </div>
                  }
                  {!statteringPassed && correct > 0 && totalTries > 0 &&
                    <div className={styles.correctAnswer}>
                      {formatMessage(thankYouPage.correctAnswers)}
                    </div>
                  }
                </div>
              </div>
              <div className={styles.summaryBlock}>
                <div className={styles.chartBlock}>
                  <div className={styles.chartWrapper}>
                    <i className={`material-icons ${styles.clockIcon}`}>access_time</i>
                  </div>
                  <div className={styles.resultsText}>{spentMessage}</div>
                </div>
                <div className={styles.chartBlock}>
                  <div className={styles.chartWrapper}>
                    <PieChart
                      data={chartData}
                      lineWidth={25}
                      startAngle={-90}
                      radius={50}
                      animate={!this.isIE}
                    />
                  </div>
                  <div className={styles.resultsText}>{totalMessage}</div>
                </div>
              </div>
            </div>
          </div>
          <div className={`mdl-cell--6-col mdl-cell--4-col-tablet ${styles.buttonWrapper}`}>
            <button
              className={buttonClass}
              onClick={() => {
                goBack();
              }}
            >
              {formatMessage(thankYouPage.repeatExercise)}
            </button>
          </div>
          <div className={`mdl-cell--6-col mdl-cell--4-col-tablet ${styles.buttonWrapper}`}>
            <button
              className={`${buttonClass} ${styles.greenButton}`}
              onClick={() => {
                push('/practice');
              }}
            >
              {formatMessage(thankYouPage.showOtherExercises)}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  userId: state.User.currentUser.id,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  lastExerciseId: state.Exercises.lastExerciseId,
  lastExerciseDuration: state.Exercises.lastExerciseDuration,
  results: state.Exercises.lastExerciseResult
});

export default connect(mapStateToProps, {
  getTotalExerciseTime,
  resetLastExerciseData
})(injectIntl(ThankYouPage));
