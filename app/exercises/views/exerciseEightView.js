import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercise,
  resetCurrentExercise,
  showOverlay,
  getOptions,
  updateCurrentSettings,
  saveResults,
  resetLastExerciseData
} from '../state/actions';

//components
import ExerciseEight from '../components/ExerciseEight';
import Spinner from '../../common/components/spinner';
import NoAccessPage from './NoAccessPage';

//utils, data
import { mergeSettingsOptions, transformSpecSymb, patientExpiredCheck } from '../helpers';
import { exerciseEightSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';


class ExerciseEightView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      timelineState: {
        tests: [],
        correct: [],
        incorrect: [],
        completed: [],
      },
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.triggerShowNextTest = this.triggerShowNextTest.bind(this);
    this.playOneAudio = this.playOneAudio.bind(this);
  }

  componentWillMount() {
    const {
      locale,
      selectedTopic,
      getOptions,
      getExercise,
      updateCurrentSettings,
      tests,
      match: { params: { id } }
    } = this.props;
    this.props.resetLastExerciseData();
    getOptions(locale)
      .then(() => getExercise(getQuery(id, 'exercise8'), getQuery(id, 'exercise8Views', selectedTopic)))
      .then(exercise => {
        const payload = mergeSettingsOptions(exercise.settings, exerciseEightSettings[locale]);
        updateCurrentSettings(payload);
        // this.setState({
        //   timelineState: {
        //     tests: tests.map(test => test.order),
        //     completed: [],
        //     correct: [],
        //     incorrect: [],
        //   }
        // });
      });
  }

  componentDidMount() {
    const { tests } = this.props;
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
    this.setState({
      currentTest: 0,
      // timelineState: {
      //   tests: tests.map(test => test.order),
      //   completed: [],
      //   correct: [],
      //   incorrect: [],
      // },
    });
  }

  componentWillReceiveProps(nextProps) {
    const { tests } = nextProps;
    this.setState(prevState => ({
      timelineState: {
        ...prevState.timelineState,
        tests: tests.map(test => test.order)
      }
    }));
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  showNextTest(answer, nextNumber) {
    const { wordLists, total, id, patientId, saveResults, history: { push }, showOverlay, tests } = this.props;
    const { timelineState, currentTest } = this.state;
    const currentTestObject = tests[this.state.currentTest];
    const testQuestions = wordLists[transformSpecSymb(currentTestObject.topic.toLowerCase())];

    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };

    if (typeof nextNumber !== 'number') {
      saveResults(results);
    }

    showOverlay(false);
    if (total < tests.length - 1) {
      const cut = tests.slice(currentTest + 1, tests.length);
      const next1 = cut.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);
      const next2 = tests.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);

      const nextTest = (typeof nextNumber === 'number')
        ? nextNumber
        : next1 || next2;

      this.setState({
        currentTest: nextTest,
        timelineState: (typeof nextNumber === 'number') ? timelineState : {
          tests: timelineState.tests,
          completed: [...timelineState.completed, currentTest + 1],
          correct: answer ? [...timelineState.correct, currentTest + 1] : [...timelineState.correct],
          incorrect: answer ? [...timelineState.incorrect] : [...timelineState.incorrect, currentTest + 1],
        },
      });

      this.dimButton(`exercise-${testQuestions[currentTest].text.toLowerCase()}-button`);
      for (let i = 0; i < testQuestions.length; i++) {
        this.dimButton(`exercise-${testQuestions[i].text.toLowerCase()}-button`);
      }

      const startButton = document.getElementById('start-exercise-button');
      startButton.removeAttribute('disabled');

      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
    }
  }

  triggerShowNextTest(answer, nextNumber) {
    this.props.showOverlay();
    this.showNextTest(answer, nextNumber);
  }

  lightButton(id) {
    const currentButton = document.getElementById(id);
    currentButton.className += ' mdl-button--accent';
  }

  dimButton(id) {
    const prevButton = document.getElementById(id);
    if (prevButton) {
      prevButton.className = prevButton.className.replace('mdl-button--accent', '');
    }
  }

  playAudio(id) {
    const currentAudio = document.getElementById(id);
    currentAudio.play();
  }

  playOneAudio(index) {
    const { tests, wordLists } = this.props;
    const currentTest = tests[this.state.currentTest];
    const testQuestions = wordLists[transformSpecSymb(currentTest.topic.toLowerCase())];

    if (index < testQuestions.length) {
      if (index > 0) {
        this.dimButton(`exercise-${testQuestions[index - 1].text.toLowerCase()}-button`);
      }

      this.lightButton(`exercise-${testQuestions[index].text.toLowerCase()}-button`);

      const id = `exercise-${testQuestions[index].text.toLowerCase()}-audio`;
      this.playAudio(id);
    } else {
      this.dimButton(`exercise-${testQuestions[index - 1].text.toLowerCase()}-button`);
      this.triggerShowNextTest(true);
    }
  }

  playNext(i = 0) {
    const { tests, wordLists, intl: { formatMessage } } = this.props;
    const currentTest = tests[this.state.currentTest];
    const testQuestions = wordLists[transformSpecSymb(currentTest.topic.toLowerCase())];

    const startButton = document.getElementById('start-exercise-button');
    startButton.setAttribute('disabled', '');

    if (i < testQuestions.length) {
      if (i > 0) {
        this.dimButton(`exercise-${testQuestions[i - 1].text.toLowerCase()}-button`);
      }

      this.lightButton(`exercise-${testQuestions[i].text.toLowerCase()}-button`);

      if (currentTest.audio.toLowerCase() === formatMessage(globalMessages.off).toLowerCase()) {
        setTimeout(this.playNext.bind(this, i + 1), 3000);
      } else if (currentTest.skippedWords.some((word) => testQuestions[i].text === word)) {
        setTimeout(this.playNext.bind(this, i + 1), 3000);
      } else if (currentTest.audio.toLowerCase() === formatMessage(globalMessages.on).toLowerCase()) {
        this.playAudio(`exercise-${testQuestions[i].text.toLowerCase()}-audio`);
      } else {
        this.playAudio(`exercise-${testQuestions[i].text.toLowerCase()}-audio`);
      }
    } else {
      this.dimButton(`exercise-${testQuestions[i - 1].text.toLowerCase()}-button`);
      this.triggerShowNextTest(true);
    }
  }

  startExercise() {
    this.playNext();
  }

  render() {
    const {
      exerciseName,
      template,
      tests,
      settings,
      updateCurrentSettings,
      instructions,
      wordLists,
      history: { push },
      currentPatient
    } = this.props;

    if (patientExpiredCheck(currentPatient && currentPatient.expires)) {
      return (<NoAccessPage />);
    }

    const { timelineState } = this.state;
    const currentTest = tests[this.state.currentTest];
    
    return tests.length && settings.length ?
      <ExerciseEight
        exerciseName={exerciseName}
        questionTitle={instructions}
        testQuestions={wordLists[transformSpecSymb(currentTest.topic.toLowerCase())]}
        questionImage={currentTest.image}
        originalText={currentTest.originalText}
        startExercise={this.startExercise.bind(this)}
        playNext={this.playNext.bind(this)}
        playOneAudio={this.playOneAudio}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        push={push}
        showNextTest={this.triggerShowNextTest}
        template={template}
        timelineState={timelineState}
        currentTest={this.state.currentTest}
      /> :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  wordLists: state.Exercises.templateWordLists,
  instructions: state.Exercises.currentSettings.shortCommand,
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests.map((test, index) => Object.assign({}, test, { order: index + 1 })),
  selectedTopic: state.Exercises.selectedTopic,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id,
  template: state.Exercises.currentExercise.template,
  total: state.Exercises.lastExerciseResult.total,
  exerciseName: state.Exercises.currentExercise.settings && state.Exercises.currentExercise.settings.exerciseName,
  currentPatient: state.Patient.currentPatient
});

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  resetCurrentExercise,
  showOverlay,
  getOptions,
  saveResults,
  resetLastExerciseData
})(injectIntl(ExerciseEightView));
