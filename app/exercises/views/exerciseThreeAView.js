import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercise,
  resetCurrentExercise,
  showOverlay,
  updateCurrentSettings,
  updateTests,
  saveResults,
  resetLastExerciseData,
  resetCurrentExerciseState,
  addExerciseThreeState,
} from '../state/actions';

//utils, data
import { shuffleArray, mergeSettingsOptions, patientExpiredCheck } from '../helpers';
import { exerciseThreeSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';

//components
import ExerciseThreeA from '../components/ExerciseThreeA';
import Spinner from '../../common/components/spinner';
import NoAccessPage from './NoAccessPage';

class ExerciseThreeAView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired,
    showOverlay: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    tests: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      correct: true,
      timelineState: {
        tests: [],
        completed: [],
        correct: [],
        incorrect: [],
      },
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.triggerShowNextTest = this.triggerShowNextTest.bind(this);
    this.showImages = this.showImages.bind(this);
    this.addResultsToExerciseState = this.addResultsToExerciseState.bind(this);
  }

  componentWillMount() {
    const {
      locale,
      selectedTopic,
      getExercise,
      match: { params: { id } },
      updateCurrentSettings,
      resetLastExerciseData
    } = this.props;

    resetLastExerciseData();
    getExercise(getQuery(id, 'exercise3'), getQuery(id, 'exercise3Views', selectedTopic))
      .then(exercise => {
        const { updateTests, tests } = this.props;
        const { settings } = exercise;

        const payload = mergeSettingsOptions(settings, exerciseThreeSettings[locale]);
        updateCurrentSettings(payload);
        if (settings.isRandom) {
          updateTests(shuffleArray(tests));
        }
      });
  }

  componentDidMount() {
    this.props.resetCurrentExerciseState();
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
    this.setState({
      timelineState: {
        tests: [],
        completed: [],
        correct: [],
        incorrect: [],
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    const { updateTests, tests, backupTests, intl: { formatMessage } } = this.props;
    if (this.props.settings.length && nextProps.settings[1].value !== this.props.settings[1].value) {
      if (nextProps.settings[1].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
        updateTests(shuffleArray(tests));
      } else {
        updateTests(backupTests);
      }
      this.setState({ currentTest: 0 });
    }
    this.setState({
      timelineState: {
        tests: tests.map(test => test.order),
        completed: this.state.timelineState.completed,
        correct: this.state.timelineState.correct,
        incorrect: this.state.timelineState.incorrect,
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    //skip render on state.correct change
    return nextState.correct === this.state.correct;
  }

  componentWillUnmount() {
    const { resetCurrentExercise } = this.props;
    resetCurrentExercise();
  }

  addResultsToExerciseState(state) {
    const { addExerciseThreeState } = this.props;
    const { currentTest } = this.state;
    addExerciseThreeState(currentTest, state);
  }

  showNextTest(correct, nextNumber) {

    console.log(correct, nextNumber)
    const {
      total,
      id,
      patientId,
      saveResults,
      settings,
      tests,
      history: { push },
      intl: { formatMessage }
    } = this.props;
    const { timelineState, currentTest } = this.state;
    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          correct,
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };

    const cut = tests.slice(currentTest + 1, tests.length);
    const next1 = cut.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);
    const next2 = tests.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);

    console.log(next1, next2)


    const nextTest = (typeof nextNumber === 'number')
      ? nextNumber
      : next1 || next2;

    if (typeof nextNumber !== 'number') {
      saveResults(results);
    }

    if (settings[2].value.toLowerCase() !== formatMessage(globalMessages.audioFirst).toLowerCase()) {
      this.startTime = new Date().getTime();
    }
    if (total < tests.length - 1 || total >= tests.length - 1 && typeof nextNumber === 'number') {
      // setState should be separate to work correctly with shouldComponentUpdate condition
      this.setState({
        currentTest: nextTest,
        timelineState: (typeof nextNumber === 'number') ? timelineState : {
          tests: timelineState.tests,
          completed: [...timelineState.completed, currentTest + 1],
          correct: correct ? [...timelineState.correct, currentTest + 1] : [...timelineState.correct],
          incorrect: correct ? [...timelineState.incorrect] : [...timelineState.incorrect, currentTest + 1],
        },
      });
      this.setState({ correct: true });
    } else {
      push('/thankyou');
    }
  }

  triggerShowNextTest(answer, nextNumber) {
    this.showNextTest(answer, nextNumber);
  }

  showImages() {
    // this.props.showOverlay(false);
  }

  render() {
    const {
      template,
      tests,
      settings,
      updateCurrentSettings,
      exerciseName,
      exerciseState,
      history: { push },
      currentPatient
    } = this.props;

    if (patientExpiredCheck(currentPatient && currentPatient.expires)) {
      return (<NoAccessPage />);
    }

    const currentTest = tests[this.state.currentTest];
    const { timelineState } = this.state;
    
    return tests.length && settings.length ?
      <ExerciseThreeA
        exerciseName={exerciseName}
        exerciseState={exerciseState}
        // questionImage={currentTest.image.filter(el => !!el.url)}
        // questionAudio={JSON.parse(currentTest.audio).filter(el => !!el.url)}
        questionAudio={JSON.parse(currentTest.audio)}
        actions={currentTest.actions}
        instructions={currentTest.originalText}
        showNextTest={this.triggerShowNextTest}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        testIndex={this.state.currentTest}
        showImages={this.showImages}
        push={push}
        template={template}
        timelineState={timelineState}
        currentTest={this.state.currentTest}
        addResultsToExerciseState={this.addResultsToExerciseState}
      />
      :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests.map((test, index) => Object.assign({}, test, { order: index + 1 })),
  backupTests: state.Exercises.backupTests,
  selectedTopic: state.Exercises.selectedTopic,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id,
  total: state.Exercises.lastExerciseResult.total,
  template: state.Exercises.currentExercise.template,
  exerciseName: state.Exercises.currentExercise.settings && state.Exercises.currentExercise.settings.exerciseName,
  exerciseState: state.Exercises.currentExerciseState,
  currentPatient: state.Patient.currentPatient
});

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  updateTests,
  resetCurrentExercise,
  showOverlay,
  saveResults,
  resetLastExerciseData,
  resetCurrentExerciseState,
  addExerciseThreeState
})(injectIntl(ExerciseThreeAView));
