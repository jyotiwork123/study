import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  resetLastExerciseData,
  getExercise,
  resetCurrentExercise,
  showOverlay,
  updateCurrentSettings,
  saveResults,
  resetCurrentExerciseState,
  addExerciseFiveState
} from '../state/actions';
import { getQuery, saveResultsQuery } from '../../common/queries';

//utils data
import { shuffleArray, mergeSettingsOptions, patientExpiredCheck } from '../helpers';
import { exerciseFiveSettings } from '../data';

//components
import ExerciseFive from '../components/ExerciseFive/';
import Spinner from '../../common/components/spinner';
import NoAccessPage from './NoAccessPage';

class ExerciseFiveView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired,
    showOverlay: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    tests: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
    instructions: PropTypes.string
  };
  static defaultProps = {
    instructions: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      correct: true,
      timelineState: {
        completed: [],
        tests: [],
        correct: [],
        incorrect: [],
      }
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.triggerShowNextTest = this.triggerShowNextTest.bind(this);
    this.addResultsToExerciseState = this.addResultsToExerciseState.bind(this);
  }

  componentWillMount() {
    const { locale, selectedTopic, getExercise, updateCurrentSettings, match: { params: { id } } } = this.props;
    this.props.resetLastExerciseData();
    getExercise(getQuery(id, 'exercise5'), getQuery(id, 'exercise5Views', selectedTopic))
      .then(exercise => {
        const payload = mergeSettingsOptions(exercise.settings, exerciseFiveSettings[locale]);
        updateCurrentSettings(payload);
      });
  }

  componentDidMount() {
    this.props.resetCurrentExerciseState();
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
    this.setState({
      timelineState: {
        tests: [],
        completed: [],
        correct: [],
        incorrect: [],
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    const { tests } = nextProps;
    this.setState({
      timelineState: {
        tests: tests.map(test => test.order),
        completed: this.state.timelineState.completed,
        correct: this.state.timelineState.correct,
        incorrect: this.state.timelineState.incorrect,
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    //skip render on state.correct change
    return nextState.correct === this.state.correct;
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  showNextTest(nextNumber) {
    const { total, id, patientId, saveResults, history: { push }, showOverlay, tests } = this.props;
    const { correct, currentTest, timelineState } = this.state;
    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          correct,
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    const cut = tests.slice(currentTest + 1, tests.length);
    const next1 = cut.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);
    const next2 = tests.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);

    const nextTest = (nextNumber || nextNumber === 0)
      ? nextNumber
      : next1 || next2;

    if (!nextNumber && !(nextNumber === 0)) {
      saveResults(results);
    }

    showOverlay(false);
    if (total < tests.length - 1 || total >= tests.length - 1 && typeof nextNumber === 'number') {
      // setState should be separate to work correctly with shouldComponentUpdate condition
      this.setState({
        currentTest: nextTest,
        timelineState: (nextNumber || nextNumber === 0) ? timelineState : {
          tests: timelineState.tests,
          completed: [...timelineState.completed, currentTest + 1],
          correct: correct ? [...timelineState.correct, currentTest + 1] : [...timelineState.correct],
          incorrect: correct ? [...timelineState.incorrect] : [...timelineState.incorrect, currentTest + 1],
        },
      });

      this.setState({ correct: true });
      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
    }
  }

  triggerShowNextTest(answer, nextNumber) {
    if (answer) {
      this.props.showOverlay();
      this.showNextTest(nextNumber);
    } else if (!answer && this.state.correct) {
      this.setState({ correct: answer });
    }
  }

  addResultsToExerciseState(answers, questions, correct) {
    const { addExerciseFiveState } = this.props;
    const { currentTest } = this.state;
    addExerciseFiveState(currentTest, answers, questions, correct);
  }

  formatAnswers(currentTestObj) {
    const { value } = this.props.settings[4];
    const numberOfAnswers = parseInt(value, 10);
    const { correctAnswer } = currentTestObj;
    const modifiedArray = Object.assign([], currentTestObj.incorrectAnswers);

    if (Number.isInteger(numberOfAnswers) && modifiedArray.length >= numberOfAnswers) {
      modifiedArray.length = numberOfAnswers - 1;
    }
    modifiedArray.push(correctAnswer);
    return {
      correctAnswer,
      testQuestions: shuffleArray(modifiedArray)
    };
  }

  render() {
    const {
      tests,
      instructions,
      settings,
      updateCurrentSettings,
      template,
      exerciseName,
      exerciseState,
      history: { push },
      currentPatient
    } = this.props;
    const { timelineState } = this.state;
    const currentTest = tests[this.state.currentTest];

    if (patientExpiredCheck(currentPatient && currentPatient.expires)) {
      return (<NoAccessPage />);
    }

    return tests.length && settings.length ?
      <ExerciseFive
        exerciseName={exerciseName}
        exerciseState={exerciseState}
        addResultsToExerciseState={this.addResultsToExerciseState}
        questionTitle={instructions}
        questionImage={currentTest.image}
        questionText={currentTest.sentence}
        testQuestions={this.formatAnswers(currentTest)}
        showNextTest={this.triggerShowNextTest}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        push={push}
        template={template}
        timelineState={timelineState}
        currentTest={this.state.currentTest}
      />
      :
      <Spinner />;
  }
}

function mapStateToProps(state) {
  return {
    instructions: state.Exercises.currentSettings.shortCommand,
    settings: state.Exercises.currentSettings,
    tests: state.Exercises.tests.map((test, index) => Object.assign({}, test, { order: index + 1 })),
    selectedTopic: state.Exercises.selectedTopic,
    locale: state.User.currentUser.locale,
    patientId: state.Patient.patientId,
    id: state.Exercises.currentExercise._id,
    template: state.Exercises.currentExercise.template,
    total: state.Exercises.lastExerciseResult.total,
    exerciseName: state.Exercises.currentExercise.settings && state.Exercises.currentExercise.settings.exerciseName,
    exerciseState: state.Exercises.currentExerciseState,
    currentPatient: state.Patient.currentPatient
  };
}

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  resetCurrentExercise,
  showOverlay,
  saveResults,
  resetLastExerciseData,
  resetCurrentExerciseState,
  addExerciseFiveState
})(ExerciseFiveView);
