import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercise,
  resetCurrentExercise,
  showOverlay,
  updateCurrentSettings,
  updateTests,
  uploadBlob,
  saveResults,
  resetLastExerciseData
} from '../state/actions';

//utils data
import { shuffleArray, mergeSettingsOptions, patientExpiredCheck } from '../helpers';
import { exerciseOneSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';

//components
import ExerciseOne from '../components/ExerciseOne/index';
import Spinner from '../../common/components/spinner';
import NoAccessPage from './NoAccessPage';

class ExerciseOneView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired,
    showOverlay: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    tests: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      audioFirst: false,
      correct: true,
      timelineState: {
        completed: [],
        tests: [],
        correct: [],
        incorrect: [],
      }
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.triggerShowNextTest = this.triggerShowNextTest.bind(this);
  }

  componentWillMount() {
    const { locale, getExercise, updateCurrentSettings, selectedTopic, match: { params: { id } } } = this.props;
    this.props.resetLastExerciseData();
    getExercise(getQuery(id, 'exercise1'), getQuery(id, 'exercise1Views', selectedTopic))
      .then((exercise) => {
        const { settings } = exercise;
        const { updateTests, tests, showOverlay } = this.props;

        const payload = mergeSettingsOptions(settings, exerciseOneSettings[locale]);
        const audioFirst = id === '58b52de74f4c127ad2c21b2d';

        if (audioFirst) {
          this.setState({ audioFirst });
          showOverlay();
        }

        updateCurrentSettings(payload);
        if (settings.isRandom) {
          updateTests(shuffleArray(tests));
        }
        this.setState({
          timelineState: Object.assign({}, this.state.timelineState, {
            tests: this.props.tests.map(test => test.order)
          })
        });
      });
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
  }

  componentWillReceiveProps(nextProps) {
    const { formatMessage } = this.props.intl;
    const { updateTests, tests, backupTests } = this.props;
    if (this.props.settings.length && nextProps.settings[2].value !== this.props.settings[2].value) {
      if (nextProps.settings[2].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
        updateTests(shuffleArray(tests));
      } else {
        updateTests(backupTests);
      }
      this.setState({ currentTest: 0 });
      // this.setState({
      //   timelineState: {
      //     tests: tests.map(test => test.order),
      //     completed: this.state.timelineState.completed,
      //     correct: this.state.timelineState.correct,
      //     incorrect: this.state.timelineState.incorrect,
      //   }
      // });
    }
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  showNextTest(answer, nextNumber) {
    const { total, id, patientId, saveResults, history: { push }, showOverlay, tests } = this.props;
    const { correct, currentTest, timelineState } = this.state;
    const currentTestObject = tests[this.state.currentTest];

    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };

    if (currentTestObject.actions.toLowerCase() === 'correct & incorrect buttons') {
      results.variables.input.correct = answer;
    }

    const cut = tests.slice(currentTest + 1, tests.length);
    const next1 = cut.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);
    const next2 = tests.reduce((a, c) => a === null && !timelineState.completed.includes(c.order) ? c.order - 1 : a, null);

    const nextTest = (nextNumber || nextNumber === 0)
      ? nextNumber
      : next1 || next2;

    if (!(typeof nextNumber === 'number')) {
      saveResults(results);
    }

    this.setState({ correct: true });

    if (!this.state.audioFirst) {
      showOverlay(false);
    }

    if (total < tests.length - 1) {
      this.setState({
        currentTest: nextTest,
        timelineState: (typeof nextNumber === 'number') ? timelineState : {
          tests: timelineState.tests,
          completed: [...timelineState.completed, currentTest + 1],
          correct: correct ? [...timelineState.correct, currentTest + 1] : [...timelineState.correct],
          incorrect: correct ? [...timelineState.incorrect] : [...timelineState.incorrect, currentTest + 1],
        },
      });
      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
      showOverlay(false);
    }
  }

  triggerShowNextTest(answer, nextNumber) {
    // this.props.showOverlay();
    this.showNextTest(answer, nextNumber);
  }

  render() {
    const {
      id,
      tests,
      settings,
      updateCurrentSettings,
      currentDescription,
      uploadBlob,
      showOverlay,
      template,
      exerciseName,
      history: { push },
      intl: { formatMessage },
      currentPatient
    } = this.props;

    if (patientExpiredCheck(currentPatient && currentPatient.expires)) {
      return (<NoAccessPage />);
    }

    const { currentTest, timelineState } = this.state;

    const currentTestObject = tests[this.state.currentTest];

    return tests.length && settings.length && currentDescription ?
      <ExerciseOne
        exerciseName={exerciseName}
        audioTitle={currentTestObject.audioTitle}
        questionImage={currentTestObject.image}
        questionAudio={currentTestObject.audio}
        audioState={currentTestObject.audioState}
        actions={currentTestObject.actions}
        instructions={currentTestObject.originalText || currentDescription.shortCommand}
        showNextTest={this.triggerShowNextTest}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        uploadBlob={uploadBlob}
        showOverlay={showOverlay}
        push={push}
        audioFunctionality={tests.some(el => (
          el.actions.toLowerCase() === 'speech recognition' || el.actions === formatMessage(globalMessages.speechRecognition)
        ))}
        id={id}
        template={template}
        currentTest={currentTest}
        timelineState={timelineState}
      /> :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests.map((test, index) => Object.assign({}, test, { order: index + 1 })),
  currentDescription: state.Exercises.currentDescription,
  backupTests: state.Exercises.backupTests,
  selectedTopic: state.Exercises.selectedTopic,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id,
  template: state.Exercises.currentExercise.template,
  total: state.Exercises.lastExerciseResult.total,
  exerciseName: state.Exercises.currentExercise.settings && state.Exercises.currentExercise.settings.exerciseName,
  currentPatient: state.Patient.currentPatient
});

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  updateTests,
  resetCurrentExercise,
  showOverlay,
  uploadBlob,
  saveResults,
  resetLastExerciseData
})(injectIntl(ExerciseOneView));
