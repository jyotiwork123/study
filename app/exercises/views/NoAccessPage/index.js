import React, { Component } from 'react';

//redux
import { connect } from 'react-redux';
import { logout } from '../../../user/state/actions';

//components
import HeaderTemplate from '../../../common/components/HeaderTemplate';
import PaymentReminder from '../../components/PaymentReminder';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

class NoAccessPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const {
            userRole,
            userId,
            token,
            logout,
            intl: { formatMessage }
        } = this.props;

        return (
            <div>
                <div className={`mdl-layout mdl-js-layout mdl-layout--fixed-header`}>
                    <HeaderTemplate
                        title={formatMessage(globalMessages.exercises)}
                        userRole={userRole}
                        userId={userId}
                        token={token}
                        logout={logout}
                    />
                    <div style={{ marginTop: 70 }}>
                        <PaymentReminder />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    userId: state.User.currentUser.id,
    locale: state.User.currentUser.locale,
    userRole: state.User.currentUser.role,
    patientId: state.Patient.patientId,
    token: state.User.currentUser.token
});

export default connect(mapStateToProps, { logout })(injectIntl(NoAccessPage));
