import moment from 'moment';

export function filterByLanguage(exercises, languageFilter) {
  const filters = [];
  for (const key in languageFilter) {
    if (languageFilter[key]) {
      filters.push(key);
    }
  }
  if (!filters.length) {
    return exercises;
  }

  return exercises.reduce((result, exercise) => {
    if (exercise.settings.languageDomain.some((el) => filters.indexOf(el) > -1)) {
      return [...result, exercise];
    }
    return result;
  }, []);
}

export function shuffleArray(array) {
  const newArray = Object.assign([], array);
  for (let i = newArray.length; i; i--) {
    const j = Math.floor(Math.random() * i);
    [newArray[i - 1], newArray[j]] = [newArray[j], newArray[i - 1]];
  }
  return newArray;
}

//counts exercises amount of every filter value
export function setFiltersAmount(filtersObj, exercises) {
  const newFiltersObj = Object.assign({}, filtersObj);
  const filtersArr = Object.keys(filtersObj);

  exercises.forEach(exercise => {
    if (filtersArr.indexOf(exercise.settings.type) !== -1) {
      newFiltersObj[exercise.settings.type]++;
    }
    if (exercise.settings.languageDomain) {
      exercise.settings.languageDomain.forEach(language => {
        if (filtersArr.indexOf(language) !== -1) {
          newFiltersObj[language]++;
        }
      });
    }
  });

  return newFiltersObj;
}

//used to replace small images with bigger ones
export function replaceWithBigger(str) {
  if (!str) {
    return null;
  }
  try {
    JSON.parse(str);
    const url = JSON.parse(str).url;
    return url ? url.replace('small/', '').replace('_small', '') : '';
  } catch (e) {
    return str.replace('small/', '').replace('_small', '');
  }
}

//substitutes audio object with url
export function getAudioUrl(obj) {
  const audio = JSON.parse(obj);
  if (audio && audio.locale && audio.file) {
    return `https://dvm38m4gcon5n.cloudfront.net/speech/${audio.locale.toLowerCase()}/${audio.file.toLowerCase()}`;
  }

}

export function checkGetUserMedia() {
  if (navigator.mediaDevices.getUserMedia === undefined) {
    navigator.mediaDevices.getUserMedia = function (constraints) {
      // First get ahold of the legacy getUserMedia, if present
      const getUserMedia = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia);

      // Some browsers just don't implement it - return a rejected promise with an error
      if (!getUserMedia) {
        return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
      }

      // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
      return new Promise((resolve, reject) => {
        getUserMedia.call(navigator, constraints, resolve, reject);
      });
    };
  }
}

//this function detects if a browser supports touch events
function testStyles(rule, callback, nodes, testnames) {
  const mod = 'modernizr';
  const div = document.createElement('div');
  const body = document.body;
  let node;
  let docOverflow;

  if (parseInt(nodes, 10)) {
    // In order not to give false positives we create a node for each test
    // This also allows the method to scale for unspecified uses
    while (nodes--) {
      node = document.createElement('div');
      node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
      div.appendChild(node);
    }
  }

  const style = ['&#173;', '<style id="s', mod, '">', rule, '</style>'].join('');
  div.id = mod;
  // IE6 will false positive on some tests due to the style element inside the test div somehow interfering offsetHeight, so insert it into body or fakebody.
  (!body.fake ? div : body).innerHTML += style;
  body.appendChild(div);
  if (body.fake) {
    //avoid crashing IE8, if background image is used
    body.style.background = '';
    //Safari 5.13/5.1.4 OSX stops loading if ::-webkit-scrollbar is used and scrollbars are visible
    body.style.overflow = 'hidden';
    docOverflow = docElement.style.overflow;
    docElement.style.overflow = 'hidden';
    docElement.appendChild(body);
  }

  const ret = callback(div, rule);
  // If this is done after page load we don't want to remove the body so check if body exists
  if (body.fake) {
    body.parentNode.removeChild(body);
    docElement.style.overflow = docOverflow;
    // Trigger layout so kinetic scrolling isn't disabled in iOS6+
    docElement.offsetHeight;
  } else {
    div.parentNode.removeChild(div);
  }

  return !!ret;
}

export function supportsTouch() {
  let supports;
  if (('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch)) {
    supports = true;
  } else {
    const prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    const query = ['@media (', prefixes.join('touch-enabled),('), 'heartz', ')', '{#modernizr{top:9px;position:absolute}}'].join('');
    testStyles(query, (node) => {
      supports = node.offsetTop === 9;
    });
  }
  // const desktopBrowser = !~window.navigator.userAgent.toLowerCase().search('mobi');
  // console.log('desktopBrowser', desktopBrowser);
  // if (supports && desktopBrowser) {
  //   supports = false;
  // }
  // console.log(`${supports ? 'touch' : 'click'} events support initialized.`);
  return supports;
}
//end

//transforms settings values from what comes from the server to what is rendered
export function mergeSettingsOptions(currentSettings, options) {
  return options.map((select) => {
    const value = typeof currentSettings[select.mapping] === 'string' ? capitalise(currentSettings[select.mapping]) : currentSettings[select.mapping];

    return {
      label: select.label,
      options: select.options,
      value: updateValue(select.label, value)
    };
  });
}

function updateValue(label, value) {
  switch (label) {
    case 'Recommended length of practice':
      return value === 1 ? `${value} minute` : `${value} minutes`;
    case 'Hide correct answer':
    case 'Automatic validation':
    case 'Show in random order':
      return value === true ? 'Yes' : 'No';
    case 'Timeline':
      return value === true ? 'Enable' : 'Disable';
    case 'Audio ':
      return value === true ? 'Play audio before showing pictures' : 'Play audio at the same time with showing pictures';
    case 'Speech recognition':
      return value === true ? 'On' : 'Off';
    case 'Order of optional answers and sentence':
      return value === true ? 'Optional answers before the sentence' : 'Sentence before the optional answers';
    case 'Valikvastuste ja lause järjekord':
      return value === true ? 'Valikvastused enne lauset' : 'Lause enne valikvastuseid';

    case 'Kas peita õige vastus peale sellele vajutamist?':
    case 'Kas valikvastused tuleb ajada segamini?':
    case 'Kas vastuseid tuleb kontrollida automaatselt?':
      return value ? 'Jah' : 'Ei';
    case 'Audio':
      return value === true ? 'Mängi audio enne piltide näitamist' : 'Mängi audio piltidega samaaegselt';
    case 'Tähed':
      switch (value) {
        case 'Capital letters':
          return 'Algab suure tähega';
        case 'Uppercase':
          return 'Trükitähed';
        case 'As is':
          return 'Nii nagu sisestatud';
        default:
          return value;
      }
    case 'Näita õiget vastust nagu on':
      switch (value) {
        case 'Word/Sentence':
          return 'Sõna/Lause';
        case 'Dotted':
          return 'Punktiir';
        case 'Tühi':
          return 'Nii nagu sisestatud';
        default:
          return value;
      }
    case 'Järgmise harjutuse laadimise kiirus':
    case 'Heli mängimise kiirus':
      switch (value) {
        case 'Slow':
          return 'Aeglane';
        case 'Normal':
          return 'Keskmine';
        case 'Fast':
          return 'Kiire';
        default:
          return value;
      }
    case 'Soovitatav harjutamise kestvus':
      return value === 1 ? `${value} minut` : `${value} minutit`;
    case 'Peida õige vastus':
    case 'Automaatne kontroll':
    case 'Näita suvalises järjekorras':
    case 'Peida õige vastus peale sellele vajutamist':
      return value === true ? 'Jah' : 'Ei';
    case 'Ajajoon':
      return value === true ? 'Luba' : 'Keela';
    case 'Kõnetuvastus':
      return value === true ? 'Sees' : 'Väljas';
    default:
      return value;
  }
}
//end

export function capitalise(str) {
  const trimmedStr = str.trim();
  return trimmedStr[0].toUpperCase() + trimmedStr.substr(1).toLowerCase();
}

export function capitaliseWithDots(str) {
  return str.replace(/\.\s+([a-z])[^\.]|^(\s*[a-z])[^\.]/g, s => s.replace(/([a-z])/, s => s.toUpperCase()));
}

export function patientExpiredCheck(expires) {
  const today = new Date();
  return moment(expires) < today;
}

//returns delay value in ms
export function setExerciseDelay(value = '') {
  switch (value.toLowerCase()) {
    case 'slow':
    case 'aeglane':
      return 2000;
    case 'normal':
    case 'keskmine':
      return 1000;
    case 'fast':
    case 'kiire':
    default:
      return 0;
  }
}

//returns delay for audios in ms
export function setAudioDelay(value = '') {
  switch (value.toLowerCase()) {
    case 'slow':
    case 'aeglane':
      return 10000;
    case 'normal':
    case 'keskmine':
      return 5000;
    case 'fast':
    case 'kiire':
    default:
      return 3000;
  }
}
//returns text-transform value
export function setTextCase(value = '') {
  switch (value.toLowerCase()) {
    case 'capital letters':
    case 'algab suure tähega':
      return 'capitalize';
    case 'uppercase':
    case 'trükitähed':
      return 'uppercase';
    default:
      return 'none';
  }
}

//formats array of strings
export function formatArrOfStr(arr, format) {
  switch (format) {
    case 'capitalize':
      return arr.map(str => str ? capitalise(str) : '');
    case 'uppercase':
      return arr.map(str => str ? str.toUpperCase() : '');
    default:
      return arr;
  }
}

export function transformSpecSymb(str) {
  return [...str].map(char => {
    switch (char) {
      case 'ä':
        return 'a';
      case 'ü':
        return 'u';
      case 'õ':
      case 'ö':
        return 'o';
      case 'Ä':
        return 'A';
      case 'Ü':
        return 'U';
      case 'Õ':
      case 'Ö':
        return 'O';
      default:
        return char;
    }
  }).join('');
}

export function detectIE() {
  const ua = window.navigator.userAgent;

  const msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  const trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    const rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  const edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}

export function getCorrectTime(time, maxTimeTotal, currentCount, maxCount) {
  const maxRatio = 900; // maximum value for ratio between maxTimeTotal and incorrectTime
  const newTimeTotal = time / 60; // Geting a minutes
  /* Our arguments:
    time - total time for each column
    maxTimeTotal - maximum totalTime values of all data
    currentCount - count of correct/incorrect answers
    maxCount - correct + incorrect answers
  */
  // Find time for currentCount
  const currentTime = newTimeTotal * currentCount / maxCount;
  // Find ration between maxTimeTotal and our current time;
  const currentRatio = maxTimeTotal / currentTime;
  // if currentCount not 0 or another bed thing and currentRatio more then maxRatio
  // then we return ratio between maxTimeTotal and maxRatio
  // else we do not change our current time
  const result = currentCount && (currentRatio > maxRatio) ? maxTimeTotal / maxRatio : currentTime;
  // formating and make this number
  return +result.toPrecision(2);
}

export function formatDate(sec, locale) {
  const hours = moment.duration(sec * 1000).hours();
  const minutes = moment.duration(sec * 1000).minutes();
  const seconds = moment.duration(sec * 1000).seconds();

  const hh = hours < 10 ? `0${hours}` : hours;
  const mm = minutes < 10 ? `0${minutes}` : minutes;
  const ss = seconds < 10 ? `0${seconds}` : seconds;
  if (!hours && !minutes) {
    return `${ss} ${locale === 'et' ? 'sek' : 'sec'}`;
  } else if (!hours) {
    return `${mm}:${ss} min`;
  }
  return `${hh}:${mm}:${ss} h`;
}
