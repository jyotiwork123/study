import React, { Component } from 'react';

//components
import { ExerciseQuestion } from '../exerciseQuestion';
import DraggableAnswer from '../draggableAnswer';
import DropArea from '../dropArea';
import ExerciseHeader from '../ExerciseHeader/';
import CustomDragLayer from '../customDragLayer';

//utils, data
import { setTextCase } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseSix.css';


class ExerciseSix extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAnswers: false,
      checked: false,
      valid: null,
      manuallyChecked: false
    };

    this.correctSound = new Audio('../../sounds/correct.wav');
    this.incorrectSound = new Audio('../../sounds/incorrect.wav');

    this.showAnswers = this.showAnswers.bind(this);
    this.validateAnswers = this.validateAnswers.bind(this);
    this.checkAnswers = this.checkAnswers.bind(this);
    this.setHeight = this.setHeight.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    window.addEventListener('resize', this.setHeight, false);
  }

  componentWillReceiveProps(nextProps) {
    // console.log(this.props, nextProps)
    // this.correctSound.play();

    const { exerciseState, currentTest } = nextProps;

    const isCompletedTest = exerciseState.reduce((a, c) => c.number === currentTest || a, false);
    if (nextProps.questionImage === this.props.questionImage) {
      this.setState({
        showAnswers: true,
        checked: isCompletedTest,
        valid: null,
      });
    } else if (nextProps.currentTest !== this.props.currentTest) {
      this.setState({
        showAnswers: false,
        checked: isCompletedTest,
        valid: null,
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setHeight);
  }

  setHeight() {
    this.setState({ windowHeight: `${window.innerHeight}px` });
  }

  checkAnswers() {
    const { currentResults } = this.props;
    if (currentResults.every((el) => el !== null)) {
      this.validateAnswers();
      this.setState({ checked: true });
    }
  }

  validateAnswers() {
    const { currentResults, currentReferences } = this.props;
    if (currentResults.every((el, index) => el === currentReferences[index])) {
      this.correctSound.play();
      this.setState({ valid: true, manuallyChecked: true });
    } else {
      this.incorrectSound.play();
      this.setState({ valid: false, manuallyChecked: true });
    }
  }

  showAnswers() {
    if (!this.state.showAnswers && !this.state.manuallyChecked) {
      this.correctSound.play();
    }
    this.setState({ showAnswers: true });
  }

  renderAnswers(currentAnswers, moveAnswer) {
    const { exerciseState, currentTest, settings } = this.props;
    const answersForThisTestInState = exerciseState.reduce((a, c) => c.number === currentTest ? c.answers : a, []);
    const answersCase = setTextCase(settings[2].value);
    return !answersForThisTestInState.length && currentAnswers.map((answer, index) =>
      <DraggableAnswer
        moveAnswer={moveAnswer}
        answersCase={answersCase}
        answer={answer}
        index={index}
        key={index}
        restoreState={this.props.restoreState}
        backupState={this.props.backupState}
      />
    );
  }

  renderResults(answersAmount, currentResults, moveAnswer, currentReferences, autoValidation) {
    const { intl: { formatMessage }, settings } = this.props;
    let dropZone = [];
    for (let i = 0; i < answersAmount; i++) {
      if (currentResults[i]) {
        let borderColor;
        if (autoValidation.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase() || this.state.checked) {
          borderColor = currentReferences[i] === currentResults[i] ? { borderColor: '#5cb85c' } : { borderColor: 'red' };
        }
        const answersCase = setTextCase(settings[2].value);
        dropZone = dropZone.concat(
          <DropArea
            index={i}
            occupiedWith={currentResults[i]}
            key={i}
            rearrange={this.props.rearrange}
            restoreState={this.props.restoreState}
            whenHaveNested={{ minWidth: '10px' }}
            borderColor={borderColor}
          >
            <DraggableAnswer
              answersCase={answersCase}
              moveAnswer={moveAnswer}
              answer={currentResults[i]}
              index={i}
              key={i}
              restoreState={this.props.restoreState}
              backupState={this.props.backupState}
              whenNested={{ margin: '5px 6px' }}
              result
            />
          </DropArea>
        );
      } else {
        dropZone = dropZone.concat(
          <DropArea
            rearrange={this.props.rearrange}
            index={i}
            key={i}
            restoreState={this.props.restoreState}
          />
        );
      }
    }
    return dropZone;
  }

  renderButton() {
    const { showNextTest } = this.props;
    const { formatMessage } = this.props.intl;
    const { checked, valid } = this.state;
    if (checked) {
      return (
        <button
          className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button}`}
          onClick={() => { showNextTest(valid); }}
        >
          {formatMessage(globalMessages.next)}
        </button>
      );
    }
    return (
      <button
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button}`}
        onClick={this.checkAnswers}
      >
        {formatMessage(globalMessages.check)}
      </button>
    );
  }

  render() {
    const {
      exerciseName,
      questionTitle,
      questionImage,
      moveAnswer,
      currentAnswers,
      currentResults,
      currentReferences,
      answersAmount,
      settings,
      updateCurrentSettings,
      showNextTest,
      push,
      template,
      timelineState,
      currentTest,
      exerciseState,
      intl: { formatMessage }
    } = this.props;

    const currentAnswersInState = exerciseState.reduce((a, c) => c.number === currentTest ? c.answers : a, []);
    const hasOverlay = exerciseState && exerciseState.reduce((a, c) => (currentTest === c.number) || a, false) || false;
    const rendererResults = currentAnswersInState.length && currentAnswersInState || currentResults;
    const answersCase = setTextCase(settings[2].value);
    return (
      <div
        className={`mdl-layout mdl-js-layout mdl-layout--fixed-header ${styles.container}`}
        style={{ height: this.state.windowHeight || `${window.innerHeight}px` }}
      >
        <ExerciseHeader
          exerciseName={exerciseName}
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
          template={template}
          selectNextTest={showNextTest}
          timelineState={timelineState}
          currentTest={currentTest}
        />
        <div
          className={styles.content}
          onLoad={() => setTimeout(this.showAnswers, 200)}
          style={{ display: `${this.state.showAnswers ? 'block' : 'none'}` }}
        >
          <ExerciseQuestion questionImage={questionImage} />
          <div
            className={styles.answerArea}
            style={{ textTransform: answersCase }}
            ref={(el) => {
              if (el) {
                document.querySelector('.question-image').style.height = `${window.innerHeight - el.offsetHeight - 94}px`;
              }
            }}
          >
            <div className={`${styles.answerWrapper} ${styles.firstRow}`}>
              {this.renderAnswers(currentAnswers, moveAnswer)}
            </div>
            <div className={styles.answerWrapper}>
              {this.renderResults(answersAmount, rendererResults, moveAnswer, currentReferences, settings[3].value)}
            </div>
            <div className={`${styles.answerWrapper} ${styles.referenceWrapper}`}>
              {this.state.valid === false && currentReferences.join(' ')}
            </div>
            <div className={styles.answerWrapper}>
              {settings[3].value.toLowerCase() === formatMessage(globalMessages.no).toLowerCase() && this.renderButton(showNextTest)}
            </div>
            <CustomDragLayer answersCase={answersCase} />
          </div>
          {hasOverlay ? <div className={styles.overlayIfTestDone} /> : null}
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseSix);
