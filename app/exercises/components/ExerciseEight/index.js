import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import ExerciseHeader from '../ExerciseHeader/index';

// utils data
import listen from '../../../../images/listen.svg';
import speak from '../../../../images/speak.svg';
import { setAudioDelay, supportsTouch } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseEight.css';


class ExerciseEight extends Component {
  static propTypes = {
    playNext: PropTypes.func.isRequired,
    startExercise: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    testQuestions: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
    questionImage: PropTypes.string.isRequired,
    originalText: PropTypes.string.isRequired,
    questionTitle: PropTypes.string
  };

  static defaultProps = {
    questionTitle: ''
  };

  constructor(props) {
    super(props);
    this.state = { currentAudio: 0 };

    this.nextAudio = this.nextAudio.bind(this);
    this.renderQuestions = this.renderQuestions.bind(this);
    this.renderAudio = this.renderAudio.bind(this);
    this.renderImage = this.renderImage.bind(this);
  }

  componentDidMount() {
    this.setState({ currentAudio: 0 });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentTest !== this.props.currentTest) {
      this.setState({ currentAudio: 0 });
    }
  }

  nextAudio() {
    console.log(this);
  }

  renderQuestions(testQuestions) {
    return testQuestions.map(({ text }, index) =>
      <button
        id={`exercise-${text.toLowerCase()}-button`}
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}
        key={index}
      >
        {text}
      </button>
    );
  }

  renderAudio(testQuestions, playNext, settings) {
    const nextTestDelay = setAudioDelay(settings[1].value);

    return testQuestions.map(({ _id, text, key, url }, index) => (
      <audio
        className="exercise-audio"
        id={`exercise-${text.toLowerCase()}-audio`}
        preload="none"
        key={_id}
        onEnded={() => {
          if (!supportsTouch()) {
            setTimeout(playNext.bind(this, index + 1), nextTestDelay);
          }
        }}
      >
        <source
          src={url}
          type="audio/mp3"
        />
      </audio>
    ));
  }

  renderImage(questionImage) {
    const { formatMessage } = this.props.intl;

    switch (questionImage.toLowerCase()) {
      case formatMessage(globalMessages.listen).toLowerCase():
        return <img src={listen} className={styles.image} alt="listen" />;
      case formatMessage(globalMessages.repeat).toLowerCase():
        return < img src={speak} className={styles.image} alt="speak" />;
      case formatMessage(globalMessages.listenRepeat).toLowerCase():
        return (
          <div className={styles.innerImageWrapper}>
            <img src={listen} className={styles.image} alt="listen" />
            <img src={speak} className={styles.image} alt="speak" />
          </div>
        );
      default :
        return <div />;
    }
  }

  render() {
    const {
      exerciseName,
      questionTitle,
      testQuestions,
      questionImage,
      originalText,
      startExercise,
      playNext,
      settings,
      updateCurrentSettings,
      push,
      template,
      showNextTest,
      timelineState,
      currentTest,
      playOneAudio,
      intl: { formatMessage }
    } = this.props;

    const { currentAudio } = this.state;
    console.log(supportsTouch());
    return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header exercise8-wrapper">
        <ExerciseHeader
          exerciseName={exerciseName}
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
          template={template}
          selectNextTest={showNextTest}
          timelineState={timelineState}
          currentTest={currentTest}
        />
        <div className={styles.content}>
          <div className={styles.imageWrapper}>
            {this.renderImage(questionImage)}
          </div>
          <div className={styles.answerWrapper}>
            {testQuestions ? this.renderQuestions(testQuestions) : ''}
          </div>
          <div className="audio-wrapper">
            {testQuestions ? this.renderAudio(testQuestions, playNext, settings) : ''}
          </div>
          <p className={styles.originalText}>
            {originalText}
          </p>
        </div>
        <footer className={styles.footer}>
          {!supportsTouch() && <button
            id="start-exercise-button"
            className={`mdl-button mdl-js-button mdl-button--raised ${styles.startButton}`}
            onClick={() => {
              if (testQuestions) {
                startExercise(this.state.currentAudio);
              }
            }}
          >
            {formatMessage(globalMessages.start)}
          </button>}
          {supportsTouch() && <button
            id="start-exercise-button"
            className={`mdl-button mdl-js-button mdl-button--raised ${styles.startButton}`}
            onClick={() => {
              if (testQuestions) {
                playOneAudio(currentAudio);
                this.setState({
                  currentAudio: currentAudio + 1,
                });
              }
            }}
          >
            {currentAudio ? formatMessage(globalMessages.next) : formatMessage(globalMessages.start)}
          </button>}
        </footer>
      </div>
    );
  }
}

export default injectIntl(ExerciseEight);
