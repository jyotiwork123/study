import React, { Component } from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
// import { globalMessages } from '../../../common/dictionary/global';

//components
import StartingInstructionsFour from '../ExercisePreviewContent/StartingInstructionsFour';
import StartingInstructionsFourA from '../ExercisePreviewContent/StartingInstructionsFourA';
import StartingInstructionsFourB from '../ExercisePreviewContent/StartingInstructionsFourB';


//styles
import styles from './instructionsPopup.css';

class StutteringInstructionsModal extends Component {
    static PropTypes = {
        locale: PropTypes.string.isRequired,
        template: PropTypes.string.isRequired,
        isOpen: PropTypes.bool.isRequired,
        onConfirm: PropTypes.func.isRequired,
        previewSettings: PropTypes.array.isRequired,
    };
    static defaultProps = {
        overlayClassName: '',
        modalClassName: '',
    };

    componentDidMount() {
        window.componentHandler.upgradeDom();
    }

    render() {
        const { isOpen, locale, onConfirm, previewSettings, template } = this.props;

        console.log(previewSettings);

        return (
            <Modal
                isOpen={isOpen}
                shouldCloseOnOverlayClick
                contentLabel={'Instructions'}
            >
                <div className={styles.instruction}>

                    {
                        (template === '4') && <div>
                            <h3 className={styles.title}>{previewSettings.exerciseName}</h3>
                            <StartingInstructionsFour settings={previewSettings} locale={locale} />
                        </div>
                    }
                    {
                        (template === '4a') && <div>
                            <h3 className={styles.title}>{previewSettings.exerciseName}</h3>
                            <StartingInstructionsFourA settings={previewSettings} locale={locale} />
                        </div>
                    }
                    {
                        (template === '4b') && <div>
                            <h3 className={styles.title}>{previewSettings.exerciseName}</h3>
                            <StartingInstructionsFourB settings={previewSettings} locale={locale} />
                        </div>
                    }
                    {/* {locale === 'et' && <div>
                        <p>Istu mugavalt ja sirgelt. Jälgi oma rühti. Rahune. Lõdvesta lõug ja häälepaelad. Hinga rahulikult ja sügavalt sisse.</p>
                        <p>Kuula kõigepealt erineva kiirusega sisseloetud näidislauset (väga aeglane, aeglane ja tavaline kiirus).</p>
                        <p>Korda seejärel sisseloetud lauset võimalikult täpselt, hoides samasugust tempot. Alusta rääkimist võimalikult pehmelt. Venita kõiki täishäälikuid. Hoia oma suu ja keel pidevas liikumises. Hoia õhuvool ühtlase ja katkematuna. Ühenda hingamiste vahel silpe ja sõnu ilma pausideta ühtseks lauseks, nii et kogelust ei tekiks.</p>
                        <p>Kui lauset kordama hakkad, siis vajuta HARJUTA. Kui lause on lõppenud, siis vajuta KONTROLLI. Arvuti ütleb Sulle seejärel, kas mahtusid ajalimiiti või pead tempot lisama või maha võtma.</p>
                        <p>Sulle on lause kordamise ajal järgmised abimehed:</p>
                        <ul>
                            <li>Korratav lause tekstina</li>
                            <li>Visuaalne progressiriba</li>
                            <li>Metronoom takti hoidmiseks</li>
                            <li>Stopper kulunud aja mõõtmiseks.</li>
                        </ul>
                        <p>Mida rohkem abimehi välja lülitad, seda keerulisemaks ülesanne Sulle muutub. Head harjutamist!</p>

                    </div>} */}
                </div>
                <div className={styles.buttons}>
                    <button
                        className={`mdl-button mdl-js-button mdl-button--raised mdl-color--green-500 ${styles.button}`}
                        onClick={onConfirm}
                    >Ok</button>
                </div>
            </Modal>
        );
    }
}

export default injectIntl(StutteringInstructionsModal);

