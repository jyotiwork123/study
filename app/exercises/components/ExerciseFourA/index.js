import React, { Component } from 'react';
import PropTypes from 'prop-types';

//utils, data
import difference from 'lodash/difference';
import { replaceWithBigger } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';


//components
import ExerciseHeader from '../ExerciseHeader/index';
import StutteringInstructionsModal from '../StutteringInstructionsModal';

//styles
import styles from './exerciseFourA.css';


class ExerciseFourA extends Component {
  static propTypes = {
    testIndex: PropTypes.number.isRequired,
    showNextTest: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    settings: PropTypes.array.isRequired,
    questionTitle: PropTypes.string,
    questionImage: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    questionAudio: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ])
  };
  static defaultProps = {
    questionTitle: '',
    questionImage: '',
    questionAudio: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      testChanged: false,
      answers: [],
      correct: true,
      check: false,
      currentFile: 0,
      playing: false,
      instructionsModalVisible: false,

      practiceResult: '',
      practicing: false,
      playedOnce: false
    };

    this.correctSound = new Audio('../../sounds/correct.wav');
    this.incorrectSound = new Audio('../../sounds/incorrect.wav');
    this.testChanged = this.testChanged.bind(this);
    this.playAudio = this.playAudio.bind(this);
    this.toggleInstructionsModal = this.toggleInstructionsModal.bind(this);

    this.calculateTime = this.calculateTime.bind(this);

    this.validateAnswer = this.validateAnswer.bind(this);
    this.validateResult = this.validateResult.bind(this);

    // this.togglePlay = this.togglePlay.bind(this);
  }

  componentDidMount() {
    const { settings, intl: { formatMessage } } = this.props;
    this.setState({
      correct: false,
      testChanged: true,
      audioFirst: settings[2].value === formatMessage(globalMessages.audioFirst)
    });
    window.componentHandler.upgradeDom();
  }

  componentWillReceiveProps(nextProps) {
    const { settings, exerciseState, currentTest, intl: { formatMessage } } = nextProps;
    if (nextProps.testIndex !== this.props.testIndex) {
      this.setState({
        tempo: 0,
        check: false,
        testChanged: true,
        answers: exerciseState.reduce((a, c) => c.number === currentTest ? c.answers : a, []),
        correct: true,
        audioFirst: settings[2].value === formatMessage(globalMessages.audioFirst),
        practicing: false,
        playing: false,
        practiceResult: ''
      });
    } else {
      this.setState({ testChanged: false });
    }
  }

  validateAnswer(target, index) {
    const { answers } = this.state;
    const { actions } = this.props;
    const correctResults = Object.keys(actions);
    if (correctResults.indexOf(index.toString()) > -1) {
      this.correctSound.play();
      /* eslint-disable */
      target.style.border = '5px solid #5cb85c';
      /* eslint-enable */
      this.setState({ answers: [...answers, index.toString()], check: true });
      setTimeout(this.validateResult, 1000);
    } else {
      this.incorrectSound.play();
      /* eslint-disable */
      target.style.border = '5px solid red';
      /* eslint-enable */
      this.validateResult();
      this.setState({ answers: [...answers, index.toString()], correct: false });
    }
  }

  validateResult() {
    const { actions, showNextTest, addResultsToExerciseState } = this.props;
    const { answers, correct } = this.state;
    const correctResults = Object.keys(actions);

    if (!difference(correctResults, answers).length) {
      addResultsToExerciseState(answers);
      showNextTest(correct);
    }
  }

  testChanged() {
    this.setState({ testChanged: false });
  }

  playAudio(file) {
    const { questionAudio } = this.props;
    const { playing, currentFile } = this.state;
    const practiceAudio0 = document.getElementById('audio-player-0');
    const practiceAudio1 = document.getElementById('audio-player-1');

    practiceAudio0.addEventListener('ended', () => {
      this.setState({ playing: false });
    });

    practiceAudio1.addEventListener('ended', () => {
      this.setState({ playing: false });
    });


    this.setState({ currentFile: file });
    if (!playing) {
      document.getElementById(`audio-player-${file}`).play();
      this.setState({ playing: true });
    }

    if (currentFile !== file) {
      if (file === 0) {
        practiceAudio1.pause();
        practiceAudio1.currentTime = 0.0;
        practiceAudio0.play();
      } else {
        practiceAudio0.pause();
        practiceAudio0.currentTime = 0.0;
        practiceAudio1.play();
      }
    } else if (playing && currentFile === file) {
      document.getElementById(`audio-player-${file}`).pause();
      this.setState({ playing: false });
    }
  }

  // togglePlay() {
  //   const { playing } = this.state;
  //   const practiceAudio = document.getElementById('audio-player');
  //   practiceAudio.removeEventListener('ended', () => { });
  //   practiceAudio.addEventListener('ended', () => {
  //     if (!this.state.playedOnce) {
  //       setTimeout(() => {
  //         practiceAudio.play();
  //         this.setState({ playedOnce: true });
  //       }, 2000);
  //     } else {
  //       this.setState({ playing: false });
  //     }
  //   });
  //   if (!playing) {
  //     practiceAudio.play();
  //     this.setState({ playing: true });
  //   } else {
  //     practiceAudio.pause();
  //     this.setState({ playing: false });
  //   }
  // }

  toggleInstructionsModal() {
    this.setState({ instructionsModalVisible: !this.state.instructionsModalVisible });
  }

  calculateTime(currentTime, full) {
    let minutes = parseInt((currentTime / 60), 0) % 60;
    let seconds = parseInt((currentTime - minutes * 60), 0).toString().substr(0, 2);
    const deciSeconds = parseFloat((currentTime - minutes * 60), 2).toString().substr(2, 2) || '00';

    if (minutes < 10) { minutes = '0' + minutes; }
    if (seconds < 10) { seconds = '0' + seconds; }

    let time = '';

    if (full) {
      time = minutes + ':' + seconds + ':' + deciSeconds;
    } else {
      time = minutes + ':' + seconds;
    }
    return time;
  }

  renderButton() {
    const { showNextTest, testIndex, timelineState: { tests } } = this.props;
    const { formatMessage } = this.props.intl;
    return (
      <button
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button}`}
        onClick={() => { showNextTest(true, true); }}
      >
        {testIndex !== tests.length - 1 ? formatMessage(globalMessages.next) : formatMessage(globalMessages.finish)}
      </button>
    );
  }

  render() {
    const { audioFirst, playing, testChanged, currentFile, instructionsModalVisible } = this.state;
    const {
      questionAudio,
      questionImage,
      settings,
      previewSettings,
      instructions,
      updateCurrentSettings,
      showImages,
      push,
      template,
      timelineState,
      showNextTest,
      setDisplayMode,
      currentTest,
      exerciseName,
      displayMode,
      showPreviousButton
    } = this.props;
    const { formatMessage } = this.props.intl;
    const imageStyles = {
      backgroundSize: 'contain',
      background: `url('${questionImage[0].url}') center center / contain no-repeat`
    };

    // console.log(setDisplayMode && (displayMode === 'all' || displayMode === 'word'));
    // console.log(displayMode);

    return (
      <div
        className="mdl-layout mdl-js-layout mdl-layout--fixed-header exercise-one-wrapper"
      >
        <ExerciseHeader
          exerciseName={exerciseName}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          // audio={questionAudio}
          instructions={instructions}
          showImages={showImages}
          push={push}
          template={template}
          timelineState={timelineState}
          selectNextTest={showNextTest}
          currentTest={currentTest}
          testChanged={testChanged}
          audioFirst={audioFirst}
          testChangedFunc={this.testChanged}
        />
        <div
          // key={questionAudio}
          className={styles.content}
        >
          <div className="mdl-grid" style={{ height: '100%' }}>
            <div className={`mdl-cell mdl-cell--12-col ${styles.topContainer}`}>
              <div className={'mdl-cell mdl-cell--2-col'} />
              <div className={`mdl-cell mdl-cell--2-col ${styles.instructionsButtonContainer}`}>
                <button
                  className={`mdl-button mdl-js-button mdl-button--raised ${styles.instructionsButton}`}
                  onClick={this.toggleInstructionsModal}
                >
                  {formatMessage(globalMessages.instructions)}
                </button>
              </div>
            </div>
            <div className={'mdl-cell mdl-cell--12-col'}>
              <div className={`${styles.exerciseContainer}`}>
                <div className={`${styles.imageContainer}`} style={imageStyles}>
                  {/* {
                    questionImage[0] && questionImage[0].url ?
                      <img
                        role='presentation'
                        src={questionImage[0].url}
                        className={`${styles.exerciseFourimage}`}
                      /> : null
                  } */}
                </div>
                <div className={`${styles.audioInner}`}>
                  <audio
                    autoPlay={false}
                    loop={false}
                    // controls
                    id={'audio-player-0'}
                    src={questionAudio[0].url}
                  >
                    {formatMessage(globalMessages.browserNotSupportFile)}
                  </audio>
                  <audio
                    autoPlay={false}
                    loop={false}
                    // controls
                    id={'audio-player-1'}
                    src={questionAudio[1].url}
                  >
                    {formatMessage(globalMessages.browserNotSupportFile)}
                  </audio>
                </div>
                <div className={`mdl-cell mdl-cell--12-col ${styles.buttonsContainer}`}>
                  <button
                    className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button} ${styles.audioButton} ${playing && currentFile === 0 ? styles.audioButtonActive : ''}`}
                    onClick={() => this.playAudio(0)}
                  // disabled={playing && currentFile === 1}
                  >
                    {formatMessage(globalMessages.normalStart)}
                  </button>
                  <span className={`${styles.instruction}`}>{instructions}</span>
                  <button
                    className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button} ${styles.audioButton} ${playing && currentFile === 1 ? styles.audioButtonActive : ''}`}
                    onClick={() => this.playAudio(1)}
                  // disabled={playing && currentFile === 0}
                  >
                    {formatMessage(globalMessages.softStart)}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer className={styles.footer}>
          <div className={`mdl-cell mdl-cell--12-col ${styles.footer}`}>
            <div className={`mdl-cell mdl-cell--3-col ${styles.footerItem}`}>
              {
                setDisplayMode && displayMode === 'sentence' && <button
                  className={`mdl-button mdl-js-button mdl-button--raised ${styles.button} ${displayMode !== 'word' ? 'mdl-button--accent' : ''}`}
                  disabled={displayMode === 'word'}
                  onClick={() => { setDisplayMode('word'); }}
                >
                  {formatMessage(globalMessages.practiceWords)}
                </button>
              }
            </div>
            <div className={`mdl-cell mdl-cell--6-col ${styles.footerItem}`}>
              {
                setDisplayMode && showPreviousButton && <button
                  className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}
                  onClick={() => { showNextTest(true, false); }}
                >
                  {formatMessage(globalMessages.previous)}
                </button>
              }
              {
                this.renderButton(showNextTest)
              }
            </div>
            <div className={`mdl-cell mdl-cell--3-col ${styles.footerItem}`}>
              {
                setDisplayMode && displayMode !== 'sentence' && <button
                  className={`mdl-button mdl-js-button mdl-button--raised ${styles.button} ${displayMode !== 'sentence' ? 'mdl-button--accent' : ''}`}
                  disabled={displayMode === 'sentence'}
                  onClick={() => { setDisplayMode('sentence'); }}
                >
                  {formatMessage(globalMessages.practiceSentences)}
                </button>
              }
            </div>
          </div>
        </footer>
        {instructionsModalVisible &&
          <StutteringInstructionsModal
            isOpen={instructionsModalVisible}
            locale={'et'}
            template={'4a'}
            previewSettings={previewSettings}
            onConfirm={this.toggleInstructionsModal}
          />}
      </div>
    );
  }
}

export default injectIntl(ExerciseFourA);
