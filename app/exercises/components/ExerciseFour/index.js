import React, { Component } from 'react';
import PropTypes from 'prop-types';

//utils, data
import difference from 'lodash/difference';
// import { replaceWithBigger } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';


//components
import ExerciseHeader from '../ExerciseHeader/index';
import PieChart from 'react-minimal-pie-chart';
import Metronome from '../../../common/components/Metronome';
import StutteringInstructionsModal from '../StutteringInstructionsModal';

//styles
import styles from './exerciseFour.css';


class ExerciseFour extends Component {
  static propTypes = {
    testIndex: PropTypes.number.isRequired,
    showNextTest: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    settings: PropTypes.array.isRequired,
    questionTitle: PropTypes.string,
    questionImage: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    questionAudio: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ])
  };
  static defaultProps = {
    questionTitle: '',
    questionImage: '',
    questionAudio: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      testChanged: false,
      answers: [],
      correct: true,
      shouldImagesRender: false,
      check: false,
      tempo: 0,
      audioLength: 0,
      played: 0,
      playing: false,

      practiceResult: '',
      resultColor: 'initial',
      voiceOn: false,
      visualGuidanceOn: true,
      metronomeOn: true,
      timerOn: true,
      instructionsModalVisible: false,
      practicing: false,
      playedOnce: false,
      progressIndexesAray: [],

      timeCorrect: 0,
      timeFailed: 0,
    };

    this.correctSound = new Audio('../../sounds/correct.wav');
    this.incorrectSound = new Audio('../../sounds/incorrect.wav');
    this.testChanged = this.testChanged.bind(this);

    this.calculateTime = this.calculateTime.bind(this);
    this.initProgressBar = this.initProgressBar.bind(this);

    this.validateAnswer = this.validateAnswer.bind(this);
    this.validateResult = this.validateResult.bind(this);
    this.addBorder = this.addBorder.bind(this);
    this.shouldImagesRender = this.shouldImagesRender.bind(this);

    this.changeTempo = this.changeTempo.bind(this);

    this.toggleInstructionsModal = this.toggleInstructionsModal.bind(this);
    this.toggleVoise = this.toggleVoise.bind(this);
    this.toggleVisualGuidance = this.toggleVisualGuidance.bind(this);
    this.toggleMetronome = this.toggleMetronome.bind(this);
    this.toggleTimer = this.toggleTimer.bind(this);

    this.togglePlay = this.togglePlay.bind(this);
    this.togglePractice = this.togglePractice.bind(this);
    this.checkPractice = this.checkPractice.bind(this);
    this.stopMetronome = this.stopMetronome.bind(this);

    this.sendResultsAndNext = this.sendResultsAndNext.bind(this);
  }

  componentDidMount() {
    const { settings, instructions, intl: { formatMessage } } = this.props;
    this.setState({
      correct: false,
      testChanged: true,
      // shouldImagesRender: settings[2].value !== formatMessage(globalMessages.audioFirst),
      audioFirst: settings[2].value === formatMessage(globalMessages.audioFirst)
    });
    window.componentHandler.upgradeDom();
    this.initProgressBar();
    this.createTextIndexArray(instructions);
  }

  componentWillReceiveProps(nextProps) {
    const { settings, exerciseState, currentTest, intl: { formatMessage } } = nextProps;
    this.createTextIndexArray(nextProps.instructions);

    if (nextProps.testIndex !== this.props.testIndex) {
      this.stopMetronome();
      const practiceAudio = document.getElementById('audio-picker');
      practiceAudio.pause();
      practiceAudio.currentTime = 0;

      this.setState({
        tempo: 0,
        check: false,
        testChanged: true,
        shouldImagesRender: settings[2].value !== formatMessage(globalMessages.audioFirst),
        answers: exerciseState.reduce((a, c) => c.number === currentTest ? c.answers : a, []),
        correct: true,
        audioFirst: settings[2].value === formatMessage(globalMessages.audioFirst),
        practicing: false,
        playing: false,
        practiceResult: ''
      }, () => {
        this.initProgressBar();
      });
    } else {
      this.setState({ testChanged: false });
    }
  }

  addBorder(index) {
    // geting number of current test, previews results and correct results
    const { currentTest, exerciseState, actions } = this.props;
    // geting results from state
    const currentAnswers = exerciseState.reduce((acc, c) => c.number === currentTest ? c.answers : acc, []);
    // if results from state includes our img (by index)
    if (currentAnswers.includes(index.toString())) {
      // geting correct results
      const correctResults = Object.keys(actions);
      // if correct results includes our img (by index)
      if (correctResults.includes(index.toString())) {
        return '5px solid #5cb85c';
      }
      return '5px solid red';
    }
    return '5px solid transparent';
  }

  validateAnswer(target, index) {
    const { answers } = this.state;
    const { actions } = this.props;
    const correctResults = Object.keys(actions);
    if (correctResults.indexOf(index.toString()) > -1) {
      this.correctSound.play();
      /* eslint-disable */
      target.style.border = '5px solid #5cb85c';
      /* eslint-enable */
      this.setState({ answers: [...answers, index.toString()], check: true });
      setTimeout(this.validateResult, 1000);
    } else {
      this.incorrectSound.play();
      /* eslint-disable */
      target.style.border = '5px solid red';
      /* eslint-enable */
      this.validateResult();
      this.setState({ answers: [...answers, index.toString()], correct: false });
    }
  }

  validateResult() {
    const { actions, showNextTest, addResultsToExerciseState } = this.props;
    const { answers, correct } = this.state;
    const correctResults = Object.keys(actions);

    if (!difference(correctResults, answers).length) {
      addResultsToExerciseState(answers);
      showNextTest(correct);
    }
  }

  testChanged() {
    this.setState({ testChanged: false });
  }

  stopMetronome() {
    this.setState({ testChanged: false });
  }
  shouldImagesRender() {
    this.setState({ shouldImagesRender: true });
  }


  changeTempo(tempo) {
    const practiceAudio = document.getElementById('audio-picker');
    practiceAudio.pause();
    practiceAudio.currentTime = 0;
    this.setState({ tempo, practicing: false, playing: false }, () => {
      this.initProgressBar();
    });
  }

  toggleVoise() {
    this.setState({ voiceOn: !this.state.voiceOn }, () => {
      if (this.state.voiceOn) {
        document.getElementById('audio-picker').muted = false;
      } else {
        document.getElementById('audio-picker').muted = true;
      }
    });
  }
  toggleVisualGuidance() {
    this.setState({ visualGuidanceOn: !this.state.visualGuidanceOn });
  }
  toggleMetronome() {
    this.setState({ metronomeOn: !this.state.metronomeOn });
  }
  toggleTimer() {
    this.setState({ timerOn: !this.state.timerOn });
  }

  toggleInstructionsModal() {
    this.setState({ instructionsModalVisible: !this.state.instructionsModalVisible });
  }

  togglePlay() {
    const { playing } = this.state;
    const practiceAudio = document.getElementById('audio-picker');
    practiceAudio.removeEventListener('ended', () => { });
    practiceAudio.addEventListener('ended', () => {
      this.setState({ playing: false });
    });
    if (!playing) {
      practiceAudio.removeEventListener('ended', () => { });
      practiceAudio.play();
      this.setState({ playing: true });
    } else {
      practiceAudio.pause();
      this.setState({ playing: false });
    }
  }

  togglePractice() {
    const { practicing } = this.state;
    const practiceAudio = document.getElementById('audio-picker');

    // practiceAudio.addEventListener('ended', () => {
    //   if (!this.state.playedOnce) {
    //     practiceAudio.play();
    //   }
    //   this.setState({ playedOnce: true });
    // });

    if (!practicing) {
      practiceAudio.addEventListener('ended', () => {
        if (!this.state.playedOnce) {
          practiceAudio.muted = true;
          practiceAudio.play();
        }
        this.setState({ playedOnce: true });
      });

      practiceAudio.currentTime = 0;
      practiceAudio.muted = true;
      this.setState({ practicing: true, playedOnce: false, practiceResult: '' });
      practiceAudio.play();
    } else {
      practiceAudio.removeEventListener('ended', () => { });
      practiceAudio.muted = false;
      practiceAudio.pause();
      this.checkPractice(practiceAudio.currentTime, practiceAudio.duration);
      practiceAudio.currentTime = 0;
      this.setState({ practicing: false, voiceOn: false });
    }
  }

  checkPractice(currentTime, duration) {
    const { intl: { formatMessage } } = this.props;
    if (!this.state.playedOnce && currentTime / duration < 0.85) {
      this.setState({ practiceResult: formatMessage(globalMessages.tooFast), timeFailed: this.state.timeFailed + 1, resultColor: 'red' });
      this.incorrectSound.play();
    } else if (this.state.playedOnce && currentTime / duration > 0.15) {
      this.setState({ practiceResult: formatMessage(globalMessages.tooSlow), timeFailed: this.state.timeFailed + 1, resultColor: 'red' });
      this.incorrectSound.play();
    } else {
      this.setState({ practiceResult: formatMessage(globalMessages.nailedIt), timeCorrect: this.state.timeCorrect + 1, resultColor: 'initial' });
      this.correctSound.play();
    }
  }

  calculateTime(currentTime, full) {
    let minutes = parseInt((currentTime / 60), 0) % 60;
    let seconds = parseInt((currentTime - minutes * 60), 0).toString().substr(0, 2);
    const deciSeconds = parseFloat((currentTime - minutes * 60), 2).toString().substr(2, 2) || '00';

    if (minutes < 10) { minutes = '0' + minutes; }
    if (seconds < 10) { seconds = '0' + seconds; }

    let time = '';

    if (full) {
      time = minutes + ':' + seconds + ':' + deciSeconds;
    } else {
      time = minutes + ':' + seconds;
    }
    return time;
  }

  initProgressBar() {
    const audioPlayer = document.getElementsByTagName('audio')[0];
    audioPlayer.onloadedmetadata = () => {
      this.setState(
        {
          audioLength: !isNaN(audioPlayer.duration) ? audioPlayer.duration : 0
        }
      );
    };
    this.setState(
      {
        played: audioPlayer.currentTime
      }
    );
  }

  sendResultsAndNext(next) {
    const { showNextTest } = this.props;
    const { timeCorrect, timeFailed } = this.state;

    showNextTest(timeCorrect > 0, next, timeCorrect, timeFailed);
  }

  createTextIndexArray(instructions) {
    // const { instructions } = this.props;
    const result = [];

    instructions.split(' ').forEach((word, wordIndex) => {
      word.split('').forEach((symbol, symbolIndex) => {
        result.push(wordIndex + '-' + symbolIndex);
      });
    });

    this.setState({ progressIndexesAray: result });
  }

  renderButton() {
    const { formatMessage } = this.props.intl;
    const { testIndex, timelineState: { tests }, template } = this.props;
    console.log('yarr', this.props);

    return (
      <button
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button}`}
        onClick={() => { this.sendResultsAndNext(true); }}
      >
        {(testIndex === tests.length - 1) || template === '4a'
          ? formatMessage(globalMessages.finish)
          : formatMessage(globalMessages.next)}
      </button>
    );
  }


  render() {
    const { audioFirst, testChanged, tempo, audioLength, played, playing, practicing, voiceOn, visualGuidanceOn, metronomeOn, timerOn, instructionsModalVisible } = this.state;
    const {
      questionAudio,
      settings,
      previewSettings,
      instructions,
      updateCurrentSettings,
      showImages,
      push,
      template,
      timelineState,
      showNextTest,
      setDisplayMode,
      currentTest,
      exerciseName,
      displayMode,
      showPreviousButton,
    } = this.props;
    const { formatMessage } = this.props.intl;

    // console.log(audioLength, played);

    const chartData = timerOn ? [
      { value: played, key: 1, color: 'grey' },
      { value: audioLength - played, key: 2, color: 'rgba(0,0,0, 0.2)' }
    ] : [
        { value: audioLength - played, key: 2, color: 'rgba(0,0,0, 0.2)' }
      ];
    // console.log('questionAudio', questionAudio)

    return (
      <div
        className="mdl-layout mdl-js-layout mdl-layout--fixed-header exercise-one-wrapper"
      >
        <ExerciseHeader
          // renderImages={this.shouldImagesRender}
          exerciseName={exerciseName}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          // audio={questionAudio}
          instructions={instructions}
          showImages={showImages}
          push={push}
          template={template}
          timelineState={timelineState}
          selectNextTest={showNextTest}
          currentTest={currentTest}
          testChanged={testChanged}
          audioFirst={audioFirst}
          testChangedFunc={this.testChanged}
        />
        <div
          key={questionAudio}
          className={styles.content}
        >
          {/* {shouldImagesRender && this.renderImages()} */}
          <div>{
          }</div>
          <div className="mdl-grid">
            <div className={`mdl-cell mdl-cell--12-col ${styles.topContainer}`}>
              <div className={'mdl-cell mdl-cell--2-col'} />
              {
                questionAudio.length > 1 ? <div className={`mdl-cell mdl-cell--8-col ${styles.changeTempoContainer}`}>
                  <button
                    className={`mdl-button mdl-js-button mdl-button--raised ${styles.changeTempoItem} ${tempo === 0 ? 'mdl-button--colored' : ''}`}
                    onClick={() => this.changeTempo(0)}
                  >
                    {formatMessage(globalMessages.normalSpeed)}
                  </button>
                  <button
                    className={`mdl-button mdl-js-button mdl-button--raised ${styles.changeTempoItem} ${tempo === 1 ? 'mdl-button--colored' : ''}`}
                    onClick={() => this.changeTempo(1)}
                  >
                    {formatMessage(globalMessages.slowSpeed)}
                  </button>
                  {
                    questionAudio.length > 2 && <button
                      className={`mdl-button mdl-js-button mdl-button--raised ${styles.changeTempoItem} ${tempo === 2 ? 'mdl-button--colored' : ''}`}
                      onClick={() => this.changeTempo(2)}
                    >
                      {formatMessage(globalMessages.extraSlowSpeed)}
                    </button>
                  }
                </div>
                  :
                  <div className={`mdl-cell mdl-cell--8-col ${styles.changeTempoContainer}`} />
              }
              <div className={`mdl-cell mdl-cell--2-col ${styles.instructionsButtonContainer}`}>
                <button
                  className={`mdl-button mdl-js-button mdl-button--raised ${styles.instructionsButton}`}
                  onClick={this.toggleInstructionsModal}
                >
                  {formatMessage(globalMessages.instructions)}
                </button>
              </div>
            </div>

            <div className={'mdl-cell mdl-cell--12-col'}>
              <div className={`${styles.audioContainer} ${styles.audioContainerMain}`}>
                <div className={`${styles.audioInner}`}>
                  <div className={`${styles.audioInnerButtonBlock}`}>
                    <button
                      className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button} ${styles.audioButton}`}
                      onClick={this.togglePlay}
                      disabled={practicing}
                    >
                      {this.state.playing && audioLength !== played
                        ? <i className={`material-icons ${styles.buttonIcon}`}>pause</i>
                        : <i className={`material-icons ${styles.buttonIcon}`}>play_arrow</i>}
                      {this.state.playing && audioLength !== played ? formatMessage(globalMessages.pauseSample) : formatMessage(globalMessages.startSample)}
                    </button>
                    <div>
                      <span>
                        {timerOn ? this.calculateTime(played) : '00:00'}
                      </span>{' / '}
                      <span>
                        {this.calculateTime(audioLength)}
                      </span>
                    </div>
                  </div>
                  <audio
                    key={tempo}
                    className={styles.picker}
                    onTimeUpdate={this.initProgressBar}
                    // tabIndex={'-1'}
                    autoPlay={false}
                    loop={false}
                    // controls
                    id={'audio-picker'}
                    src={questionAudio[tempo].url}
                  >
                    {formatMessage(globalMessages.browserNotSupportFile)}
                  </audio>
                  <div className={`mdl-cell mdl-cell--8-col ${styles.textProgressContainer}`}>
                    {
                      instructions.split(' ').map((word, wordIndex) => <div key={wordIndex} className={styles.wordContainer}>
                        <div className={styles.word}>{word.replace('_', '')}</div>
                        <div className={styles.wordProgress}>
                          {
                            word.split('').map((symbol, symbolIndex) => <div
                              className={styles.wordProgressItem}
                              key={symbolIndex}
                              style={{
                                background: visualGuidanceOn && this.state.progressIndexesAray.indexOf(wordIndex + '-' + symbolIndex) / this.state.progressIndexesAray.length < played / audioLength
                                  ? 'rgb(63,81,181)' : null
                              }}
                            />)
                          }
                        </div>
                      </div>)
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="mdl-grid">
            <div className={'mdl-cell mdl-cell--12-col'}>
              <div className={`${styles.audioContainer}`}>
                <div
                  className={`mdl-cell mdl-cell--12-col ${styles.result}`}
                  style={{ color: this.state.resultColor }}
                >{this.state.practiceResult}</div>
                <div className={`${styles.audioInner}`}>
                  <div className={`mdl-cell mdl-cell--8-col ${styles.chartBlock}`}>
                    <Metronome
                      key={currentTest}
                      audible={metronomeOn}
                      visual={visualGuidanceOn}
                      togglePractice={this.togglePractice}
                      playingSample={playing}
                    />
                  </div>
                  {/* <div className={`mdl-cell mdl-cell--1-col ${styles.chartBlock}`} /> */}
                  <div className={`mdl-cell mdl-cell--4-col ${styles.chartBlock}`}>
                    {
                      chartData.length !== 0 && <div className={styles.resutsChartWrapper}>
                        <PieChart
                          data={chartData}
                          lineWidth={25}
                          startAngle={-90}
                          radius={50}
                        />
                        {/* <span className={styles.resultPercentage}>
                          {timerOn ? this.calculateTime(played) : '00:00'}
                        </span><br /> */}
                        {/* <span className={styles.resultLeft}>
                  {this.calculateTime(audioLength - played)}
                </span> */}
                      </div>
                    }
                  </div>
                </div>
              </div>
            </div>
            <div className={`mdl-cell mdl-cell--12-col ${styles.toggleContainer}`}>
              <div className={`mdl-cell mdl-cell--6-col ${styles.toggleBlock}`}>
                <label className={`${styles.switch}`} htmlFor='switch1'>
                  <input type='checkbox' id='switch1' checked={voiceOn} onChange={() => this.toggleVoise()} />
                  <span className={`${styles.slider} ${styles.sliderRound}`}>{}</span>
                </label>
                <span>{`${formatMessage(globalMessages.voice)}`}</span>
              </div>
              <div className={`mdl-cell mdl-cell--6-col ${styles.toggleBlock}`}>
                <label className={`${styles.switch}`} htmlFor='switch2'>
                  <input type='checkbox' id='switch2' checked={visualGuidanceOn} onChange={() => this.toggleVisualGuidance()} />
                  <span className={`${styles.slider} ${styles.sliderRound}`}>{}</span>
                </label>
                <span>{`${formatMessage(globalMessages.visualGuidance)}`}</span>
              </div>
              <div className={`mdl-cell mdl-cell--6-col ${styles.toggleBlock}`}>
                <label className={`${styles.switch}`} htmlFor='switch3'>
                  <input type='checkbox' id='switch3' checked={metronomeOn} onChange={() => this.toggleMetronome()} />
                  <span className={`${styles.slider} ${styles.sliderRound}`}>{}</span>
                </label>
                <span>{`${formatMessage(globalMessages.metronome)}`}</span>
              </div>
              <div className={`mdl-cell mdl-cell--6-col ${styles.toggleBlock}`}>
                <label className={`${styles.switch}`} htmlFor='switch4'>
                  <input type='checkbox' id='switch4' checked={timerOn} onChange={() => this.toggleTimer()} />
                  <span className={`${styles.slider} ${styles.sliderRound}`}>{}</span>
                </label>
                <span>{`${formatMessage(globalMessages.timer)}`}</span>
              </div>
            </div>
          </div>
        </div>
        <footer className={styles.footer}>
          <div className={`mdl-cell mdl-cell--12-col ${styles.footer}`}>
            <div className={`mdl-cell mdl-cell--3-col ${styles.footerItem}`}>
              {
                setDisplayMode && displayMode === 'sentence' && <button
                  className={`mdl-button mdl-js-button mdl-button--raised ${styles.button} ${displayMode !== 'word' ? 'mdl-button--accent' : ''}`}
                  disabled={displayMode === 'word'}
                  onClick={() => { setDisplayMode('word'); }}
                >
                  {formatMessage(globalMessages.practiceWords)}
                </button>
              }
            </div>
            <div className={`mdl-cell mdl-cell--6-col ${styles.footerItem}`}>
              {/* {
                setDisplayMode && showPreviousButton && <button
                  className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}
                  onClick={() => { this.sendResultsAndNext(false); }}
                >
                  {formatMessage(globalMessages.previous)}
                </button>
              } */}
              {
                this.renderButton(showNextTest)
              }
            </div>
            <div className={`mdl-cell mdl-cell--3-col ${styles.footerItem}`}>
              {
                setDisplayMode && displayMode !== 'sentence' && <button
                  className={`mdl-button mdl-js-button mdl-button--raised ${styles.button} ${displayMode !== 'sentence' ? 'mdl-button--accent' : ''}`}
                  disabled={displayMode === 'sentence'}
                  onClick={() => { setDisplayMode('sentence'); }}
                >
                  {formatMessage(globalMessages.practiceSentences)}
                </button>
              }
            </div>
          </div>
        </footer>
        {instructionsModalVisible &&
          <StutteringInstructionsModal
            isOpen={instructionsModalVisible}
            locale={'et'}
            template={'4'}
            previewSettings={previewSettings}
            onConfirm={this.toggleInstructionsModal}
          />}
      </div>
    );
  }
}

export default injectIntl(ExerciseFour);
