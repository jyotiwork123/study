import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import ContentSection from '../ContentSection';
import NoContentSection from '../NoContentSection';

//i18n declaration
import { injectIntl } from 'react-intl';
import { exercisesGridViewMessages } from '../../../common/dictionary/specific';

//styles
import styles from './exerciseContent.css';

class ExercisesContent extends Component {
  static propTypes = {
    exercises: PropTypes.array.isRequired,
    sections: PropTypes.array.isRequired,
    contentType: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.renderSections = this.renderSections.bind(this);
    this.renderIncreaseSearchNotice = this.renderIncreaseSearchNotice.bind(this);
  }

  renderSections(section, index, sections) {
    const { exercises, push, contentType } = this.props;
    let filteredExercises = exercises.filter((exercise) => (
      exercise.settings && exercise.settings.type && exercise.settings.type === section.id
    ));

    const isStuttering = section.title.toLowerCase() === 'sujuva kõne harjutused' || section.title.toLowerCase() === 'stuttering exercises';

    // added to reorder stuttering exercises by exercise name
    if (isStuttering) {
      filteredExercises = filteredExercises.sort((a, b) => (a.settings.exerciseName > b.settings.exerciseName) ? 1 : -1);
    }


    const visible = sections.every((section) => !section.visible);

    const visibility = {
      exercises: !!filteredExercises.length,
      section: visible || section.visible
    };

    this.visibility = [...this.visibility, visibility];
    return (
      <ContentSection
        key={index}
        genderTabs={isStuttering}
        title={section.title}
        supportingText={section.supportingText}
        exercises={filteredExercises}
        // exercises={filteredExercises.sort((a, b) => (a.template > b.template) ? 1 : -1)}
        visible={visible || section.visible}
        push={push}
      />
    );
  }

  renderIncreaseSearchNotice() {
    const { formatMessage } = this.props.intl;
    if (this.visibility.some(el => el.exercises && el.section)) {
      return <span />;
    }
    return (
      <div className={`mdl-shadow--4dp ${styles.increaseSearchCard}`}>
        {formatMessage(exercisesGridViewMessages.increaseSearch)}
      </div>
    );
  }

  render() {
    const { sections, exercises, contentType, userRole } = this.props;
    this.visibility = [];

    if (!exercises.length) {
      return <NoContentSection contentType={contentType} userRole={userRole} />;
    }

    if (exercises[0] === 'error') {
      return (
        <div className={`mdl-cell mdl-cell--12-col ${styles.noContentError}`}>
          Something is wrong. Please contact us cognuse@cognuse.com
        </div>
      );
    }

    return (
      <div>
        {sections.map(this.renderSections)}
        {this.renderIncreaseSearchNotice()}

      </div>
    );
  }
}


export default injectIntl(ExercisesContent);
