import React, { Component } from 'react';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './paymentReminder.css';

class PaymentReminder extends Component {

    render() {
        const { intl: { formatMessage } } = this.props;
        const billingUrl = "https://konto.koneravi.ee/billing.html?" + this.props.patientId;
        
        return (
            <div className={`mdl-grid mdl-shadow ${styles.contentSection} ${styles.paymentReminderSection}`}>
                <div className={`mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet ${styles.paymentReminderSection}`}>
                    <div className={`${styles.paymentReminder}`}>
                        {formatMessage(globalMessages.paymentReminder)}
                    </div>
                    <div className={`${styles.paymentReminderLink}`}>
                        <a href={billingUrl}>
                            <button className={'mdl-button mdl-js-button mdl-button--raised mdl-button--colored'}>
                                {formatMessage(globalMessages.paymentLink)}
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default injectIntl(PaymentReminder);
