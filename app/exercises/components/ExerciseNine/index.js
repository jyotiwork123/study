import React, { Component } from 'react';
import DraggableAnswer from '../draggableAnswer';
import DropArea from '../dropArea';
import ExerciseHeader from '../ExerciseHeader/index';
import CustomDragLayer from '../customDragLayer';
import { setTextCase, formatArrOfStr } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseNine.css';


class ExerciseNine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAnswers: false,
      checked: false,
      valid: null,
      letters: 'As is',
    };

    this.correctSound = new Audio('../../sounds/correct.wav');
    this.incorrectSound = new Audio('../../sounds/incorrect.wav');

    this.showAnswers = this.showAnswers.bind(this);
    this.validateAnswers = this.validateAnswers.bind(this);
    this.checkAnswers = this.checkAnswers.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    const { intl: { formatMessage }, settings } = this.props;
    const letters = settings.reduce((a, c) => c.label === formatMessage(globalMessages.letters) ? a + c.value : a, '').replace('.', '');
    let wordsStyle = '';
    switch (letters) {
      case formatMessage(globalMessages.capitalLetters):
        wordsStyle = 'capitalize';
        break;
      case formatMessage(globalMessages.upperLetters):
        wordsStyle = 'uppercase';
        break;
      case formatMessage(globalMessages.asIs):
        wordsStyle = 'initial';
        break;
      default:
    }
    this.setState({ letters: wordsStyle });
  }

  componentWillReceiveProps(nextProps) {
    const { intl: { formatMessage } } = this.props;
    const { settings, exerciseState, currentTest } = nextProps;
    const letters = settings.reduce((a, c) => c.label === formatMessage(globalMessages.letters) ? a + c.value : a, '');
    let wordsStyle = '';
    switch (letters) {
      case formatMessage(globalMessages.capitalLetters):
        wordsStyle = 'capitalize';
        break;
      case formatMessage(globalMessages.upperLetters):
        wordsStyle = 'uppercase';
        break;
      case formatMessage(globalMessages.asIs):
        wordsStyle = 'initial';
        break;
      default:
    }

    const isCompletedTest = exerciseState.reduce((a, c) => c.number === currentTest || a, false);
    this.setState({
      showAnswers: false,
      checked: isCompletedTest,
      valid: null,
      letters: wordsStyle
    });
  }

  checkAnswers() {
    const { currentResults } = this.props;
    if (currentResults.every((el) => el !== null)) {
      this.validateAnswers();
      this.setState({ checked: true });
    }
  }

  showAnswers() {
    this.setState({ showAnswers: true });
  }

  validateAnswers() {
    const { currentResults, currentReferences, answerPairs } = this.props;
    const valid = currentResults.every((el, index) =>
      answerPairs[index].word.toLowerCase() === currentReferences[index].toLowerCase()
      && answerPairs[index].match.toLowerCase() === el.toLowerCase());
    if (valid) {
      this.correctSound.play();
    } else {
      this.incorrectSound.play();
    }
    this.setState({ valid, });
  }

  renderReferences(currentReferences) {
    const { letters } = this.state;
    return currentReferences.map((reference, index) =>
      <div key={index} className='reference-wrapper'>
        <h4>
          {letters === 'capitalize' ? reference[0].toUpperCase() + reference.toLowerCase().slice(1) : ''}
          {letters === 'uppercase' ? reference.toUpperCase() : ''}
          {letters === 'initial' ? reference : ''}
        </h4>
      </div>
    );
  }

  renderAnswers(currentAnswers, moveAnswer) {
    const { exerciseState, currentTest, settings } = this.props;
    const answersForThisTestInState = exerciseState.reduce((a, c) => c.number === currentTest ? c.answers : a, []);
    return !answersForThisTestInState.length && currentAnswers.map((answer, index) => {
      const answersCase = setTextCase(settings[3].value);
      return (<DraggableAnswer
        answersCase={answersCase}
        moveAnswer={moveAnswer}
        answer={answer}
        index={index}
        key={index}
        restoreState={this.props.restoreState}
        backupState={this.props.backupState}
      />);
    });
  }

  renderResults(answersAmount, currentResults, moveAnswer, currentReferences, autoValidation) {
    const { answerPairs, settings, intl: { formatMessage } } = this.props;
    let dropZone = [];
    for (let i = 0; i < answersAmount; i++) {
      if (currentResults[i]) {
        const isCorrectSlot = answerPairs
          .some((el) => el.match.toLowerCase() === currentResults[i].toLowerCase() && el.word.toLowerCase() === currentReferences[i].toLowerCase());

        let borderColor = {};
        if (autoValidation.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase() || this.state.checked) {
          borderColor = isCorrectSlot ? { borderColor: '#5cb85c' } : { borderColor: 'red' };
        }
        const answersCase = setTextCase(settings[3].value);
        dropZone = dropZone.concat(
          <DropArea
            index={i}
            occupiedWith={currentResults[i]}
            key={i}
            rearrange={this.props.rearrange}
            restoreState={this.props.restoreState}
            whenHaveNested={{ minWidth: '10px' }}
            borderColor={borderColor}
          >
            <DraggableAnswer
              answersCase={answersCase}
              moveAnswer={moveAnswer}
              answer={currentResults[i]}
              index={i}
              key={i}
              restoreState={this.props.restoreState}
              backupState={this.props.backupState}
              whenNested={{ margin: '5px 6px' }}
              result
            />
          </DropArea>
        );
      } else {
        dropZone = dropZone.concat(
          <DropArea
            rearrange={this.props.rearrange}
            index={i}
            key={i}
            restoreState={this.props.restoreState}
          />
        );
      }
    }
    return dropZone;
  }

  renderButton() {
    const { showNextTest } = this.props;
    const { formatMessage } = this.props.intl;
    const { checked, valid } = this.state;
    if (checked) {
      return (
        <button
          className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button} ${styles.buttonNextAndCheck}`}
          onClick={() => { showNextTest(valid); }}
        >
          {formatMessage(globalMessages.next)}
        </button>
      );
    }
    return (
      <button
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button} ${styles.buttonNextAndCheck}`}
        onClick={this.checkAnswers}
      >
        {formatMessage(globalMessages.check)}
      </button>
    );
  }

  render() {
    const {
      exerciseName,
      questionTitle,
      moveAnswer,
      currentAnswers,
      currentResults,
      answersAmount,
      currentReferences,
      settings,
      updateCurrentSettings,
      push,
      showNextTest,
      countTests,
      currentTest,
      selectNextTest,
      timelineState,
      template,
      exerciseState,
      intl: { formatMessage }
    } = this.props;

    const currentAnswersInState = exerciseState.reduce((a, c) => c.number === currentTest ? c.answers : a, []);

    const answersCase = setTextCase(settings[3].value);
    const formatedCurrentReferences = formatArrOfStr(currentReferences, answersCase);
    const formatedCurrentAnswers = formatArrOfStr(currentAnswers, answersCase);
    const formatedCurrentResults = currentAnswersInState.length && currentAnswersInState || formatArrOfStr(currentResults, answersCase);
    const hasOverlay = exerciseState && exerciseState.reduce((a, c) => (currentTest === c.number) || a, false) || false;

    return (
      <div className={`mdl-layout mdl-js-layout mdl-layout--fixed-header ${styles.autoHeight}`}>
        <ExerciseHeader
          exerciseName={exerciseName}
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
          countTests={countTests}
          currentTest={currentTest}
          selectNextTest={selectNextTest}
          timelineState={timelineState}
          template={template}
        />
        <div className={styles.wrap}>
          <div className="exercise-content exercise-nine" style={{ textTransform: answersCase }}>
            <div className="answers-wrapper">
              {this.renderReferences(formatedCurrentReferences)}
            </div>
            <div className="answers-wrapper">
              {this.renderResults(answersAmount, formatedCurrentResults, moveAnswer, formatedCurrentReferences, settings[4].value)}
            </div>
            <div className="answers-wrapper">
              {this.renderAnswers(formatedCurrentAnswers, moveAnswer)}
            </div>
            <CustomDragLayer answersCase={answersCase} exerciseNumber="nine" />
          </div>
          <div className="exercise-footer">
            <div className={`${styles.answerWrapper} ${styles.wrapNextAndCheck}`}>
              {settings[4].value.toLowerCase() === formatMessage(globalMessages.no).toLowerCase() && this.renderButton(showNextTest)}
            </div>
          </div>
          {hasOverlay ? <div className={styles.overlayIfTestDone} /> : null}
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseNine);
