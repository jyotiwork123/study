import React, { Component } from 'react';
import PropTypes from 'prop-types';

//router
import { Link } from 'react-router-dom';

//utils, data
import { Scrollbars } from 'react-custom-scrollbars';

//i18n declaration
import { injectIntl } from 'react-intl';
import { exercisesPreview } from '../../../common/dictionary/specific';

//components
import CloseButton from '../../../common/components/closeButton';
import StartingInstructionsFour from './StartingInstructionsFour';
import StartingInstructionsFourA from './StartingInstructionsFourA';
import StartingInstructionsFourB from './StartingInstructionsFourB';

//styles
import styles from './exercisePreviewContent.css';


class ExercisePreviewContent extends Component {
  static propTypes = {
    onRequestClose: PropTypes.func.isRequired,
    link: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.renderButtons = this.renderButtons.bind(this);
  }

  componentDidMount() {
    componentHandler.upgradeDom();
  }

  renderButtons() {
    const {
      topics,
      link,
      selectedTopic,
      pushToExercise,
      addToFavouritesAction,
      removeFromFavouritesAction,
      addToSuggestionsAction,
      removeFromSuggestionsAction,
      toggleContent,
      toggleTopics,
      favourites,
      suggestedExercises,
      currentExercise,
      intl: { formatMessage },
      userRole
    } = this.props;

    console.log(userRole);

    const buttonClasses = 'mdl-button mdl-button--raised mdl-button--colored mdl-js-button mdl-js-ripple-effect ';
    const alreadyFav = favourites.reduce((acc, ex) => currentExercise._id === ex._id || acc, false);
    const alreadySuggested = suggestedExercises && suggestedExercises.includes(currentExercise._id);
    if (topics.length) {
      return (
        <div className={styles.buttonsWrapper}>
          <div
            className={`${buttonClasses} ${styles.selectTopic}`}
            onClick={toggleTopics}
            ref={el => { this.selectTopicButton = el; }}
          >
            {selectedTopic || formatMessage(exercisesPreview.selectTopic)}
          </div>
          <div className={styles.bottomButtonsWrapper}>
            <button
              className={`${buttonClasses} ${styles.beginWhenTopics}`}
              onClick={pushToExercise}
            >
              {formatMessage(exercisesPreview.begin)}
            </button>
            <button
              className={`${buttonClasses} ${styles.beginWhenTopics}`}
              style={{
                margin: '8px 0 0 2%'
              }}
              onClick={toggleContent}
            >
              {formatMessage(exercisesPreview.checkResults)}
            </button>
          </div>
          {
            userRole < 10 && <div>
              {!alreadyFav
                ? <div
                  className={`${buttonClasses} ${styles.favouritesButton} ${styles.addToFavouritesWhenTopics}`}
                  onClick={addToFavouritesAction}
                >
                  {formatMessage(exercisesPreview.favourites)}
                </div>
                : <div
                  className={`${buttonClasses} ${styles.favouritesButton} ${styles.removeFromFavouritesWhenTopics}`}
                  onClick={removeFromFavouritesAction}
                >
                  {formatMessage(exercisesPreview.removeFavourites)}
                </div>
              }
            </div>
          }
          {
            userRole >= 10 && <div>
              {!alreadySuggested
                ? <div
                  className={`${buttonClasses} ${styles.favouritesButton} ${styles.addToFavouritesWhenTopics}`}
                  onClick={addToSuggestionsAction}
                >
                  {formatMessage(exercisesPreview.suggestions)}
                </div>
                : <div
                  className={`${buttonClasses} ${styles.favouritesButton} ${styles.removeFromFavouritesWhenTopics}`}
                  onClick={removeFromSuggestionsAction}
                >
                  {formatMessage(exercisesPreview.removeSuggestions)}
                </div>
              }
            </div>
          }
        </div>
      );
    }
    return (
      <div>
        <Link
          className={`${buttonClasses} ${styles.begin}`}
          to={link}
        >
          {formatMessage(exercisesPreview.begin)}
        </Link>
        <div
          className={`${buttonClasses} ${styles.begin}`}
          style={{
            marginTop: '8px'
          }}
          onClick={toggleContent}
        >
          {formatMessage(exercisesPreview.checkResults)}
        </div>
        {
          userRole < 10 && <div>
            {!alreadyFav
              ? <div
                className={`${buttonClasses} ${styles.favouritesButton} ${styles.addToFavourites}`}
                onClick={addToFavouritesAction}
              >
                {formatMessage(exercisesPreview.favourites)}
              </div>
              : <div
                className={`${buttonClasses} ${styles.favouritesButton} ${styles.removeFromFavourites}`}
                onClick={removeFromFavouritesAction}
              >
                {formatMessage(exercisesPreview.removeFavourites)}
              </div>
            }
          </div>
        }
        {
          userRole >= 10 && <div>
            {!alreadySuggested
              ? <div
                className={`${buttonClasses} ${styles.favouritesButton} ${styles.addToFavourites}`}
                onClick={addToSuggestionsAction}
              >
                {formatMessage(exercisesPreview.suggestions)}
              </div>
              : <div
                className={`${buttonClasses} ${styles.favouritesButton} ${styles.removeFromFavourites}`}
                onClick={removeFromSuggestionsAction}
              >
                {formatMessage(exercisesPreview.removeSuggestions)}
              </div>
            }
          </div>
        }
      </div>
    );
  }

  render() {
    const { imageUrl, settings, onRequestClose, topics, showTopicsPopup, setTopic, intl: { formatMessage }, currentExercise } = this.props;
    console.log('this.props', this.props);

    return (
      <div className={`mdl-grid ${styles.preview}`}>
        <CloseButton
          mainClassName={styles.back}
          iconClassName={styles.backIcon}
          text={formatMessage(exercisesPreview.back)}
          handleClose={onRequestClose}
        />
        <div
          className={`mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone ${styles.leftSection}`}
        >
          <div className={styles.scrollbarsWrapper}>
            {
              (currentExercise.template !== '4' && currentExercise.template !== '4a' && currentExercise.template !== '4b') && <Scrollbars>
                <h3 className={styles.title}>{settings.exerciseName}</h3>
                <div className={styles.instructions}>
                  <h5>{formatMessage(exercisesPreview.introduction)}</h5>
                  {settings.exerciseDescription}
                </div>
                <div className={styles.benefits}>
                  <h5>{formatMessage(exercisesPreview.benefits)}</h5>
                  {settings.benefits}
                </div>
                <div className={styles.usefullness}>
                  <h5>{formatMessage(exercisesPreview.usefullness)}</h5>
                  {settings.usefullness}
                </div>
              </Scrollbars>
            }
            {
              (currentExercise.template === '4') && <Scrollbars>
                <h3 className={styles.title}>{settings.exerciseName}</h3>
                <div className={styles.stutteringInstructions}>
                  <StartingInstructionsFour settings={settings} preview locale={'et'} />
                </div>
              </Scrollbars>
            }
            {
              (currentExercise.template === '4a') && <Scrollbars>
                <h3 className={styles.title}>{settings.exerciseName}</h3>
                <div className={styles.stutteringInstructions}>
                  <StartingInstructionsFourA settings={settings} preview locale={'et'} />
                </div>
              </Scrollbars>
            }
            {
              (currentExercise.template === '4b') && <Scrollbars>
                <h3 className={styles.title}>{settings.exerciseName}</h3>
                <div className={styles.stutteringInstructions}>
                  <StartingInstructionsFourB settings={settings} preview locale={'et'} />
                </div>
              </Scrollbars>
            }
          </div>
        </div>
        <div className={`mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone ${styles.rightSection}`}>
          <div className={styles.scrollbarsWrapper}>
            <div
              className={styles.image}
              style={{ background: `url('${imageUrl}') no-repeat top center / contain` }}
            />
            {this.renderButtons()}
          </div>
        </div>
        {showTopicsPopup &&
          <div className={`mdl-shadow--4dp ${styles.topicsPopup}`}>
            <Scrollbars>
              {
                topics.map((el, index) => (
                  <div
                    key={index}
                    className={styles.listItem}
                    onClick={setTopic}
                  >
                    {el}
                  </div>
                ))
              }
            </Scrollbars>
            <div
              className={styles.triangle}
              style={{ top: `${this.selectTopicButton.offsetTop + (this.selectTopicButton.offsetHeight / 2) - 70}px` }}
            />
          </div>
        }
      </div>
    );
  }
}

export default injectIntl(ExercisePreviewContent);
