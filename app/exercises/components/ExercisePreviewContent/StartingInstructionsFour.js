import React, { Component } from 'react';

//styles
import styles from './exercisePreviewContent.css';

export default class StartingInstructionsFourA extends Component {

    render() {
        const { locale, settings, preview, type } = this.props;

        return (
            <div>
                {
                    locale === 'et' && <div className={styles.stutteringInstructionsContainer}>
                        {/* <h3>{'Harjutus 3 - pehmed algused'}</h3> */}

                        {
                            settings.exerciseName.includes('4') &&
                             <p className={preview ? styles.stutteringInstructions : styles.popupInstructions}>See harjutus koondab eelmised kolm harjutust.</p>

                        }

                        <p className={preview ? styles.stutteringInstructions : styles.popupInstructions}>
                            Istu mugavalt ja sirgelt. Jälgi oma rühti. Rahune. Lõdvesta lõug ja häälepaelad. Hinga rahulikult ja sügavalt.
                            </p>
                        {/* <p className={preview && styles.stutteringInstructions}>Kuula kõigepealt sisseloetud näidislauset (väga aeglane, aeglane või tavaline kiirus). Korda seejärel seda võimalikult täpselt, hoides samasugust tempot.</p> */}
                        <p className={preview ? styles.stutteringInstructionsVariable : styles.popupInstructionsVariable}>{settings && settings.shortCommand && settings.shortCommand}</p>
                        <p className={preview ? styles.stutteringInstructions : styles.popupInstructions}>Kui lauset kordama hakkad, siis vajuta HARJUTA. Kui lause on lõppenud, siis vajuta KONTROLLI. Arvuti ütleb Sulle seejärel, kas mahtusid ajalimiiti või pead tempot lisama või maha võtma.</p>
                        <p className={preview ? styles.stutteringInstructions : styles.popupInstructions}>Sulle on lause kordamise ajal abiks järgmised vahendid:</p>
                        <ul>
                            <li className={preview ? styles.stutteringInstructions : styles.popupInstructions}>Korratav lause tekstina</li>
                            <li className={preview ? styles.stutteringInstructions : styles.popupInstructions}>Visuaalne progressiriba</li>
                            <li className={preview ? styles.stutteringInstructions : styles.popupInstructions}>Metronoom takti hoidmiseks</li>
                            <li className={preview ? styles.stutteringInstructions : styles.popupInstructions}>Stopper kulunud aja mõõtmiseks. </li>
                        </ul>
                        <p className={preview ? styles.stutteringInstructions : styles.popupInstructions}>Mida rohkem abivahendeid välja lülitad, seda keerulisemaks ülesanne Sulle muutub.</p>
                        <p className={preview ? styles.stutteringInstructions : styles.popupInstructions}>Head harjutamist!</p>
                    </div>
                }
                {
                    locale === 'en' && <div className={styles.stutteringInstructions}>
                        <h4>{''}</h4>
                    </div>
                }
            </div>
        );
    }
}
