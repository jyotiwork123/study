import React, { Component } from 'react';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
import { noContentSection, noSuggestionSection } from '../../../common/dictionary/specific';

//styles
import styles from './noContentSection.css';


class NoContentSection extends Component {
  static propTypes = {
    contentType: PropTypes.string,
    // supportingText: PropTypes.string.isRequired,
    // exercises: PropTypes.array.isRequired,
    // visible: PropTypes.bool.isRequired
  };

  render() {
    const { formatMessage } = this.props.intl;
    const { contentType, userRole } = this.props;

    // console.log('contentType', contentType)

    const noContent = (contentType && contentType === 'suggestions') ? noSuggestionSection : noContentSection;

    // console.log('noContent', noContent)


    return (
      <div className={`mdl-cell mdl-cell--12-col mdl-shadow--4dp ${styles.contentSection}`}>
        {
          userRole < 10 ?
            <h3 className={styles.title}>{formatMessage(noContent.titlePatient)}</h3>
            :
            <div>
              <h3 className={styles.title}>{formatMessage(noContent.title)}</h3>
              <p className={`${styles.supportingText}`}>
                {formatMessage(noContent.subTitle)}
              </p>
              <p>
                {formatMessage(noContent.step1)}
              </p>
              <p>
                {formatMessage(noContent.step2)}
              </p>
              <p>
                {formatMessage(noContent.step3)}
              </p>
              <p>
                {formatMessage(noContent.step4)}
              </p>
            </div>
        }
      </div>
    );
  }
}

export default injectIntl(NoContentSection);
