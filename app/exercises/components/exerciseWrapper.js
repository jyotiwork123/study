import React, { Component } from 'react';
//router
import { Route } from 'react-router-dom';
//components
import WarningPopup from './WarningPopup/index';
import {
  ExerciseOneView,
  ExerciseThreeView,
  ExerciseThreeAView,
  ExerciseFourView,
  ExerciseFourAView,
  ExerciseFourBView,
  ExerciseFiveView,
  ExerciseFiveAView,
  ExerciseFiveBView,
  ExerciseSixView,
  ExerciseSevenView,
  ExerciseSevenAView,
  ExerciseEightView,
  ExerciseNineView
} from '../views/index';

//i18n declaration
import { injectIntl } from 'react-intl';
import { warnings } from '../../common/dictionary/specific';

class ExerciseWrapper extends Component {
  render() {
    const { intl: { formatMessage } } = this.props;

    if (window.innerWidth < 640) {
      return <WarningPopup text={formatMessage(warnings.smallScreen)} />;
    }

    return (
      <div className='settings--wrapper'>
        <Route path="/exercises/1/:id" component={ExerciseOneView} />
        <Route path="/exercises/3/:id" component={ExerciseThreeView} />
        <Route path="/exercises/3a/:id" component={ExerciseThreeAView} />
        <Route path="/exercises/4/:id" component={ExerciseFourView} />
        <Route path="/exercises/4a/:id" component={ExerciseFourAView} />
        <Route path="/exercises/4b/:id" component={ExerciseFourBView} />
        <Route path="/exercises/5/:id" component={ExerciseFiveView} />
        <Route path="/exercises/5a/:id" component={ExerciseFiveAView} />
        <Route path="/exercises/5b/:id" component={ExerciseFiveBView} />
        <Route path="/exercises/6/:id" component={ExerciseSixView} />
        <Route path="/exercises/7/:id" component={ExerciseSevenView} />
        <Route path="/exercises/7a/:id" component={ExerciseSevenAView} />
        <Route path="/exercises/8/:id" component={ExerciseEightView} />
        <Route path="/exercises/9/:id" component={ExerciseNineView} />
      </div>
    );
  }
}

export default injectIntl(ExerciseWrapper);
