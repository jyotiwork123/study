import React, { Component } from 'react';
import PropTypes from 'prop-types';

//utils, data
import shortid from 'shortid';
import { setExerciseDelay, setTextCase } from '../../helpers';

//components
import { ExerciseQuestion } from '../exerciseQuestion';
import ExerciseHeader from '../ExerciseHeader/';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseSeven.css';

const SYMBOLS_FILTER = /[.,!?“;:‘'"-]/g;

class ExerciseSeven extends Component {
  static propTypes = {
    showNextTest: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    testQuestions: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
    testAnswers: PropTypes.array.isRequired,
    questionImage: PropTypes.string.isRequired,
    questionTitle: PropTypes.string
  };
  static defaultProps = {
    questionTitle: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      showAnswers: false,
      inputResultValue: '',
      checked: false,
      answer: false,
      shouldNextTest: false,
    };

    this.correctSound = new Audio('../../sounds/correct.wav');
    this.incorrectSound = new Audio('../../sounds/incorrect.wav');

    this.renderAnswers = this.renderAnswers.bind(this);
    this.renderButton = this.renderButton.bind(this);
    this.showAnswers = this.showAnswers.bind(this);
    this.validateInput = this.validateInput.bind(this);
    this.showNext = this.showNext.bind(this);
    this.handleResultValue = this.handleResultValue.bind(this);
    this.validateResult = this.validateResult.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  componentWillReceiveProps(props) {
    const { exerciseState, currentTest } = props;
    const inputValue = exerciseState.reduce((acc, ex) =>
      ex.number === currentTest
        ? ex.answer
        : acc
      , '');
    if (props.questionImage !== this.props.questionImage) {
      this.setState({
        showAnswers: !!inputValue,
        inputResultValue: inputValue,
        checked: !!inputValue,
        shouldNextTest: false
      });
    }
  }

  showAnswers() {
    this.setState({ showAnswers: true });
  }

  handleResultValue(e) {
    this.setState({ inputResultValue: e.target.value });
  }

  showNext(answer) {
    const { inputResultValue } = this.state;
    const { addResultsToExerciseState, showNextTest, settings } = this.props;
    this.setState({ shouldNextTest: true }, () => {
      const nextTestDelay = setExerciseDelay(settings[3].value);
      setTimeout(() => {
        addResultsToExerciseState(inputResultValue);
        showNextTest(answer);
      }, nextTestDelay);
    });
  }

  validateInput(inputResultValue) {
    const correctAnswer = this.props.correctAnswer.toLowerCase();
    const filteredInputResultsValue = inputResultValue.replace(SYMBOLS_FILTER, '').toLowerCase();
    let symbolCounter = 0;
    if (filteredInputResultsValue === correctAnswer && !this.state.shouldNextTest) {
      this.correctSound.play();
      this.answer = true;
      this.showNext(this.answer);
    }

    return inputResultValue.split('').map((el, index) => {
      if (el.match(SYMBOLS_FILTER)) {
        symbolCounter++;
        return <span key={shortid.generate()}>{el}</span>;
      }
      const updatedIndex = index - symbolCounter;
      if (correctAnswer[updatedIndex] && el.toLowerCase() === correctAnswer[updatedIndex]) {
        return <span key={shortid.generate()}>{el}</span>;
      }

      return <span key={shortid.generate()} className={styles.charRed}>{el}</span>;
    });
  }

  validateResult(inputResultValue) {
    const { correctAnswer, intl: { formatMessage } } = this.props;
    const filteredInputResultsValue = inputResultValue.replace(SYMBOLS_FILTER, '');

    if (filteredInputResultsValue.toLowerCase() === correctAnswer.toLowerCase()) {
      this.correctSound.play();
      this.answer = true;
      return `${formatMessage(globalMessages.yourCorrectAnswer)}`;
    }
    this.incorrectSound.play();
    this.answer = false;
    return `${formatMessage(globalMessages.correctAnswerIs)}: ${correctAnswer}`;
  }

  renderButton() {
    const { settings } = this.props;
    const { checked } = this.state;
    const { formatMessage } = this.props.intl;
    if (checked) {
      return (
        <button
          className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.checkButton}`}
          style={{ textTransform: setTextCase(settings[4].value) }}
          onClick={() => { this.showNext(this.answer); }}
        >
          {formatMessage(globalMessages.next)}
        </button>
      );
    }
    return (
      <button
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.nextButton}`}
        style={{ textTransform: setTextCase(settings[4].value) }}
        onClick={() => this.setState({ checked: true })}
      >
        {formatMessage(globalMessages.check)}
      </button>
    );
  }

  renderAnswers(testQuestions) {
    const { settings } = this.props;
    return testQuestions.map((answer) =>
      <div
        className={`mdl-button mdl-button--raised mdl-button--colored ${styles.question}`}
        key={shortid.generate()}
        style={{ textTransform: setTextCase(settings[4].value) }}
      >
        {answer}
      </div>
    );
  }


  render() {
    const {
      exerciseName,
      answersAmount,
      questionTitle,
      questionImage,
      testQuestions,
      settings,
      updateCurrentSettings,
      push,
      template,
      timelineState,
      showNextTest,
      currentTest,
      exerciseState,
      intl: { formatMessage }
    } = this.props;
    const { inputResultValue } = this.state;
    const { completed } = timelineState;
    const hasOverlay = exerciseState && exerciseState.reduce((a, c) => (currentTest === c.number) || a, false) || false;

    return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <ExerciseHeader
          exerciseName={exerciseName}
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
          answersAmount={answersAmount}
          template={template}
          selectNextTest={showNextTest}
          currentTest={currentTest}
          timelineState={timelineState}
        />
        <div
          className={styles.content}
          onLoad={this.showAnswers}
          style={{ display: `${this.state.showAnswers ? 'block' : 'none'}` }}
        >
          <ExerciseQuestion
            questionImage={questionImage}
          />
          <div className={styles.answers}>
            <div className={styles.answerWrapper}>
              {this.renderAnswers(testQuestions, settings)}
            </div>
            <div className={styles.answerWrapper}>
              <div className={`mdl-textfield mdl-js-textfield ${styles.resultInputWrapper}`}>
                <div className={styles.fakeInput}>
                  {
                    settings[2].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase() && !completed.includes(currentTest + 1)
                      ? this.validateInput(inputResultValue)
                      : inputResultValue
                  }
                </div>
                <input
                  className={`mdl-textfield__input ${document.documentMode ? styles.mainInputExplorer : styles.mainInputWebkit} ${styles.input}`}
                  type="text"
                  id="result"
                  autoComplete="off"
                  disabled={this.state.checked}
                  value={inputResultValue}
                  onChange={this.handleResultValue}
                />
                <label className={`mdl-textfield__label ${styles.label}`} htmlFor="result">{formatMessage(globalMessages.enterCorrectSentence)}</label>
              </div>
            </div>
            <div className={styles.correctAnswer}>
              {this.state.checked && this.validateResult(inputResultValue)}
            </div>
            <div className={styles.answerWrapper}>
              {settings[2].value.toLowerCase() === formatMessage(globalMessages.no).toLowerCase() && this.renderButton()}
            </div>
          </div>
          {hasOverlay ? <div className={styles.overlayIfTestDone} /> : null}
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseSeven);
