import React, { Component } from 'react';
import shortid from 'shortid';
import PropTypes from 'prop-types';

//components
import ExerciseCard from '../ExerciseCard';

//redux
import { connect } from 'react-redux';
import {
  setPrefferedVoiceGender
} from '../../state/actions';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './contentSection.css';


class ContentSection extends Component {
  static propTypes = {
    prefferedVoiceGender: PropTypes.string,
    title: PropTypes.string.isRequired,
    supportingText: PropTypes.string.isRequired,
    exercises: PropTypes.array.isRequired,
    visible: PropTypes.bool.isRequired,
    genderTabs: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      prefferedVoiceGender: 'male'
    };
    this.renderCards = this.renderCards.bind(this);
    this.changeVoiceGender = this.changeVoiceGender.bind(this);
  }

  componentDidMount() {
    
    let prefferedVoiceGender = 'male';
    if (this.props.genderTabs && localStorage.getItem('prefferedVoiceGender')) {
      prefferedVoiceGender = localStorage.getItem('prefferedVoiceGender');
      this.setState({ prefferedVoiceGender });
    }
  }

  changeVoiceGender(gender) {
    this.setState({ prefferedVoiceGender: gender });
    localStorage.setItem('prefferedVoiceGender', gender);
  }

  renderCards(item) {
    const { exerciseName, shortDescription } = item.settings;
    const { push } = this.props;
    const location = window.location.search.split('/');
  
    return (
      <div key={shortid.generate()} className='mdl-cell mdl-cell--2-col mdl-cell--3-col-tablet mdl-cell--3-col-phone'>
        <ExerciseCard
          data={item}
          title={exerciseName || ''}
          description={shortDescription || ''}
          link={`/exercises/${item.template}/${item._id}`}
          template={item.template}
          push={push}
          previewInitially={location[1] === item.template && location[2] === item._id}
        />
      </div>
    );
  }

  render() {
    const { title, supportingText, exercises, visible, genderTabs } = this.props;
    const { formatMessage } = this.props.intl;
    const { prefferedVoiceGender } = this.state;

    const style = {
      display: `${visible ? 'flex' : 'none'}`
    };

    if (!exercises.length) {
      return null;
    }

    const genderOption = formatMessage(globalMessages[prefferedVoiceGender]).toLowerCase();
    const activeExercises = exercises.filter(ex => ex.settings.exerciseName.includes(genderOption));
    // console.log(genderOption)
    // console.log(activeExercises)
    return (
      <div className={`mdl-grid mdl-shadow--4dp ${styles.contentSection}`} style={style}>
        <div className={`mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet ${styles.supportingText}`}>
          <h2 className={styles.title}>{title}</h2>
          {supportingText}
        </div>
        {
          genderTabs
            ?
            <div className='mdl-tabs mdl-js-tabs'>
              <div className='mdl-tabs__tab-bar'>
                <div
                  // href='#tab1-panel'
                  className={`mdl-tabs__tab ${prefferedVoiceGender === 'male' ? 'is-active' : ''}`}
                  onClick={() => this.changeVoiceGender('male')}
                >{formatMessage(globalMessages.maleVoice)}
                </div>
                <div
                  // href='#tab2-panel'
                  className={`mdl-tabs__tab ${prefferedVoiceGender === 'female' ? 'is-active' : ''}`}
                  onClick={() => this.changeVoiceGender('female')}
                >{formatMessage(globalMessages.femaleVoice)}
                </div>
              </div>
              {
                <div
                  className={`is-active`}
                // id='tab1-panel'
                >
                  <div className={`mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-grid mdl-grid--no-spacing ${styles.cardsField}`}>
                    {activeExercises.map(this.renderCards)}
                  </div>
                </div>
              }
              {/* <div
                className={`mdl-tabs__panel is-active`}
                id='tab1-panel'
              >
                <div className={`mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-grid mdl-grid--no-spacing ${styles.cardsField}`}>
                  {exercises.filter(ex => ex.settings.exerciseName.includes('mees')).map(this.renderCards)}
                </div>
              </div>
              <div
                className={`mdl-tabs__panel`}
                id='tab2-panel'
              >
                <div className={`mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-grid mdl-grid--no-spacing ${styles.cardsField}`}>
                  {exercises.filter(ex => ex.settings.exerciseName.includes('naine')).map(this.renderCards)}
                </div>
              </div> */}
            </div>
            :
            <div className={`mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-grid mdl-grid--no-spacing ${styles.cardsField}`}>
              {exercises.map(this.renderCards)}
            </div>
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    prefferedVoiceGender: state.Exercises.prefferedVoiceGender
  };
}

export default connect(mapStateToProps, {
  setPrefferedVoiceGender
})(injectIntl(ContentSection));
