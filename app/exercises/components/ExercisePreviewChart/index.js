import React, { Component } from 'react';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
import { exercisesPreview } from '../../../common/dictionary/specific';

//components
import ReactHighstock from 'react-highcharts/bundle/ReactHighstock';
import CloseButton from '../../../common/components/closeButton';

//styles
import styles from './exercisePreviewChart.css';


class ExercisePreviewChart extends Component {
  static propTypes = {
    toggleContent: PropTypes.func.isRequired,
    config: PropTypes.object.isRequired,
  };
  componentWillMount() {
    ReactHighstock.Highcharts.setOptions({
      lang: {
        weekdays: ['esmaspäev', 'teisipäev', 'kolmapäev', 'neljapäev', 'reede', 'laupäev', 'pühapäev'],
        shortMonths: ['jaan', 'veebr', 'märts', 'apr', 'mai', 'juuni', 'juuli', 'aug', 'sept', 'okt', 'nov', 'dets'],
        rangeSelectorFrom: 'alates',
        rangeSelectorTo: 'kuni',
        rangeSelectorZoom: 'suurenda'
      }
    });
  }

  componentDidMount() {
    componentHandler.upgradeDom();
  }

  render() {
    const { toggleContent, config, intl: { formatMessage } } = this.props;

    return (
      <div className={styles.wrapper}>
        <CloseButton
          mainClassName={styles.back}
          iconClassName={styles.backIcon}
          handleClose={toggleContent}
        />
        {config.series ?
          <ReactHighstock
            config={config}
          />
          :
          <span className={styles.noData}>No data available</span>
        }
      </div>
    );
  }
}

export default injectIntl(ExercisePreviewChart);
