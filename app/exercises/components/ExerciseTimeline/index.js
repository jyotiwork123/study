import React, { Component } from 'react';
import styles from './exerciseTimeline.css';

class Timeline extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
      ranges: [],
      full: true, // need scroll or not, true - not, false - need
      activeRange: 0,
    };

    this.onChange = this.onChange.bind(this);
    this.prev = this.prev.bind(this);
    this.next = this.next.bind(this);
    this.calculateSizes = this.calculateSizes.bind(this);
  }

  componentWillMount() {
    this.calculateSizes();
  }

  componentDidMount() {
    window.addEventListener('resize', this.calculateSizes);
  }

  componentWillReceiveProps(nextProps) {
    const { currentTest } = nextProps;
    const { ranges } = this.state;
    const activeRange = ranges.reduce((a, c, i) => c.includes(currentTest + 1) ? i : a, 0);
    this.setState({ activeRange });
  }

  onChange(number) {
    const templatesHasPossibilityToComeBack = ['3', '5', '5a', '5b', '6', '7', '7a', '9'];
    const { ranges } = this.state;
    const { currentTest, selectNextTest, template } = this.props;
    if ((number - 1) !== currentTest && templatesHasPossibilityToComeBack.includes(template)) {
      this.setState({
        activeRange: ranges.reduce((a, c, i) => c.includes(number) ? i : a, 0)
      });
      selectNextTest(true, number - 1);
    }
  }

  calculateSizes() {
    const { timelineState, currentTest } = this.props;
    const width = window.innerWidth - 272;
    const full = width / timelineState.tests.length >= 96;
    const count = Math.floor(width / 96);

    const ranges = timelineState.tests.reduce(
      (a, c, i, arr) =>
        !(i % count)
          ? [
              ...a,
              arr.slice(count * (i / count), count * ((i / count) + 1))]
          : a
      , []);
    /*
    input:
      array e.g. [1, 2, 3, 4, 5]
      count - 2
    output:
      [[1, 2], [3, 4], [5]]
    */
    const activeRange = ranges.reduce((a, c, i) => c.includes(currentTest + 1) ? i : a, 0);
    this.setState({
      width,
      full,
      ranges,
      activeRange
    });
  }

  prev() {
    const { activeRange } = this.state;
    const ar = activeRange ? activeRange - 1 : 0;
    this.setState({ activeRange: ar });
  }

  next() {
    const { ranges, activeRange } = this.state;
    const ar = (activeRange === ranges.length - 1)
      ? ranges.length - 1
      : activeRange + 1;

    this.setState({ activeRange: ar });
  }

  render() {
    const {
      currentTest,
      timelineState,
    } = this.props;

    const { full, ranges, activeRange } = this.state;
    const arrowClass = full
      ? 'mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect'
      : 'mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect';

    const wrapNumbersClass = full
      ? `${styles.wrapNumbersScroll}`
      : `${styles.wrapNumbers}`;

    const displayedNumbers = full ? timelineState.tests : ranges[activeRange];

    return (
      <div className={styles.wrapTimeline}>
        <button
          className={`${arrowClass} ${styles.arrows} ${styles.arrowPrev}`}
          disabled={full}
          onClick={this.prev}
        >
          <i className="material-icons">navigate_before</i>
        </button>
        <div className={wrapNumbersClass}>
          {
            displayedNumbers.map(test =>
              <button
                key={'test-' + test}
                onClick={() => this.onChange(test)}
                className={
                  `mdl-button mdl-js-button mdl-button--fab ${styles.number} ${
                    test === (currentTest + 1)
                      ? styles.active
                      : ''
                    } ${
                      timelineState.correct.includes(test)
                        ? styles.correct
                        : ''
                    } ${
                      timelineState.incorrect.includes(test)
                        ? styles.incorrect
                        : ''
                    }`
                }
              >
                <i>{test}</i>
              </button>
            )
          }
        </div>
        <button
          className={`${arrowClass} ${styles.arrows} ${styles.arrowNext}`}
          disabled={full}
          onClick={this.next}
        >
          <i className="material-icons">navigate_next</i>
        </button>
      </div>
    );
  }
}

export default Timeline;
