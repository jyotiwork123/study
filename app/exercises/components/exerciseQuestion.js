import React from 'react';

export const ExerciseQuestion = ({ questionImage, questionText, handleLoad }) => (
  questionImage ?
    <div
      className="question-image"
      style={{
        backgroundSize: 'contain',
        background: `url('${questionImage}') center center / contain no-repeat`,
        marginTop: '30px'
      }}
    >
      <img
        className="picture"
        style={{ display: 'none' }}
        src={questionImage}
        onLoad={handleLoad}
        alt=""
      />
    </div> :
    <div className="question-wrapper">
      <h2>
        {questionText}
      </h2>
    </div>
);
