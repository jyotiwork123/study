import React, { Component } from 'react';
import PropTypes from 'prop-types';

//utils, data
import difference from 'lodash/difference';
// import { replaceWithBigger } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';


//components
import ExerciseHeader from '../ExerciseHeader/index';
import PieChart from 'react-minimal-pie-chart';
import Metronome from '../../../common/components/Metronome'

//styles
import styles from './exerciseThreeA.css';


class ExerciseThreeA extends Component {
  static propTypes = {
    testIndex: PropTypes.number.isRequired,
    showNextTest: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    settings: PropTypes.array.isRequired,
    questionTitle: PropTypes.string,
    questionImage: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    questionAudio: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ])
  };
  static defaultProps = {
    questionTitle: '',
    questionImage: '',
    questionAudio: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      testChanged: false,
      answers: [],
      correct: true,
      shouldImagesRender: false,
      check: false,
      tempo: 0,
      audioLength: 0,
      played: 0,
    };

    this.correctSound = new Audio('../../sounds/correct.wav');
    this.incorrectSound = new Audio('../../sounds/incorrect.wav');
    this.testChanged = this.testChanged.bind(this);

    this.calculateTime = this.calculateTime.bind(this);
    this.initProgressBar = this.initProgressBar.bind(this);

    this.validateAnswer = this.validateAnswer.bind(this);
    this.validateResult = this.validateResult.bind(this);
    this.addBorder = this.addBorder.bind(this);
    this.shouldImagesRender = this.shouldImagesRender.bind(this);

    this.changeTempo = this.changeTempo.bind(this);
  }

  componentDidMount() {
    const { settings, intl: { formatMessage } } = this.props;
    this.setState({
      correct: false,
      testChanged: true,
      // shouldImagesRender: settings[2].value !== formatMessage(globalMessages.audioFirst),
      audioFirst: settings[2].value === formatMessage(globalMessages.audioFirst)
    });
    window.componentHandler.upgradeDom();
    this.initProgressBar();

  }

  componentWillReceiveProps(nextProps) {
    const { settings, exerciseState, currentTest, intl: { formatMessage } } = nextProps;
    if (nextProps.testIndex !== this.props.testIndex) {
      this.setState({
        tempo: 0,
        check: false,
        testChanged: true,
        shouldImagesRender: settings[2].value !== formatMessage(globalMessages.audioFirst),
        answers: exerciseState.reduce((a, c) => c.number === currentTest ? c.answers : a, []),
        correct: true,
        audioFirst: settings[2].value === formatMessage(globalMessages.audioFirst),
      });
    } else {
      this.setState({ testChanged: false });
    }
  }

  addBorder(index) {
    // geting number of current test, previews results and correct results
    const { currentTest, exerciseState, actions } = this.props;
    // geting results from state
    const currentAnswers = exerciseState.reduce((acc, c) => c.number === currentTest ? c.answers : acc, []);
    // if results from state includes our img (by index)
    if (currentAnswers.includes(index.toString())) {
      // geting correct results
      const correctResults = Object.keys(actions);
      // if correct results includes our img (by index)
      if (correctResults.includes(index.toString())) {
        return '5px solid #5cb85c';
      }
      return '5px solid red';
    }
    return '5px solid transparent';
  }

  validateAnswer(target, index) {
    const { answers } = this.state;
    const { actions } = this.props;
    const correctResults = Object.keys(actions);
    if (correctResults.indexOf(index.toString()) > -1) {
      this.correctSound.play();
      /* eslint-disable */
      target.style.border = '5px solid #5cb85c';
      /* eslint-enable */
      this.setState({ answers: [...answers, index.toString()], check: true });
      setTimeout(this.validateResult, 1000);
    } else {
      this.incorrectSound.play();
      /* eslint-disable */
      target.style.border = '5px solid red';
      /* eslint-enable */
      this.validateResult();
      this.setState({ answers: [...answers, index.toString()], correct: false });
    }
  }

  validateResult() {
    const { actions, showNextTest, addResultsToExerciseState } = this.props;
    const { answers, correct } = this.state;
    const correctResults = Object.keys(actions);

    if (!difference(correctResults, answers).length) {
      addResultsToExerciseState(answers);
      showNextTest(correct);
    }
  }

  testChanged() {
    this.setState({ testChanged: false });
  }

  shouldImagesRender() {
    this.setState({ shouldImagesRender: true });
  }


  changeTempo(tempo) {
    this.setState({ tempo }, () => {
      this.initProgressBar();
    });
  }

  calculateTime(currentTime, full) {
    let minutes = parseInt((currentTime / 60), 0) % 60;
    let seconds = parseInt((currentTime - minutes * 60), 0).toString().substr(0, 2);
    const deciSeconds = parseFloat((currentTime - minutes * 60), 2).toString().substr(2, 2) || '00';

    if (minutes < 10) { minutes = '0' + minutes; }
    if (seconds < 10) { seconds = '0' + seconds; }

    let time = '';

    if (full) {
      time = minutes + ':' + seconds + ':' + deciSeconds;
    } else {
      time = minutes + ':' + seconds;
    }
    return time;
  }

  initProgressBar() {
    const audioPlayer = document.getElementsByTagName('audio')[0];
    this.setState(
      {
        audioLength: !isNaN(audioPlayer.duration) ? audioPlayer.duration : 1,
        played: audioPlayer.currentTime
      }
    );
  }


  renderButton() {
    const { showNextTest } = this.props;
    const { formatMessage } = this.props.intl;
    return (
      <button
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button}`}
        onClick={() => { showNextTest(true, true); }}
      >
        {formatMessage(globalMessages.next)}
      </button>
    );
  }

  render() {
    const { audioFirst, testChanged, tempo, audioLength, played } = this.state;
    const {
      questionAudio,
      settings,
      instructions,
      updateCurrentSettings,
      showImages,
      push,
      template,
      timelineState,
      showNextTest,
      currentTest,
      exerciseName,
    } = this.props;
    const { formatMessage } = this.props.intl;

    const chartData = [
      { value: played, key: 1, color: '#32887A' },
      { value: audioLength - played, key: 2, color: 'rgba(0,0,0, 0.2)' }
    ];
    // console.log('chartData', chartData);
    console.log('styles1', styles)

    return (
      <div
        className="mdl-layout mdl-js-layout mdl-layout--fixed-header exercise-one-wrapper"
      >
        <ExerciseHeader
          // renderImages={this.shouldImagesRender}
          exerciseName={exerciseName}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          // audio={questionAudio}
          instructions={instructions}
          showImages={showImages}
          push={push}
          template={template}
          timelineState={timelineState}
          selectNextTest={showNextTest}
          currentTest={currentTest}
          testChanged={testChanged}
          audioFirst={audioFirst}
          testChangedFunc={this.testChanged}
        />
        <div
          key={questionAudio}
          className={styles.content}
        >
          {/* {shouldImagesRender && this.renderImages()} */}
          <div>{
            // questionAudio.map(audio => <p>{JSON.parse(audio).url}</p>)
          }</div>
          <div className="mdl-grid">
            <div className={`mdl-cell mdl-cell--12-col ${styles.container}`}>
              <h4>{instructions}</h4>
            </div>

            <div className={`mdl-cell mdl-cell--8-col`}>
              <div className={`${styles.audioContainer}`}>
                <audio
                  key={tempo}
                  className={styles.picker}
                  onTimeUpdate={this.initProgressBar}
                  // tabIndex={'-1'}
                  autoPlay={false}
                  loop={false}
                  controls
                  id={'audio-picker'}
                  src={JSON.parse(questionAudio[tempo]).url}
                >
                  {formatMessage(globalMessages.browserNotSupportFile)}
                </audio>
                <div className={`mdl-cell mdl-cell--12-col ${styles.container}`}>

                  <h5>{JSON.parse(questionAudio[tempo]).title}</h5>
                </div>
              </div>

            </div>
            <div className={`mdl-cell mdl-cell--4-col ${styles.changeTempoContainer}`}>
              <h5
                className={`${styles.changeTempoItem} ${tempo === 0 ? styles.tempoItemActive : ''}`}
                onClick={() => this.changeTempo(0)}
              >
                {formatMessage(globalMessages.normalSpeed)}
              </h5>
              <h5
                className={`${styles.changeTempoItem} ${tempo === 1 ? styles.tempoItemActive : ''}`}
                onClick={() => this.changeTempo(1)}
              >
                {formatMessage(globalMessages.slowSpeed)}
              </h5>
              <h5
                className={`${styles.changeTempoItem} ${tempo === 2 ? styles.tempoItemActive : ''}`}
                onClick={() => this.changeTempo(2)}
              >
                {formatMessage(globalMessages.extraSlowSpeed)}
              </h5>
            </div>
          </div>

          <div className="mdl-grid">
            <div className={`mdl-cell mdl-cell--4-col ${styles.chartBlock}`}>
              <label className={`${styles.switch}`} htmlFor='switch1'>
                <input type='checkbox' id='switch1' onChange={() => this.changeTempo(2)} />
                <span className={`${styles.slider} ${styles.sliderRound}`}>{}</span>
              </label>
              <label className={`${styles.switch}`} htmlFor='switch1'>
                <input type='checkbox' id='switch1' onChange={() => this.changeTempo(2)} />
                <span className={`${styles.slider} ${styles.sliderRound}`}>{}</span>
              </label>
              <label className={`${styles.switch}`} htmlFor='switch1'>
                <input type='checkbox' id='switch1' onChange={() => this.changeTempo(2)} />
                <span className={`${styles.slider} ${styles.sliderRound}`}>{}</span>
              </label>
              <label className={`${styles.switch}`} htmlFor='switch1'>
                <input type='checkbox' id='switch1' onChange={() => this.changeTempo(2)} />
                <span className={`${styles.slider} ${styles.sliderRound}`}>{}</span>
              </label>
            </div>
            <div className={`mdl-cell mdl-cell--4-col ${styles.chartBlock}`}>
              <Metronome />
            </div>
            <div className={`mdl-cell mdl-cell--4-col ${styles.chartBlock}`}>
              {
                chartData.length !== 0 && <div className={styles.resutsChartWrapper}>
                  <PieChart
                    data={chartData}
                    lineWidth={15}
                    startAngle={-90}
                    radius={50}
                  />
                  <span className={styles.resultPercentage}>
                    {this.calculateTime(played)}
                  </span><br />
                  {/* <span className={styles.resultLeft}>
                  {this.calculateTime(audioLength - played)}
                </span> */}
                </div>
              }
            </div>
          </div>

        </div>
        <footer className={styles.footer}>
          {this.renderButton(showNextTest)}
        </footer>
      </div>
    );
  }
}

export default injectIntl(ExerciseThreeA);
