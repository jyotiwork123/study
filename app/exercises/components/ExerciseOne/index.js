import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import { ExerciseQuestion } from '../exerciseQuestion';
import ExerciseHeader from '../ExerciseHeader/index';
import WarningPopup from '../WarningPopup/index';

//utils, data
import { setTextCase, checkGetUserMedia } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';
import { warnings } from '../../../common/dictionary/specific';

//styles
import styles from './exerciseOne.css';


class ExerciseOne extends Component {
  static propTypes = {
    showNextTest: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    settings: PropTypes.array.isRequired,
    audioState: PropTypes.string.isRequired,
    questionTitle: PropTypes.string,
    questionImage: PropTypes.string,
    questionAudio: PropTypes.string,
    id: PropTypes.string.isRequired,
  };
  static defaultProps = {
    questionTitle: '',
    questionImage: '',
    questionAudio: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      showAnswers: false,
      isConnected: false,
      hasRecordingDevice: true
    };

    this.showAnswers = this.showAnswers.bind(this);
    this.initDictate = this.initDictate.bind(this);
    this.message = this.message.bind(this);
    this.error = this.error.bind(this);
    this.serverStatus = this.serverStatus.bind(this);
    this.toggleListening = this.toggleListening.bind(this);
    this.formatTitle = this.formatTitle.bind(this);
    this.restartSpeechRecognition = this.restartSpeechRecognition.bind(this);
    this.initRecording = this.initRecording.bind(this);
    this.toggleRecording = this.toggleRecording.bind(this);
    this.showNext = this.showNext.bind(this);
    this.toggleAudioFunctions = this.toggleAudioFunctions.bind(this);
    this.onAudioEnded = this.onAudioEnded.bind(this);
    this.testChanged = this.testChanged.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    if (this.props.questionText) {
      this.setState({ showAnswers: true });
    }
    this.setState({ testChanged: true });

    if (this.props.audioFunctionality) {
      // this.initDictate();
      this.initRecording();
    }
  }

  componentWillReceiveProps(props) {
    if (props.questionImage !== this.props.questionImage || props.questionText !== this.props.questionText) {
      this.setState({
        showAnswers: false,
        testChanged: true
      });
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
    if (this.dictate) {
      this.dictate.stopListening();
    }
    if (this.audioRecorder && this.audioRecorder.isRecording()) {
      this.audioRecorder.finishRecording();
    }
  }

  onAudioEnded() {
    const { actions, showOverlay, id } = this.props;
    const { formatMessage } = this.props.intl;
    const actionsLowerCase = actions.toLowerCase();
    switch (actionsLowerCase) {
      case 'auto next after 1 second':
      case 'auto next after 2 seconds':
      case 'auto next after 3 seconds':
      case formatMessage(globalMessages.auto1).toLowerCase():
      case formatMessage(globalMessages.auto2).toLowerCase():
      case formatMessage(globalMessages.auto3).toLowerCase():
        this.timeout = setTimeout(this.showNext, actionsLowerCase.match(/\d+/g)[0] * 1000);
        break;
      default:
        break;
    }

    if (id === '58b52de74f4c127ad2c21b2d') {
      setTimeout(() => {
        showOverlay(false);
      }, 5000);
    } else {
      showOverlay(false);
    }
  }

  showAnswers() {
    const { id, showOverlay } = this.props;
    this.setState({ showAnswers: true });
    if (id === '58b52de74f4c127ad2c21b2d') {
      showOverlay('spinner');
    }
  }

  initRecording() {
    console.log('recording initialized');
    const { uploadBlob } = this.props;
    const AudioContext = window.AudioContext || window.webkitAudioContext;
    const audioCtx = new AudioContext();
    checkGetUserMedia();

    navigator.mediaDevices.getUserMedia({ audio: true })
      .then(stream => {
        const audioNode = audioCtx.createMediaStreamSource(stream);
        this.audioRecorder = new window.WebAudioRecorder(audioNode, {
          workerDir: '../../vendor/recorder/',
          encoding: 'mp3'
        });
        this.audioRecorder.setOptions({ encodeAfterRecord: true });

        this.audioRecorder.onComplete = (recorder, blob) => {
          uploadBlob(blob, `${this.answer}${Date.now()}.mp3`);
          //uncomment to hear what's been recorded
          // const url = URL.createObjectURL(blob);
          // const audio = new Audio();
          // audio.src = url;
          // audio.play();
        };
      })
      .catch((err) => {
        console.log(`${err.name}: ${err.message}`);
        if (err.name && err.name.search('PermissionDeniedError') !== -1) {
          console.log('no access to media devices');
          this.noAccess = true;
        }
      });
  }

  initDictate() {
    const that = this;
    this.dictate = new window.Dictate({
      server: 'wss://fells.cognuse.com/client/ws/speech',
      serverStatus: 'wss://fells.cognuse.com/client/ws/status',

      recorderWorkerPath: '../../../vendor/dictate/recorderWorker.js',
      onReadyForSpeech() {
        that.setState({ isConnected: true });
        that.message('READY FOR SPEECH');
      },
      onEndOfSpeech() {
        that.message('END OF SPEECH');
      },
      onEndOfSession() {
        that.setState({ isConnected: false });
        that.message('END OF SESSION');
      },
      onServerStatus(json) {
        that.serverStatus(json.num_workers_available);
      },
      onPartialResults(hypos) {
        const result = hypos[0].transcript.replace(/\+\+garbage\+\+/g, '');

        console.log('partial result', result);
        if (result.replace(/ /g, '').toLowerCase() === that.answer.replace(/ /g, '').toLowerCase()) {
          that.restartSpeechRecognition();
        }
      },
      onResults(hypos) {
        const result = hypos[0].transcript.replace(/\+\+garbage\+\+/g, '');

        console.log('result', result);
        if (result.replace(/ /g, '').toLowerCase() === that.answer.replace(/ /g, '').toLowerCase()) {
          that.restartSpeechRecognition();
        }
      },
      onError(code, data) {
        that.dictate.cancel();
        that.error(code, data);
      },
      onEvent(code, data) {
        that.message(code, data);
      }
    });
    this.dictate.init();
  }

  // Private methods (called from the callbacks)
  message(code, data) {
    console.debug(`DICTATE msg: ${code}: ${data || ''}`);
  }

  error(code, data) {
    if (data.search('No live audio input in this browser') > -1) {
      this.setState({ hasRecordingDevice: false });
    }
    console.error(`DICTATE ERR: ${code}: ${data || ''}`);
  }

  serverStatus(msg) {
    console.warn(`DICTATE server status: ${msg}`);
  }

  // Public methods (called from the GUI)
  toggleListening() {
    if (this.state.isConnected) {
      console.log('stop listening');
      this.dictate.stopListening();
    } else {
      console.log('start listening');
      this.dictate.startListening();
    }
  }

  restartSpeechRecognition() {
    if (this.dictate) {
      this.dictate.stopListening();
      this.dictate.startListening();
    }
    this.showNext(true);
  }

  toggleRecording() {
    if (this.audioRecorder && this.audioRecorder.isRecording()) {
      this.audioRecorder.finishRecording();
      console.log('recording finished');
    } else {
      this.audioRecorder.startRecording();
      console.log('recording started');
    }
    this.setState({ isConnected: !this.state.isConnected });
  }

  toggleAudioFunctions() {
    // this.toggleListening();
    if (!this.noAccess) {
      this.toggleRecording();
    } else {
      alert('to use this function please enable media devices access');
    }
  }

  testChanged() {
    this.setState({ testChanged: false });
  }

  showNext(answer) {
    const { showNextTest } = this.props;
    if (this.audioRecorder && this.audioRecorder.isRecording()) {
      this.audioRecorder.finishRecording();
      console.log('recording finished');
      this.setState({ isConnected: false });
    }
    showNextTest(answer);
  }

  formatTitle() {
    const { audioTitle: correctAnswer, settings, intl: { formatMessage } } = this.props;
    const correctAnswerSetting = settings[3].value.toLowerCase();
    this.answer = correctAnswer;

    switch (correctAnswerSetting) {
      case formatMessage(globalMessages.word).toLowerCase():
      case 'word/sentence':
        return correctAnswer;
      case formatMessage(globalMessages.dotted).toLowerCase():
      case 'dotted':
        return `${correctAnswer[0]} ${'_ '.repeat(correctAnswer.length - 2)}${correctAnswer[correctAnswer.length - 1]}`;
      default:
        return '';
    }
  }

  renderButtons() {
    const { actions } = this.props;
    const { formatMessage } = this.props.intl;
    const actionsLowerCase = actions.toLowerCase();
    switch (actionsLowerCase) {
      case 'next button':
      case formatMessage(globalMessages.nextButton).toLowerCase():
        return (
          <div className={styles.answerWrapper}>
            <div
              className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored next-button"
              onClick={() => {
                this.showNext(false);
              }}
            >
              {formatMessage(globalMessages.next)}
            </div>
          </div>
        );
      case 'correct & incorrect buttons':
      case formatMessage(globalMessages.correctIncorrect).toLowerCase():
        return (
          <div className={styles.answerWrapper}>
            <div
              className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored correct-button"
              onClick={() => {
                this.showNext(true);
              }}
            >
              {formatMessage(globalMessages.correct)}
            </div>
            <div
              className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored incorrect-button"
              onClick={() => {
                this.showNext(false);
              }}
            >
              {formatMessage(globalMessages.incorrect)}
            </div>
          </div>
        );
      case 'speech recognition':
      case formatMessage(globalMessages.speechRecognition).toLowerCase():
        return (
          <div className={styles.answerWrapper}>
            <div
              className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored incorrect-button"
              onClick={this.toggleAudioFunctions}
            >
              <i className="material-icons">{this.state.isConnected ? 'pause' : 'mic'}</i>
            </div>
            <div
              className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored correct-button"
              onClick={this.restartSpeechRecognition}
            >
              {formatMessage(globalMessages.skip)}
            </div>
          </div>
        );
      default:
        break;
    }
  }

  render() {
    const { testChanged, showAnswers, hasRecordingDevice } = this.state;
    const {
      exerciseName,
      id,
      audioFunctionality,
      questionImage,
      questionAudio,
      settings,
      audioState,
      instructions,
      updateCurrentSettings,
      push,
      template,
      timelineState,
      currentTest,
      showNextTest,
      intl: { formatMessage }
    } = this.props;

    if (audioFunctionality && !window.navigator.getUserMedia) {
      return <WarningPopup text={formatMessage(warnings.noAudioSupport)} />;
    }

    if (audioFunctionality && !hasRecordingDevice) {
      return <WarningPopup text={formatMessage(warnings.noAudioDevice)} />;
    }

    const stylesObj = {
      textTransform: setTextCase(settings[1].value)
    };
    if (id === '58b52de74f4c127ad2c21b2d') {
      stylesObj.display = `${showAnswers ? 'flex' : 'none'}`;
    }

    return (
      <div
        className="mdl-layout mdl-js-layout mdl-layout--fixed-header exercise-one-wrapper"
        style={stylesObj}
      >
        <ExerciseHeader
          exerciseName={exerciseName}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          audio={questionAudio}
          instructions={instructions}
          audioState={id !== '58b52e0a4f4c127ad2c21b2f' && !showAnswers && audioState}
          pictureTitle={this.formatTitle()}
          onAudioEnded={this.onAudioEnded}
          push={push}
          template={template}
          timelineState={timelineState}
          currentTest={currentTest}
          selectNextTest={showNextTest}
          testChanged={testChanged}
          testChangedFunc={this.testChanged}
        />
        <div
          className={styles.content}
          onLoad={this.showAnswers}
        >
          <ExerciseQuestion questionImage={questionImage} />
          {showAnswers && this.renderButtons()}
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseOne);
