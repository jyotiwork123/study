import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DragLayer } from 'react-dnd';
import { supportsTouch } from '../helpers';

const layerStyles = {
  position: 'fixed',
  pointerEvents: 'none',
  zIndex: 100,
  left: 0,
  top: 0,
  width: '100%',
  height: '100%'
};

const getItemStyles = (props) => {
  const { initialOffset, currentOffset } = props;
  return (!initialOffset || !currentOffset)
    ? { display: 'none' }
    : {
      transform: `translate(${currentOffset.x}px, ${currentOffset.y}px)`,
      WebkitTransform: `translate(${currentOffset.x}px, ${currentOffset.y}px)`
    };
};

@DragLayer(monitor => ({
  item: monitor.getItem(),
  itemType: monitor.getItemType(),
  initialOffset: monitor.getInitialSourceClientOffset(),
  currentOffset: monitor.getSourceClientOffset(),
  isDragging: monitor.isDragging()
}))
export default class CustomDragLayer extends Component {
  static propTypes = {
    item: PropTypes.object,
    itemType: PropTypes.string,
    initialOffset: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired
    }),
    currentOffset: PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired
    }),
    isDragging: PropTypes.bool.isRequired
  };

  render() {
    const { item, isDragging, answersCase } = this.props;
    if (!isDragging || !supportsTouch()) {
      return null;
    }
    return (
      <div
        style={Object.assign({}, layerStyles, { textTransform: answersCase })}
        className={`custom-drag exercise-${this.props.exerciseNumber}`}
      >
        <div
          style={Object.assign({}, getItemStyles(this.props), { textTransform: answersCase })}
          className="button-wrapper"
        >
          <button
            className={'mdl-button mdl-js-button mdl-button--raised mdl-button--colored '}
            style={{ textTransform: answersCase }}
          >
            {item.answer}
          </button>
        </div>
      </div>
    );
  }
}
