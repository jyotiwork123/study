import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import Timer from '../../../common/components/Timer/index';
import SettingsModal from '../../containers/SettingsModal/index';
import Timeline from '../ExerciseTimeline';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';


//assets
import listenAgain from '../../../../images/listen_again.svg';

//styles
import styles from './exerciseHeader.css';

class ExerciseHeader extends Component {
  static propTypes = {
    settings: PropTypes.array.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    audio: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    questionTitle: PropTypes.string,
    instructions: PropTypes.string,
    pictureTitle: PropTypes.string,
    onAudioEnded: PropTypes.func
  };
  static defaultProps = {
    questionTitle: '',
    audio: '',
    instructions: '',
    pictureTitle: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      showPlayButton: true,
      currentAudio: false,
    };

    this.closeExercise = this.closeExercise.bind(this);
    this.toggleSettingsModal = this.toggleSettingsModal.bind(this);
    this.playAudio = this.playAudio.bind(this);
    this.renderAudioNodes = this.renderAudioNodes.bind(this);
    this.playMultipleAudio = this.playMultipleAudio.bind(this);
    this.playAudioInc = this.playAudioInc.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  componentWillReceiveProps(props) {
    const { audio } = this.props;
    const { testChangedFunc, renderImages, testChanged, currentTest, timelineState: { completed } } = props;
    if (audio && testChanged) {
      if (completed.includes(currentTest + 1)) {
        renderImages();
        testChangedFunc();
      } else if (typeof audio === 'string') {
        setTimeout(this.playAudio, 500);
      } else {
        if (!audio['2']) {
          setTimeout(this.playAudio, 500);
        }
        if (audio['2']) {
          setTimeout(this.playMultipleAudio, 500);
        }
      }
    }
    this.setState({ currentAudio: false });
  }

  closeExercise() {
    this.props.push(`/practice${window.location.pathname.replace('/exercises/', '?preview/')}`);
  }

  playAudio() {
    const { audio, testChangedFunc } = this.props;
    if (typeof audio === 'string') {
      this.audio.load();
      this.audio.play();
      testChangedFunc();
    } else {
      this['audio-0'].load();
      this['audio-0'].play();
    }
  }

  playAudioInc() {
    const { currentAudio } = this.state;
    if (this[`audio-${currentAudio}`]) {
      this[`audio-${currentAudio}`].load();
      this[`audio-${currentAudio}`].play();
      this.setState({
        currentAudio: currentAudio + 1,
      });
    } else {
      this['audio-0'].load();
      this['audio-0'].play();
      this.setState({
        currentAudio: 1,
      });
    }
  }

  playMultipleAudio(index = 0, length) {
    if (index === 0) {
      // this.setState({ showPlayButton: false });
    }
    if (index === length) {
      this.setState({ showPlayButton: true });
      if (this.props.showImages) {
        this.props.showImages();
      }
    }
    if (this[`audio-${index}`]) {
      this[`audio-${index}`].load();
      this[`audio-${index}`].play();
    }
  }

  toggleSettingsModal() {
    this.setState({ modalIsOpen: !this.state.modalIsOpen });
  }

  renderAudioNodes() {
    const {
      intl: { formatMessage },
      settings,
      questionTitle,
      audio,
      instructions,
      pictureTitle,
      onAudioEnded,
      renderImages,
      testChangedFunc
    } = this.props;
    if ((typeof audio).toLowerCase() === 'string' && !audio.length) {
      return (
        <div className={`mdl-layout-title ${styles.centerText}`}>
          {questionTitle}
        </div>
      );
    }
    if ((typeof audio).toLowerCase() === 'string') {
      return (
        <div className={`mdl-layout-title ${styles.centerText}`}>
          <h4 className={styles.title}>
            { this.state.showPlayButton &&
              <img
                className={styles.audioButton}
                src={listenAgain}
                alt=""
                onClick={() => {
                  this.playAudio();
                }}
              />
            }
            {`${instructions}${pictureTitle ? ':' : ''} ${pictureTitle}`}
          </h4>
          <audio
            preload="none"
            ref={el => {
              this.audio = el;
            }}
            onEnded={() => {
              if (settings[2].value === formatMessage(globalMessages.audioFirst)) {
                renderImages();
                testChangedFunc();
              }
              if (onAudioEnded) {
                onAudioEnded();
              }
            }}
          >
            <source src={audio} type="audio/mp3" />
          </audio>
        </div>
      );
    }

    return (
      <div className={`mdl-layout-title ${styles.centerText}`}>
        <h4 className={styles.title}>
          { this.state.showPlayButton &&
            <img
              className={styles.audioButton}
              src={listenAgain}
              alt=""
              onClick={() => {
                // this.playMultipleAudio();
                this.playAudioInc();
              }}
            />
          }
          {`${instructions}${pictureTitle ? ':' : ''} ${pictureTitle}`}
        </h4>
        {
          audio.map((record, index, audios) =>
            <audio
              className="exercise-audio"
              preload="none"
              key={record.key}
              ref={(el) => {
                this[`audio-${index}`] = el;
              }}
              onEnded={() => {
                if (index === audios.length - 1) {
                  if (settings[2].value === formatMessage(globalMessages.audioFirst)) {
                    renderImages();
                    testChangedFunc();
                  }
                }
                if (typeof this.state.currentAudio !== 'number') {
                  this.playMultipleAudio(index + 1, audios.length);
                }
              }}
            >
              <source
                src={record.url}
                type="audio/mp3"
              />
            </audio>
          )
        }
      </div>
    );
  }

  render() {
    const {
      exerciseName,
      settings,
      updateCurrentSettings,
      countTests,
      currentTest,
      selectNextTest,
      timelineState,
      template,
      intl: { formatMessage }
    } = this.props;
    const hasTimeline = settings.reduce((a, c) =>
      c.label === formatMessage(globalMessages.timeline)
        ? c.value
        : a
      , formatMessage(globalMessages.disable)
    );
    return (
      <header>
      <div className={`mdl-layout__header ${styles.header}`}>
        <div className={`mdl-layout__header-row ${styles.headerRow}`}>
          <div>
            <div className={styles.exerciseName}>
              {exerciseName}
            </div>
            <div>
              {settings.length && <Timer endtime={parseInt(settings[0].value, 10)} />}
            </div>
          </div>
          <div className="mdl-layout-spacer" />
          {this.renderAudioNodes()}
          <div className="mdl-layout-spacer" />
          <nav className={`mdl-navigation ${styles.rightSide}`}>
            <i className={`material-icons ${styles.icon}`} onClick={this.toggleSettingsModal}>
              settings
            </i>
            <i className={`material-icons ${styles.icon}`}
             onClick={this.closeExercise}>
              input
            </i>
          </nav>
        </div>
        <SettingsModal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.toggleSettingsModal}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          toggleSettingsModal={this.toggleSettingsModal}
        />
      </div>
      {hasTimeline === formatMessage(globalMessages.enable) &&
        <Timeline
            timelineState={timelineState}
            max={countTests}
            currentTest={currentTest}
            selectNextTest={selectNextTest}
            template={template}
        />
      }
    </header>
    );
  }
}

export default injectIntl(ExerciseHeader);
