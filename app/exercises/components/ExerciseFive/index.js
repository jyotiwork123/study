import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import { ExerciseQuestion } from '../exerciseQuestion';
import ExerciseHeader from '../ExerciseHeader/';

//utils, data
import shortid from 'shortid';
import { setExerciseDelay, setTextCase } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseFive.css';


class ExerciseFive extends Component {
  static propTypes = {
    showNextTest: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    testQuestions: PropTypes.object.isRequired,
    settings: PropTypes.array.isRequired,
    questionTitle: PropTypes.string,
    questionImage: PropTypes.string,
    questionText: PropTypes.string,
    template: PropTypes.string,
  };
  static defaultProps = {
    questionTitle: '',
    questionImage: '',
    questionText: '',
    template: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      showAnswers: false,
      answers: [],
      testQuestions: [],
      correctAnswer: '',
    };

    this.correctSound = new Audio('../../sounds/correct.wav');
    this.incorrectSound = new Audio('../../sounds/incorrect.wav');

    this.renderAnswers = this.renderAnswers.bind(this);
    this.showAnswers = this.showAnswers.bind(this);
    this.validateAnswer = this.validateAnswer.bind(this);
  }

  componentDidMount() {
    const { testQuestions: { testQuestions, correctAnswer } } = this.props;
    window.componentHandler.upgradeDom();
    if (this.props.questionText) {
      this.setState({ showAnswers: true });
    }
    this.setState({ testQuestions, correctAnswer });
  }

  componentWillReceiveProps(props) {
    const { exerciseState, currentTest, testQuestions: { testQuestions, correctAnswer } } = props;
    if (props.questionImage !== this.props.questionImage || props.questionText !== this.props.questionText) {
      const questions = exerciseState.reduce((acc, c) =>
        (c.number === currentTest)
          ? c.questions
          : acc
      , []);

      const correct = exerciseState.reduce((acc, c) =>
        (c.number === currentTest)
          ? c.correct
          : acc
      , '');

      this.setState({
        showAnswers: false,
        answers: exerciseState.reduce((acc, c) =>
          (c.number === currentTest)
            ? c.answers
            : acc
        , []),
        testQuestions: questions.length && questions || testQuestions,
        correctAnswer: correct || correctAnswer
      });
    }
    if (props.questionText !== this.props.questionText) {
      setTimeout(this.showAnswers, 500);
    }
  }

  showAnswers() {
    this.setState({ showAnswers: true });
  }

  validateAnswer(event, answer) {
    const {
      showNextTest,
      settings,
      addResultsToExerciseState,
    } = this.props;
    const { testQuestions, correctAnswer } = this.state;

    const nextTestDelay = setExerciseDelay(settings[1].value);
    this.setState({
      answers: [...this.state.answers, answer]
    });

    if (correctAnswer !== answer) {
      this.incorrectSound.play();
      showNextTest(false);
    } else {
      this.correctSound.play();
      setTimeout(() => {
        addResultsToExerciseState(this.state.answers, testQuestions, correctAnswer);
        showNextTest(true);
      }, nextTestDelay);
    }
  }

  renderAnswers({ correctAnswer, testQuestions }, settings) {
    const { intl: { formatMessage } } = this.props;
    const answersCase = setTextCase(settings[3].value);
    return testQuestions.map((answer) => {
      const correctAnswerClass = answer === correctAnswer ? answer : '';
      const hasBackground = this.state.answers.includes(answer);
      let background = 'fakeColor';
      if (hasBackground && answer === correctAnswer) {
        background = '#4caf50';
      } else if (hasBackground && answer !== correctAnswer) {
        background = 'red';
      }
      const hide = hasBackground && answer === correctAnswer && settings[2].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase();
      return (
        <div
          className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${hide ? styles.hiddenButton : styles.visibleButton} ${correctAnswerClass} ${styles.answer}`}
          style={{
            textTransform: answersCase,
            background,
          }}
          key={shortid.generate()}
          onClick={(e) => this.validateAnswer(e, answer)}
        >
          {answer}
        </div>
      );
    });
  }

  render() {
    const {
      exerciseName,
      questionTitle,
      questionImage,
      questionText,
      settings,
      updateCurrentSettings,
      push,
      template,
      timelineState,
      showNextTest,
      currentTest,
      exerciseState,
      intl: { formatMessage }
    } = this.props;
    const answersFirst = settings[4].value === formatMessage(globalMessages.answersFirst);
    const imageAndText = template === '5b';
    const imageStyles = {
      backgroundSize: 'contain',
      background: `url('${questionImage}') center center / contain no-repeat`
    };

    const hasOverlay = exerciseState && exerciseState.reduce((a, c) => (currentTest === c.number) || a, false) || false;

    return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <ExerciseHeader
          exerciseName={exerciseName}
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
          template={template}
          timelineState={timelineState}
          selectNextTest={showNextTest}
          currentTest={currentTest}
        />
        <div
          className={styles.content}
          onLoad={this.showAnswers}
          style={{ display: `${this.state.showAnswers ? 'block' : 'none'}` }}
        >
          {answersFirst && !imageAndText &&
            <div className={styles.answerWrapper}>
              {this.renderAnswers(this.state, settings)}
            </div>
          }

          {!imageAndText &&
            <ExerciseQuestion
              questionImage={questionImage}
              questionText={questionText}
            />
          }

          {imageAndText &&
            <div className="question-image-text" style={imageStyles}>
              <img className="picture" src={questionImage} alt="" style={{ display: 'none' }} />
            </div>
          }
          {answersFirst && imageAndText &&
            <div className={styles.answerWrapper}>
              {this.renderAnswers(this.state, settings)}
            </div>
          }
          {imageAndText &&
            <div className={styles.questionWrapper}>
              <h2>
                {questionText}
              </h2>
            </div>
          }

          {!answersFirst &&
            <div className={styles.answerWrapper}>
              {this.renderAnswers(this.state, settings)}
            </div>
          }
          {hasOverlay ? <div className={styles.overlayIfTestDone} /> : null}
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseFive);
