import React, { Component } from 'react';
import PropTypes from 'prop-types';

//utils, data
import difference from 'lodash/difference';
import { replaceWithBigger } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';


//components
import ExerciseHeader from '../ExerciseHeader/index';

//styles
import styles from './exerciseThree.css';


class ExerciseThree extends Component {
  static propTypes = {
    testIndex: PropTypes.number.isRequired,
    showNextTest: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    settings: PropTypes.array.isRequired,
    questionTitle: PropTypes.string,
    questionImage: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    questionAudio: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ])
  };
  static defaultProps = {
    questionTitle: '',
    questionImage: '',
    questionAudio: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      testChanged: false,
      answers: [],
      correct: true,
      shouldImagesRender: false,
      check: false,
    };

    this.correctSound = new Audio('../../sounds/correct.wav');
    this.incorrectSound = new Audio('../../sounds/incorrect.wav');

    this.testChanged = this.testChanged.bind(this);
    this.renderImages = this.renderImages.bind(this);
    this.validateAnswer = this.validateAnswer.bind(this);
    this.validateResult = this.validateResult.bind(this);
    this.addBorder = this.addBorder.bind(this);
    this.shouldImagesRender = this.shouldImagesRender.bind(this);
  }

  componentDidMount() {
    const { settings, intl: { formatMessage } } = this.props;
    this.setState({
      correct: false,
      testChanged: true,
      shouldImagesRender: settings[2].value !== formatMessage(globalMessages.audioFirst),
      audioFirst: settings[2].value === formatMessage(globalMessages.audioFirst)
    });
    window.componentHandler.upgradeDom();
  }

  componentWillReceiveProps(nextProps) {
    const { settings, exerciseState, currentTest, intl: { formatMessage } } = nextProps;
    if (nextProps.testIndex !== this.props.testIndex) {
      this.setState({
        check: false,
        testChanged: true,
        shouldImagesRender: settings[2].value !== formatMessage(globalMessages.audioFirst),
        answers: exerciseState.reduce((a, c) => c.number === currentTest ? c.answers : a, []),
        correct: true,
        audioFirst: settings[2].value === formatMessage(globalMessages.audioFirst),
      });
    } else {
      this.setState({ testChanged: false });
    }
  }

  addBorder(index) {
    // geting number of current test, previews results and correct results
    const { currentTest, exerciseState, actions } = this.props;
    // geting results from state
    const currentAnswers = exerciseState.reduce((acc, c) => c.number === currentTest ? c.answers : acc, []);
    // if results from state includes our img (by index)
    if (currentAnswers.includes(index.toString())) {
      // geting correct results
      const correctResults = Object.keys(actions);
      // if correct results includes our img (by index)
      if (correctResults.includes(index.toString())) {
        return '5px solid #5cb85c';
      }
      return '5px solid red';
    }
    return '5px solid transparent';
  }

  validateAnswer(target, index) {
    const { answers } = this.state;
    const { actions } = this.props;
    const correctResults = Object.keys(actions);
    if (correctResults.indexOf(index.toString()) > -1) {
      this.correctSound.play();
      /* eslint-disable */
      target.style.border = '5px solid #5cb85c';
      /* eslint-enable */
      this.setState({ answers: [...answers, index.toString()], check: true });
      setTimeout(this.validateResult, 1000);
    } else {
      this.incorrectSound.play();
      /* eslint-disable */
      target.style.border = '5px solid red';
      /* eslint-enable */
      this.validateResult();
      this.setState({ answers: [...answers, index.toString()], correct: false });
    }
  }

  validateResult() {
    const { actions, showNextTest, addResultsToExerciseState } = this.props;
    const { answers, correct } = this.state;
    const correctResults = Object.keys(actions);

    if (!difference(correctResults, answers).length) {
      addResultsToExerciseState(answers);
      showNextTest(correct);
    }
  }

  testChanged() {
    this.setState({ testChanged: false });
  }

  shouldImagesRender() {
    this.setState({ shouldImagesRender: true });
  }

  renderImages() {
    const {
      questionImage,
      exerciseState,
      currentTest,
    } = this.props;
    let layoutClass;

    switch (questionImage.length) {
      case 2:
        layoutClass = `${styles.oneRow} ${styles.twoInRow}`;
        break;
      case 4:
        layoutClass = `${styles.twoRows} ${styles.twoInRow}`;
        break;
      case 6:
        layoutClass = `${styles.twoRows} ${styles.threeInRow}`;
        break;
      case 8:
      default:
        layoutClass = `${styles.twoRows} ${styles.fourInRow}`;
    }

    const hasOverlay = exerciseState && exerciseState.reduce((a, c) => (currentTest === c.number) || a, false) || false;

    return (
      <div className={styles.layoutWrapper}>
        {
          questionImage.map((image, index) => (
            <div
              className={`${layoutClass} ${styles.imageWrapper}`}
              style={{ border: this.addBorder(index) }}
              key={image.key}
              ref={el => this[image.key] = el}
              onClick={() => {
                if (!this.state.check) {
                  this.validateAnswer(this[image.key], index);
                }
              }}
            >
              <img
                src={replaceWithBigger(image.url)}
                alt=""
                className={styles.image}
                onLoad={this.testChanged}
              />
            </div>
          ))
        }
        {hasOverlay ? <div className={styles.overlayIfTestDone} /> : null}
      </div>
    );
  }

  render() {
    const { audioFirst, shouldImagesRender, testChanged } = this.state;
    const {
      questionAudio,
      settings,
      instructions,
      updateCurrentSettings,
      showImages,
      push,
      template,
      timelineState,
      showNextTest,
      currentTest,
      exerciseName,
    } = this.props;
    console.log(questionAudio)

    return (
      <div
        className="mdl-layout mdl-js-layout mdl-layout--fixed-header exercise-one-wrapper"
      >
        <ExerciseHeader
          renderImages={this.shouldImagesRender}
          exerciseName={exerciseName}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          audio={questionAudio}
          instructions={instructions}
          showImages={showImages}
          push={push}
          template={template}
          timelineState={timelineState}
          selectNextTest={showNextTest}
          currentTest={currentTest}
          testChanged={testChanged}
          audioFirst={audioFirst}
          testChangedFunc={this.testChanged}
        />
        <div
          className={styles.content}
        >
          {shouldImagesRender && this.renderImages()}
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseThree);
