import axios from 'axios';
//TODO: use thunk third argument applyMiddleware(thunk.withExtraArgument(arg)
//utils, data
import { sortBy } from 'lodash';
import { getQuery, getMutation, getContentQuery } from '../../common/queries';
import { getCorrectTime, formatDate } from '../helpers.js';

// api
import {
  GRAPHQL_URL,
  GRAPHQL_URL_RESULTS,
  AUDIOS_URL,
  PATIENTS_URL,
  apiConfig
} from '../../common/api';

//constants
import {
  ADD_TO_FAVOURITES,
  REMOVE_FROM_FAVOURITES,
  ADD_TO_SUGGESTIONS,
  REMOVE_FROM_SUGGESTIONS,
  ADD_TO_FAVOURITES_LIST,
  ADD_TO_SUGGESTIONS_LIST,
  SET_VOICE_GENDER,
  SET_SECTIONS,
  SET_FILTERS,
  SET_TOPICS,
  MODIFY_EXERCISES,
  MODIFY_SECTIONS,
  MODIFY_CONTENT,
  MODIFY_ACCESS,
  RESET_EXERCISES,
  GET_EXERCISE_PREVIEW,
  GET_EXERCISES,
  GET_SINGLE_EXERCISE,
  GET_TEMPLATE_ONE_EXERCISE,
  GET_TEMPLATE_THREE_EXERCISE,
  GET_TEMPLATE_SIX_EXERCISE,
  GET_TEMPLATE_NINE_EXERCISE,
  UPDATE_SETTINGS,
  UPDATE_TESTS,
  RESET_CURRENT_EXERCISE,
  RESET_LAST_EXERCISE,
  SHOW_OVERLAY,
  TEMPLATES_GET_SKIPPED_WORDS_OPTIONS,
  SET_LANGUAGE_FILTER,
  SET_SELECTED_TOPIC,
  EXERCISE_DURATION,
  EXERCISE_RESULT,
  STUTTERING_EXERCISE_RESULT,
  RESET_CURRENT_EXERCISE_STATE,
  ADD_EXERCISE_THREE_RESULTS,
  ADD_EXERCISE_FOUR_RESULTS,
  ADD_EXERCISE_FIVE_RESULTS,
  ADD_EXERCISE_SEVEN_RESULTS,
  ADD_EXERCISE_SEVEN_A_RESULTS,
  ADD_EXERCISE_NINE_RESULTS,
  ADD_EXERCISE_SIX_RESULTS,
} from './constants';

import { PATIENT_SET_SUGGESTED } from '../../patient/state/constants';

export function getFavoriteExercises(queryObj) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
      .then(result => {
        const ids = result.data.data.favoriteExercises.map(({ exercise }) => exercise);
        ids.forEach(id => {
          const queryObj = getQuery(id, 'exercisePreview');
          axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
            .then(result => {
              dispatch({
                type: ADD_TO_FAVOURITES,
                payload: result.data.data.exercise
              });
            })
            .catch(err => {
              console.error(err);
            });
        });
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export const addToFavourites = (exercise) => (dispatch, getState) => {
  // const token = getState().User.currentUser.token;
  // const queryObj = getMutation('createFavoriteExercise', exercise._id);
  // axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
  //   .then(() => {
  //     // dispatch({
  //     //   type: ADD_TO_FAVOURITES,
  //     //   payload: exercise
  //     // });
  //   }, error => {
  //     throw new Error(`rejected ${error.message}`);
  //   });

  dispatch({
    type: ADD_TO_FAVOURITES,
    payload: exercise
  });
};

export const removeFromFavourites = (exercise) => (dispatch, getState) => {
  // const token = getState().User.currentUser.token;
  // const queryObj = getMutation('removeFavoriteExercise', exercise._id);
  // axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
  //   .then(() => {
  //     // dispatch({
  //     //   type: REMOVE_FROM_FAVOURITES,
  //     //   payload: exercise
  //     // });
  //   }, error => {
  //     throw new Error(`rejected ${error.message}`);
  //   });

  dispatch({
    type: REMOVE_FROM_FAVOURITES,
    payload: exercise
  });
};


export const addToSuggestions = (exercise, patientId) => (dispatch, getState) => {
  const token = getState().User.currentUser.token;
  const payload = {
    action: 'add-suggestion',
    suggestion: exercise._id,
  };
  // console.log('payload', payload);

  axios.put(`${PATIENTS_URL}/${patientId}`, payload, apiConfig(token))
    .then((result) => {
      dispatch({
        type: PATIENT_SET_SUGGESTED,
        payload: result.data.patient
      });
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
};

export const removeFromSuggestions = (exercise, patientId) => (dispatch, getState) => {
  const token = getState().User.currentUser.token;
  const payload = {
    action: 'delete-suggestion',
    suggestion: exercise._id,
  };
  axios.put(`${PATIENTS_URL}/${patientId}`, payload, apiConfig(token))
    .then((result) => {
      // console.log(result);
      dispatch({
        type: PATIENT_SET_SUGGESTED,
        payload: result.data.patient
      });
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });

  dispatch({
    type: REMOVE_FROM_SUGGESTIONS,
    payload: exercise
  });
};

export const setSelectedTopic = (topic) => ({
  type: SET_SELECTED_TOPIC,
  payload: topic
});

export const setFilters = (filterObj) => ({
  type: SET_FILTERS,
  payload: filterObj
});

export const setSections = (sections) => ({
  type: SET_SECTIONS,
  payload: sections
});

export const setPrefferedVoiceGender = (gender) => ({
  type: SET_VOICE_GENDER,
  payload: gender
});
export const modifyExercises = (modifier, value) => (dispatch) => {
  dispatch({
    type: SET_LANGUAGE_FILTER,
    payload: {
      modifier,
      value
    }
  });
  dispatch({
    type: MODIFY_EXERCISES
  });
};

export const modifySections = (modifier) => ({
  type: MODIFY_SECTIONS,
  payload: modifier
});

const __modifyAccess = (modifier, id) => ({
    type: MODIFY_ACCESS,
    payload: {
      modifier,
      id
    }
  });

export const modifyAccess = (modifier) => (dispatch, getState) => {
  dispatch(__modifyAccess(modifier, getState().User.currentUser.id));
};

export const modifyContent = (modifier) => (dispatch, getState) => {

  dispatch({
    type: MODIFY_CONTENT,
    payload: modifier
  });
  dispatch(__modifyAccess(getState().Exercises.accessType, getState().User.currentUser.id));
};

export const getExercises = (queryObj) => (dispatch, getState) => {
  const { id, token, role } = getState().User.currentUser;
  const { accessType } = getState().Exercises;

  axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
    .then(result => {
      const exercises = result.data.data.exercises;
      const modifier = exercises.some(el => el.createdBy === id) ?
        'mine' : 'others';

      dispatch({
        type: GET_EXERCISES,
        payload: {
          id,
          role,
          exercises
        }
      });

      if (accessType === 'mine') {
        dispatch(__modifyAccess(modifier, id));
      } else {
        dispatch(__modifyAccess('others', id));
      }
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
};

export const updateDuration = (duration) => ({
  type: EXERCISE_DURATION,
  payload: duration
});

export const updateResult = (correct) => 
  // console.log('updateResult', { correct });

   ({
    type: EXERCISE_RESULT,
    payload: correct
  });

export const updateStutteringResult = (correct, incorrect) => 
  // console.log('updateStutteringResult', { correct, incorrect });

   ({
    type: STUTTERING_EXERCISE_RESULT,
    payload: { correct, incorrect }
  });

export const saveResults = (results, type = '', timeCorrect = 0, timeFailed = 0) => (dispatch, getState) => {
  // console.log('saveResults', { results, type, timeCorrect, timeFailed });

  const token = getState().User.currentUser.token;
  dispatch(updateDuration(results.variables.input.timeTotal));

  if (type && type === 'stuttering') {
    dispatch(updateStutteringResult(timeCorrect, timeFailed));
  } else {
    dispatch(updateResult(+results.variables.input.correct));
  }

  axios.post(GRAPHQL_URL_RESULTS, results, apiConfig(token))
    .then(() => {
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
};

export const getExercisePreview = (queryObj, groupsQuery) => (dispatch, getState) => {
  const token = getState().User.currentUser.token;
  Promise.all([
    axios.post(GRAPHQL_URL, queryObj, apiConfig(token)),
    axios.post(GRAPHQL_URL, groupsQuery, apiConfig(token))
  ]).then(result => {
    dispatch({
      type: GET_EXERCISE_PREVIEW,
      payload: result[0].data.data.exercise
    });

    if (!result[1].data.data.exerciseGroups || result[1].data.data.exerciseGroups.some(el => !el.title)) {
      return;
    }
    dispatch({
      type: SET_TOPICS,
      payload: result[1].data.data.exerciseGroups
    });
  }, error => {
    throw new Error(`rejected ${error.message}`);
  });
};

export const getExercise = (exerciseQuery, viewsQuery) => (dispatch, getState) => {
  const token = getState().User.currentUser.token;
  const config = apiConfig(token);
  return Promise.all([
    axios.post(GRAPHQL_URL, exerciseQuery, config),
    axios.post(GRAPHQL_URL, viewsQuery, config)
  ])
    .then(result => {
      const exerciseData = result[0].data.data.exercise;
      const views = result[1].data.data.views;
      const exercise = Object.assign({}, exerciseData, { views });

      dispatch({
        type: getActionType(exercise.template),
        payload: exercise
      });

      return exercise;
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
};

const getActionType = (template) => {
  // console.log('template', template)
  switch (template) {
    case '1':
      return GET_TEMPLATE_ONE_EXERCISE;
    case '3':
    case '4':
    case '4a':
    case '4b':
      return GET_TEMPLATE_THREE_EXERCISE;
    case '6':
      return GET_TEMPLATE_SIX_EXERCISE;
    case '9':
      return GET_TEMPLATE_NINE_EXERCISE;
    default:
      return GET_SINGLE_EXERCISE;
  }
};

export const getOptions = (locale) => (dispatch, getState) => {
  const token = getState().User.currentUser.token;
  const queryList = getContentQuery(locale);

  return Promise.all(
    queryList.map(query => axios.post(GRAPHQL_URL, query, apiConfig(token)))
  )
    .then(result => {
      const payload = {};
      result.forEach(({ data: { data } }) => (
        Object.assign(payload, {
          [data.content[0].topic]: sortBy(data.content, 'order')
            .map(({ ...rest }) => rest)
        })
      ));
      dispatch({
        type: TEMPLATES_GET_SKIPPED_WORDS_OPTIONS,
        payload
      });
    });
};

export const getExerciseStatistics = (query) => (dispatch, getState) => {
  if (!query.variables.createdBy) {
    console.warn('patient or user id is missing');
    return;
  }
  if (!query.variables.exercise) {
    console.warn('exercise id is missing');
    return;
  }
  const locale = getState().User.currentUser.locale;
  const token = getState().User.currentUser.token;
  return axios.post(GRAPHQL_URL_RESULTS, query, apiConfig(token))
    .then(({ data: { data: { aggregateResultsByDate } } }) => {
      if (!aggregateResultsByDate.length) return null;
      const correctSum = aggregateResultsByDate.reduce((a, c) => a + c.correctCount, 0);
      const incorrectSum = aggregateResultsByDate.reduce((a, c) => a + (c.count - c.correctCount), 0);
      const maxTimeTotal = Math.max.apply(this, aggregateResultsByDate.map(item => item.timeTotal));
      console.log(aggregateResultsByDate);
      return (!correctSum || !incorrectSum)
        ? ([
          {
            type: 'column',
            name: locale === 'et' ? 'läbitud harjutusi' : 'exercises completed',
            color: '#FF87B0',
            className: 'columnChart',
            data: sortBy(aggregateResultsByDate.map(({ created, timeTotal }) => ({
              dataLabels: {
                enabled: true,
                inside: false,
                style: {
                  fontWeight: 'bold',
                  fontSize: '14px',
                  color: 'black'
                },
                formatter() {
                  return formatDate(timeTotal, locale);
                },
                useHTML: true
              },
              x: Date.parse(created),
              y: Math.ceil(timeTotal / 60)
            })
            ), 'x')
          }
        ])
        : ([
          {
            type: 'column',
            name: ' ',
            color: 'transparent',
            data: sortBy(aggregateResultsByDate.map(({ timeTotal, correctCount, count, created }) => ({
              dataLabels: {
                enabled: true,
                inside: false,
                style: {
                  fontWeight: 'bold',
                  fontSize: '14px',
                  color: 'black'
                },
                formatter() {
                  return formatDate(timeTotal, locale);
                },
                useHTML: false
              },
              x: Date.parse(created),
              y: 0,
              correct: correctCount,
              count
            })
            ), 'x')
          },
          {
            type: 'column',
            name: locale === 'et' ? 'valesid vastuseid' : 'wrong answers',
            color: '#65CAEF',
            className: 'columnChart',
            data: sortBy(aggregateResultsByDate.map(({ correctCount, count, created, timeTotal }) => ({
              dataLabels: {
                enabled: true,
                inside: true,
                visible: false,
                style: {
                  fontWeight: 'bold',
                  fontSize: '14px',
                  stroke: 'none',
                  textShadow: '0 0 5px rgba(0, 0, 0, .5)',
                  color: (count - correctCount) ? 'white' : 'transparent',
                },
                formatter() {
                  return `<span>${count - correctCount}</span>`;
                },
                useHTML: true
              },
              x: Date.parse(created),
              y: getCorrectTime(timeTotal, maxTimeTotal, count - correctCount, count)
            })
            ), 'x')
          },
          {
            type: 'column',
            name: locale === 'et' ? 'õigeid vastuseid' : 'correct answers',
            color: '#21B14D',
            className: 'columnChart',
            data: sortBy(aggregateResultsByDate.map(({ correctCount, count, created, timeTotal }) => ({
              dataLabels: {
                enabled: true,
                inside: true,
                style: {
                  fontWeight: 'bold',
                  fontSize: '14px',
                  stroke: 'none',
                  color: correctCount ? 'white' : 'transparent',
                },
                formatter() {
                  return `<span>${this.point.correct}</span>`;
                },
                useHTML: true
              },
              x: Date.parse(created),
              y: getCorrectTime(timeTotal, maxTimeTotal, correctCount, count),
              correct: correctCount,
              count
            })
            ), 'x')
          }
        ]);
    })
    .catch(err => {
      console.error(err);
    });
};

export const getTotalExerciseTime = (query) => (dispatch, getState) => {
  if (!query.variables.createdBy) {
    console.warn('patient or user id is missing');
    return;
  }
  if (!query.variables.exercise) {
    console.warn('exercise id is missing');
    return;
  }
  const token = getState().User.currentUser.token;
  return axios.post(GRAPHQL_URL_RESULTS, query, apiConfig(token))
    .then(result => result.data.data.countResults.timeTotal)
    .catch(err => {
      console.error(err);
    });
};

export const sendReport = (queryObj) => (dispatch, getState) => {
  const token = getState().User.currentUser.token;
  axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
    .then(result => {
      console.log('result data', result.data);
    })
    .catch(err => {
      console.error(err);
    });
};

export const sendResults = (queryObj) => (dispatch, getState) => {
  const token = getState().User.currentUser.token;
  axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
    .then(result => {
      console.log('result data', result.data);
    })
    .catch(err => {
      console.error(err);
    });
};

export const resetCurrentExercise = () => ({
  type: RESET_CURRENT_EXERCISE,
});

export const resetLastExerciseData = () => ({
  type: RESET_LAST_EXERCISE,
});

export const resetExercises = () => ({
  type: RESET_EXERCISES,
});

export const showOverlay = (status = true) => ({
  type: SHOW_OVERLAY,
  payload: status
});

export const updateCurrentSettings = (payload) => ({
  payload,
  type: UPDATE_SETTINGS
});

export const updateTests = (payload) => ({
  payload,
  type: UPDATE_TESTS
});

export const uploadBlob = (blob, fileName = 'testName') => {
  if (!blob) return;
  axios.post(AUDIOS_URL, {
    filename: fileName,
    filetype: blob.type
  }).then(result => {
    if (result.request.readyState === 4 && result.status === 200) {
      upload(blob, result.data.signed);
    }
  });

  return {
    type: ''
  };
};

const upload = (blob, signRequest) => {
  const xhr = new XMLHttpRequest();
  xhr.open('PUT', signRequest);
  xhr.setRequestHeader('x-amz-acl', 'public-read');
  xhr.onload = function () {
    if (xhr.status === 200) {
      console.log('file successfully uploaded');
    }
  };
  xhr.send(blob);
};

// Save state of current exercise

export const resetCurrentExerciseState = () => ({
  type: RESET_CURRENT_EXERCISE_STATE,
});

export const addExerciseThreeState = (testNumber, state) => ({
  type: ADD_EXERCISE_THREE_RESULTS,
  testNumber,
  state,
});

export const addExerciseFourState = (testNumber, state) => ({
  type: ADD_EXERCISE_FOUR_RESULTS,
  testNumber,
  state,
});

export const addExerciseFiveState = (testNumber, answers, questions, correct) => ({
  type: ADD_EXERCISE_FIVE_RESULTS,
  testNumber,
  answers,
  questions,
  correct
});

export const addExerciseSevenState = (testNumber, answer) => ({
  type: ADD_EXERCISE_SEVEN_RESULTS,
  testNumber,
  answer,
});

export const addExerciseSevenAState = (testNumber, answer) => ({
  type: ADD_EXERCISE_SEVEN_A_RESULTS,
  testNumber,
  answer,
});

export const addExerciseSixState = (testNumber, state) => ({
  type: ADD_EXERCISE_SIX_RESULTS,
  testNumber,
  state,
});

export const addExerciseNineState = (testNumber, state) => ({
  type: ADD_EXERCISE_NINE_RESULTS,
  testNumber,
  state,
});
