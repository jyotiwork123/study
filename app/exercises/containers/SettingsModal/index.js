import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import { sendReport } from '../../state/actions';

//components
import PureSelect from '../../../common/components/PureSelect';
import PromptPopup from '../../../common/components/PromptPopup';
import Modal from 'react-modal';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//utils, data
import { getMutation } from '../../../common/queries';

//styles
import styles from './settingsModal.css';


class SettingsModal extends Component {
  static propTypes = {
    settings: PropTypes.array.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    toggleSettingsModal: PropTypes.func.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      settings: null,
      showPrompt: false,
      promptType: ''
    };

    this.modifySettings = this.modifySettings.bind(this);
    this.saveSettings = this.saveSettings.bind(this);
    this.cancelSettings = this.cancelSettings.bind(this);
    this.confirmReport = this.confirmReport.bind(this);
    this.toggleShowPrompt = this.toggleShowPrompt.bind(this);
  }
  componentWillMount() {
    this.setState({ settings: this.props.settings });
  }
  componentDidMount() {
  window.componentHandler.upgradeDom();
}
  modifySettings(index, setting) {
    const newSettings = Object.assign([], this.state.settings);
    newSettings[index] = setting;
    this.setState({ settings: newSettings });
  }
  saveSettings() {
    const { updateCurrentSettings, toggleSettingsModal } = this.props;
    updateCurrentSettings(this.state.settings);
    toggleSettingsModal();
  }
  cancelSettings() {
    const { onRequestClose, settings } = this.props;
    this.setState({ settings });
    onRequestClose();
  }
  toggleShowPrompt(promptType = '') {
    this.setState({
      promptType,
      showPrompt: !this.state.showPrompt
    });
  }
  confirmReport(promptType) {
    const { userId, exerciseId, sendReport } = this.props;
    const payload = {
      query: getMutation('addExerciseFeedback'),
      variables: {
        input: {
          exercise: exerciseId,
          content: `${promptType} does not work`,
          createdBy: userId
        }
      }
    };
    sendReport(payload);
    this.toggleShowPrompt();
  }
  render() {
    const { isOpen } = this.props;
    const { settings, promptType, showPrompt } = this.state;
    const { formatMessage } = this.props.intl;

    return (
      <Modal
        className="settings-modal"
        overlayClassName="settings-modal-overlay"
        isOpen={isOpen}
        onRequestClose={this.cancelSettings}
        shouldCloseOnOverlayClick
        contentLabel="Settings window"
      >
        <div className={styles.header}>
          <div className={styles.title}>{formatMessage(globalMessages.settings)}</div>
          <div>
            <button
              className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.accentButton}`}
              onClick={() => {
                this.toggleShowPrompt('Audio');
              }}
            >
              {formatMessage(globalMessages.reportAudio)}
            </button>
            <button
              className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.accentButton}`}
              onClick={() => {
                this.toggleShowPrompt('Image');
              }}
            >
              {formatMessage(globalMessages.reportImage)}
            </button>
            <button
              className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.accentButton}`}
              onClick={() => {
                this.toggleShowPrompt('Text');
              }}
            >
              {formatMessage(globalMessages.reportText)}
            </button>
            <button
              className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.coloredButton}`}
              onClick={this.saveSettings}
            >
              {formatMessage(globalMessages.save)}
            </button>
          </div>
        </div>
        <div className={styles.content}>
          {settings.map((setting, index) =>
            <PureSelect
              key={index}
              index={index}
              label={setting.label}
              options={setting.options}
              value={setting.value}
              changeSetting={this.modifySettings}
            />
          )}
        </div>
        {showPrompt &&
          <PromptPopup
            modalClassName="report"
            overlayClassName="report-modal-overlay"
            isOpen={showPrompt}
            promptType={promptType}
            onRequestClose={this.toggleShowPrompt}
            onConfirm={this.confirmReport}
            contentLabel="report"
          >
            {`Would you like to report that ${promptType.toLowerCase()} doesn't work?`}
          </PromptPopup>
        }
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    userId: state.User.currentUser.id,
    exerciseId: state.Exercises.currentExercise._id
  };
}

export default connect(mapStateToProps, {
  sendReport
})(injectIntl(SettingsModal));
