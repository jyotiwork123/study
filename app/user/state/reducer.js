import { decodeToken } from '../helpers';
import {
  USER_SET_CURRENT_USER,
  USER_GET_TOKEN_EXPIRATION_DATA,
  USER_REFRESH_USER,
  USER_LOGOUT,
  USER_SHOW_PROMPT_TOKEN_REFRESH,
  USER_ADD_ADDITIONAL_TUTOR_DATA,
  USER_GET_GROUPS,
  USER_GET_TAGS,
  USER_GET_AUDIOS,
  USER_GET_IMAGES,
  USER_GET_VIDEOS
} from './constants';

export const INITIAL_STATE = {
  currentUser: {},
  currentUserTutorData: {},
  expiration: undefined,
  authenticating: true,
  isAuthenticated: false,
  showPromptTokenRefresh: false,
  groups: [],
  tags: [],
  savedAudios: [],
  savedVideos: [],
  savedImages: []
};

export default function (state = INITIAL_STATE, action) {
  let currentUser;
  switch (action.type) {
    case USER_SET_CURRENT_USER:
      mixpanel.identify(action.payload.id);
      currentUser = action.payload;
      if (currentUser.locale === 'ee') {
        currentUser.locale = 'et';
      }
      return {
        ...state,
        ...state.currentUser,
        currentUser
      };
    case USER_GET_TOKEN_EXPIRATION_DATA:
      return {
        ...state,
        authenticating: false,
        expiration: action.payload.ttl,
        isAuthenticated: action.payload.result
      };
    case USER_SHOW_PROMPT_TOKEN_REFRESH:
      return {
        ...state,
        showPromptTokenRefresh: action.payload
      };
    case USER_REFRESH_USER:
      currentUser = decodeToken(action.payload);
      if (currentUser.locale === 'ee') {
        currentUser.locale = 'et';
      }
      mixpanel.identify(currentUser.id);
      return {
        ...state,
        currentUser
      };
    case USER_LOGOUT:
      return INITIAL_STATE;

    case USER_ADD_ADDITIONAL_TUTOR_DATA:
      return {
        ...state,
        currentUserTutorData: action.payload
      };

    case USER_GET_GROUPS:
      return {
        ...state,
        groups: action.payload
      };
    case USER_GET_TAGS:
      return {
        ...state,
        tags: action.payload
      };
    case USER_GET_AUDIOS:
      return {
        ...state,
        savedAudios: action.payload
      };
    case USER_GET_IMAGES:
      return {
        ...state,
        savedImages: action.payload
      };
    case USER_GET_VIDEOS:
      return {
        ...state,
        savedVideos: action.payload
      };
    default:
      return state;
  }
}
