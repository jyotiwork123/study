import axios from 'axios';
import { redirectToLogin } from '../helpers';
import {
  EXPIRATION_URL,
  REFRESH_URL,
  LOGIN_URL,
  GROUPS_URL,
  HOBBIES_URL,
  GRAPHQL_URL,
  USER_URL,
  apiConfig
} from '../../common/api';

//constants
import {
  USER_SET_CURRENT_USER,
  USER_GET_TOKEN_EXPIRATION_DATA,
  USER_REFRESH_USER,
  USER_LOGOUT,
  USER_SHOW_PROMPT_TOKEN_REFRESH,
  USER_PROMPT_TIME,
  USER_GET_GROUPS,
  USER_GET_TAGS,
  USER_ADD_ADDITIONAL_TUTOR_DATA,
  USER_GET_AUDIOS,
  USER_GET_IMAGES
} from './constants';

export function setCurrentUser(object) {
  return {
    payload: object,
    type: USER_SET_CURRENT_USER
  };
}

let showPromptTimeout;
let logoutTimeout;
const setPromptTokenRefreshTimer = (time, dispatch) => {
  if (showPromptTimeout) {
    clearTimeout(showPromptTimeout);
  }
  if (logoutTimeout) {
    clearTimeout(logoutTimeout);
  }
  showPromptTimeout = setTimeout(() => {
    dispatch(showPromptTokenRefresh(true));
  }, (time - USER_PROMPT_TIME) * 1000);
  logoutTimeout = setTimeout(() => {
    logout();
  }, time * 1000);
};

export function getTokenExpirationData() {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.post(EXPIRATION_URL, { token })
      .then(response => {
        const { ttl, result } = response.data;
        dispatch({
          type: USER_GET_TOKEN_EXPIRATION_DATA,
          payload: {
            result,
            ttl
          }
        });
        setPromptTokenRefreshTimer(ttl, dispatch);
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function showPromptTokenRefresh(bool) {
  return {
    payload: bool,
    type: USER_SHOW_PROMPT_TOKEN_REFRESH
  };
}

export function refreshUser() {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.post(REFRESH_URL, { token })
      .then(result => {
        console.log('result 1', result);
        dispatch({
          payload: result.data.token,
          type: USER_REFRESH_USER
        });
        dispatch(showPromptTokenRefresh(false));
      })
      .then(() => {
        getTokenExpirationData()(dispatch, getState);
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function logout() {
  return (dispatch, getState) => {
    const userRole = getState().User.currentUser.role;
    redirectToLogin(LOGIN_URL, userRole);
    dispatch({
      type: USER_LOGOUT
    });    
  }  
}

export function getGroups() {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.get(GROUPS_URL, apiConfig(token))
      .then(result => {
        // console.log('result groups', result.data);
        // dispatch({
        //   payload: result.data,
        //   type: USER_SET_CURRENT_USER
        // });
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function getCurrentTutorInfo(userId) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.get(`${USER_URL}/${userId}`, apiConfig(token))
      .then(result => {
        // console.log('result user info', result);  
        dispatch({
          payload: result.data,
          type: USER_ADD_ADDITIONAL_TUTOR_DATA
        });
      })
      .catch(err => {
        console.error(err);
      });
  };
}
// export function getHobbies(locale) {
//   return (dispatch, getState) => {
//     const token = getState().User.currentUser.token;
//     axios.get(`${HOBBIES_URL}?locale=${locale}`, apiConfig(token))
//       .then(result => {
//         console.log('result groups', result);
//         dispatch({
//           payload: result.data || [],
//           type: PATIENTS_GET_HOBBIES
//         });
//       })
//       .catch(err => {
//         console.error(err);
//       });
//   };
// }

export const getTags = (queryObj) => (dispatch, getState) => {
  const { token } = getState().User.currentUser;
  console.log('tags token', token);

  axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
    .then(result => {
      console.log('result tags', result);
      // dispatch({
      //   type: USER_GET_TAGS,
      //   payload: result.data.tags || [],
      // });
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
};

export const getAudios = (queryObj) => (dispatch, getState) => {
  const { token } = getState().User.currentUser;
  return axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
    .then(result => {
      console.log('result audios', result);
      dispatch({
        type: USER_GET_AUDIOS,
        payload: result.data.data.audios || [],
      });
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
};

export const getImages = (queryObj) => (dispatch, getState) => {
  const { token } = getState().User.currentUser;
  return axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
    .then(result => {
      console.log('result images', result);
      dispatch({
        type: USER_GET_IMAGES,
        payload: result.data.data.images || [],
      });
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
};

export function getImageUploadUrl(queryObj) {
  return async (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    try {
      const result = await axios.post(GRAPHQL_URL, queryObj, apiConfig(token));
      return result.data.data.createUploadSignedLink;
    } catch (err) {
      console.error(err);
    }
  };
}

export function uploadFile(url, file) {
  // const formData = new FormData();
  // formData.append('Content-Type', file.type);  
  // formData.append('file', file);
  return async () => {
    try {
      const result = await axios.put(url, file, {
        // headers: {
        //   'Content-Type': file.type
        // }
    });
      return result;
    } catch (err) {
      console.error(err);
    }
  };
}

export const saveFileToDB = (queryObj) => (dispatch, getState) => {
  const { token } = getState().User.currentUser;
  return axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
    .then(result => {
      console.log('result saving image', result);
      // dispatch({
      //   type: USER_GET_IMAGES,
      //   payload: result.data.data.images || [],
      // });
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
};


// export const getImageUploadUrl = (queryObj) => (dispatch, getState) => {
//   const { token } = getState().User.currentUser;
//   // console.log('getImageUploadUrl token', token);
//   console.log('queryObj', queryObj);

//   axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
//     .then(result => {
//       console.log('result imageLink', result);      
//     }, error => {
//       throw new Error(`rejected ${error.message}`);
//     });
// };

export const saveAudio = (queryObj, data) => (dispatch, getState) => {
  const formdata = new FormData();

  console.log('tags', data.tags);


  formdata.append('operations', JSON.stringify(queryObj));
  formdata.append('map', JSON.stringify({ 0: ['variables.file'] }));
  // formdata.append('file', data.file);


  const token = getState().User.currentUser.token;
  axios.post(GRAPHQL_URL, formdata, apiConfig(token))
    .then(result => {
      console.log('saveAudio result data', result.data);
    })
    .catch(err => {
      console.error(err);
    });
};

export function editProfile(data) {
  return (dispatch, getState) => {
    console.log('editProfile', data);

    const token = getState().User.currentUser.token;
    const { currentUserId, position, firstName, lastName, email, phone } = data;
    const payload = {
      // action: '',
      position,
      firstname: firstName,
      lastname: lastName,
      email,
      phone
    };
    axios.put(`${USER_URL}/${currentUserId}/info`, payload, apiConfig(token))
      .then(result => {
        console.log('result user edit', result);
      })
      .catch(err => {
        console.error(err);
      });
  };
}
