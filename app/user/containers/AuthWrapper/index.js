import React, { PureComponent } from 'react';
//redux
import { connect } from 'react-redux';
import { logout } from '../../state/actions';
//components
import Spinner from '../../../common/components/spinner';
//styles
import styles from './authWrapper.css';
//logRocket
import LogRocket from 'logrocket';


export function AuthWrapper(WrappedComponent) {
  class AuthComponent extends PureComponent {
    componentWillMount() {
      LogRocket.identify(this.props.userId);
    }
    render() {
      const { isAuthenticated, authenticating, logout } = this.props;

      if (!authenticating && !isAuthenticated) {
        logout();
      }

      if (authenticating && !isAuthenticated) {
        return <Spinner spinnerClass={styles.spinner} />;
      }

      return (
        <WrappedComponent {...this.props} />
      );
    }
  }

  function mapStateToProps(state) {
    return {
      authenticating: state.User.authenticating,
      isAuthenticated: state.User.isAuthenticated,
      userId: state.User.currentUser.id,
      currentPatient: state.Patient.currentPatient
      // currentPatient: state.Patient.patients.filter(patient => patient._id === state.Patient.patientId)[0]
    };
  }

  return connect(mapStateToProps, { logout })(AuthComponent);
}
