import { combineReducers } from 'redux';
import ExercisesReducer from './exercises/state/reducer';
import UserReducer from './user/state/reducer';
import PatientReducer from './patient/state/reducer';


const rootReducer = combineReducers({
  Exercises: ExercisesReducer,
  User: UserReducer,
  Patient: PatientReducer
});

export default rootReducer;
