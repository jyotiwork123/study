export const PATIENT_SET_PATIENTS = 'PATIENT_SET_PATIENTS';
export const PATIENT_SET_PATIENT = 'PATIENT_SET_PATIENT';
export const PATIENT_SET_SUGGESTED = 'PATIENT_SET_SUGGESTED';
export const PATIENTS_GET_HOBBIES = 'PATIENTS_GET_HOBBIES';
