import {
  PATIENT_SET_PATIENTS,
  PATIENT_SET_PATIENT,
  PATIENT_SET_SUGGESTED,
  PATIENTS_GET_HOBBIES,

} from './constants';

const INITIAL_STATE = {
  patients: [],
  patientId: '',
  patientsReceived: false,
  suggestedExercises: [],
  hobbiesList: [],
  currentPatient: {}
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case PATIENT_SET_PATIENTS:
      return {
        ...state,
        patients: action.payload,
        patientsReceived: true
      };
    case PATIENT_SET_PATIENT:
      return {
        ...state,
        patients: [],
        patientsReceived: false,
        patientId: action.payload
      };
    case PATIENT_SET_SUGGESTED:
      return {
        ...state,
        currentPatient: action.payload,
        suggestedExercises: action.payload.suggestions
      };
    case PATIENTS_GET_HOBBIES:
      return {
        ...state,
        hobbiesList: action.payload
      };
    default:
      return state;
  }
}
