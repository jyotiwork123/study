import axios from 'axios';
import { PATIENTS_URL, HOBBIES_URL, apiConfig } from '../../common/api';
import {
  PATIENT_SET_PATIENTS,
  PATIENT_SET_PATIENT,
  PATIENT_SET_SUGGESTED,
  PATIENTS_GET_HOBBIES
} from './constants';


export function getPatients() {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.get(PATIENTS_URL, apiConfig(token))
      .then(result => {
        const ids = result.data.map(({ _id }) => _id);
        return axios.post(`${PATIENTS_URL}/batch`, ids, apiConfig(token));
      })
      .then(result => {
        dispatch({
          type: PATIENT_SET_PATIENTS,
          payload: result.data.items
        });
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function createPatient(firstName, lastName, email) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    if (!firstName || !lastName) {
      console.log('enter first and last name');
      return;
    }
    if (!email) {
      console.log('enter email');
      return;
    }
    const payload = {
      firstname: firstName,
      lastname: lastName,
      email
    };
    axios.post(PATIENTS_URL, payload, apiConfig(token))
      .then(result => {
        dispatch(setPatient(result.data._id));
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function removePatient(id) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.delete(`${PATIENTS_URL}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      },
      // data: id
    }).then(result => {
      console.log(result);
      dispatch(getPatients());
    })
      .catch(err => {
        console.error(err);
      });
  };
}

export function editPatient(data) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    const { patientId, birthYear, gender, hobbiesArray } = data;
    // console.log(patientId, birthYear, gender, hobbiesArray);

    const payload = {
      action: 'update-basicinfo',
      birthyear: Number(birthYear),
      gender,
      hobbies: hobbiesArray
    };

    // console.log('payload', payload);
    axios.put(`${PATIENTS_URL}/${patientId}`, payload, apiConfig(token))
      .then(result => {
        // console.log(result.data.patient);
        // getPatients();
        dispatch(getPatients()); // add saving the data to local if needed
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function setPatient(payload) {
  return {
    type: PATIENT_SET_PATIENT,
    payload
  };
}

export function getSuggestedExercises(id) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    return axios.get(`${PATIENTS_URL}/${id}`, apiConfig(token))
      .then(result => {
        // console.log('suggested exercises', result.data)
        dispatch({
          type: PATIENT_SET_SUGGESTED,
          payload: result.data
        });
        return result.data;
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function getHobbies(locale) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.get(`${HOBBIES_URL}?locale=${locale}`, apiConfig(token))
      .then(result => {
        // console.log('result hobbies', result);
        dispatch({
          payload: result.data || [],
          type: PATIENTS_GET_HOBBIES
        });
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function saveHobby(title, locale) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    const payload = { title, locale };
    axios.put(`${HOBBIES_URL}`, payload, apiConfig(token))
      .then(result => {
        console.log('result saveHobby', result);        
      })
      .catch(err => {
        console.error(err);
      });
  };
}
