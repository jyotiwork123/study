import React, { Component } from 'react';

//redux
import { connect } from 'react-redux';
import { getPatients, setPatient, removePatient, createPatient, getSuggestedExercises } from '../../state/actions';
import { modifyContent } from '../../../exercises/state/actions';

//components
import SelectPatient from '../../components/SelectPatient/index';
import NewPatient from '../../components/NewPatient/index';
import Spinner from '../../../common/components/spinner';

//styles
import styles from './patientView.css';


class PatientsView extends Component {


  componentWillMount() {
    // this.props.getPatients();
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  render() {
    const { loaded, patients, setPatient, removePatient, createPatient, push, modifyContent } = this.props;

    if (!loaded) {
      return (
        <div className={`mdl-shadow--8dp ${styles.patientsBlock}`}>
          <Spinner />
        </div>
      );
    }

    return patients.length ?
      <div className={`mdl-shadow--8dp ${styles.patientsBlock}`}>
        <div className={styles.leftSection}>
          <NewPatient createPatient={createPatient} push={push} modifyContent={modifyContent} />
        </div>
        <div className={styles.rightSection}>
          <SelectPatient
            key={patients.length}
            patients={patients}
            setPatient={setPatient}
            removePatient={removePatient}
            push={push}
            modifyContent={modifyContent}
          />
        </div>
       
      </div>
      :
      <div className={styles.createPatient}>
        <NewPatient createPatient={createPatient} push={push} modifyContent={modifyContent} solo />
      </div>;
  }
}

function mapStateToProps(state) {
  return {
    // patients: state.Patient.patients,
    loaded: state.Patient.patientsReceived
  };
}

export default connect(mapStateToProps, {
  getPatients,
  setPatient,
  removePatient,
  createPatient,
  modifyContent
})(PatientsView);
