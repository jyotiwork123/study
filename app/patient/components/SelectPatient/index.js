import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

//components
import UserDataDialog from '../../../common/components/UserDataDialog';

//redux
import { connect } from 'react-redux';
import { editPatient } from '../../../patient/state/actions';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//utils, data
import debounce from 'lodash/debounce';
import sortBy from 'lodash/sortBy';
// import { Scrollbars } from 'react-custom-scrollbars';
import { patientExpiredCheck } from '../../../exercises/helpers';

//styles
import styles from './selectPatient.css';


class SelectPatient extends Component {
  static propTypes = {
    setPatient: PropTypes.func.isRequired,
    removePatient: PropTypes.func.isRequired,
    patients: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      patients: [],
      showUserDataDialog: false,
      showConfirmationDialog: false,
      currentId: null
    };

    this.handleSelection = this.handleSelection.bind(this);
    this.saveUserChanges = this.saveUserChanges.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.renderPatients = this.renderPatients.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  componentWillMount() {
    const { patients } = this.props;
    const sortedPatients = sortBy(patients, 'firstname');
    this.setState({ patients: sortedPatients });
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  handleSelection(id) {
    const { push, setPatient, modifyContent } = this.props;
    setPatient(id);
    modifyContent('all exercises');
    // modifyContent('suggestions');
    push('/practice');
  }
  handleEdit(event, id) {
    event.stopPropagation();
    this.setState({ showUserDataDialog: true, currentId: id });
    // console.log(id);
  }

  toggleConfirmModal() {

  }
  
  saveUserChanges(event, data) {
    this.props.editPatient(data);
    this.setState({ showUserDataDialog: false });
  }

  handleDelete(event, id) {
    console.log(id);

    event.stopPropagation();

    const { removePatient } = this.props;
    // const { currentId } = this.state;
    // console.log(currentId);
    removePatient(id);
  }

  handleSearchChange(value) {
    const { patients } = this.props;
    const filteredPatients = patients.filter(({ firstname, lastname }) => (
      `${firstname.toLowerCase()}${lastname.toLowerCase()}`.search(value) > -1)
    );
    const sortedPatients = sortBy(filteredPatients, 'firstname');
    this.setState({ patients: sortedPatients });
  }

  renderPatients({ lastname, firstname, key, email, lastLogin, expires, _id }) {
    const { formatMessage } = this.props.intl;

    return (
      <tr
        key={_id}
        className={styles.row}
        onClick={() => {
          this.handleSelection(_id);
        }}
      >
        <td className={styles.cell}>
          <div className={styles.iconsWrapper}>
            <i
              className={`material-icons ${styles.icon}`}
              style={{ color: 'red' }}
              onClick={(event) => {
                this.handleDelete(event, _id);
              }}
            >delete_outline</i>
            <i
              className={`material-icons ${styles.icon}`}
              style={{ color: 'lightblue' }}
              onClick={(event) => {
                event.stopPropagation();
                this.handleEdit(event, _id);
              }}
            >edit</i>
          </div>
        </td>
        <td className={styles.cell}>{`${firstname || ''} ${lastname || ''}`}</td>
        <td className={styles.cell}>{email}</td>
        <td className={`${styles.cell} ${styles.centered}`}>{key}</td>
        <td className={`${styles.cell} ${styles.centered}`}><span>{`${moment(lastLogin).format('DD.MM.YYYY')}  ${moment(lastLogin).format('hh:mm')}`}</span>
        </td>
        <td className={`${styles.cell} ${styles.centered}`}>
          {patientExpiredCheck(expires)
            ? <span style={{ color: 'red' }}>{formatMessage(globalMessages.expired)}</span>
            : moment(expires).format('DD.MM.YYYY')
          }
        </td>
      </tr>
    );
  }

  render() {
    const { patients, showUserDataDialog, showConfirmationDialog, currentId } = this.state;
    const { locale, patientId } = this.props;
    const user = patients.filter(patientsEntry => patientsEntry._id === currentId);
    const { formatMessage } = this.props.intl;
    const debounced = debounce((value) => { this.handleSearchChange(value); }, 500);

    return (
      <div className={styles.container}>
        <div className={styles.header}>
          <span>{formatMessage(globalMessages.myPatients)}</span>
          {/* <div className={`mdl-textfield mdl-js-textfield ${styles.searchInputWrapper}`}>
            <i className={`material-icons ${styles.searchIcon}`}>search</i>
            <input
              className={`mdl-textfield__input ${styles.searchInput}`}
              type="text"
              id="image-picker-input"
              onChange={(e) => debounced(e.target.value)}
            />
          </div> */}
        </div>
        <div className={styles.content}>
          {/* <Scrollbars> */}
          <table style={{ width: '100%', marginTop: 20 }} cellSpacing={0}>
            <thead>
              <tr>
                <td className={styles.cell} />
                <td className={styles.cell}>{formatMessage(globalMessages.fullName)}</td>
                <td className={styles.cell}>{formatMessage(globalMessages.enterEmail)}</td>
                <td className={styles.cell}>{formatMessage(globalMessages.accessCode)}</td>
                <td className={styles.cell}>{formatMessage(globalMessages.lastLogin)}</td>
                <th className={styles.cell}>{formatMessage(globalMessages.accountActivity)}</th>
              </tr>
            </thead>
            <tbody className={styles.tableBody}>
              {patients.map(this.renderPatients)}
            </tbody>
          </table>
          {/* </Scrollbars> */}
        </div>
        {
          showUserDataDialog && <UserDataDialog
            overlayClassName={styles.landingModal}
            isOpen={this.state.showUserDataDialog}
            user={user[0]}
            onUpload={(event, data, locale) => this.saveUserChanges(event, data)}
            onCancel={() => this.setState({ showUserDataDialog: false })}
          />
        }
        {/* {
          showConfirmationDialog && <ConfirmDialog
            overlayClassName={styles.landingModal}
            isOpen={this.state.showUserDataDialog}
            onUpload={(event) => this.handleDelete(event)}
            onCancel={() => this.setState({ showConfirmationDialog: false })}
          />
        } */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    patients: state.Patient.patients,
    patientId: state.Patient.patientId,
    locale: state.User.currentUser.locale,

  };
}

export default connect(mapStateToProps, { editPatient })(injectIntl(SelectPatient));
