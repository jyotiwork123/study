import { defineMessages } from 'react-intl';

export const globalMessages = defineMessages({
  exercises: {
    id: 'Global_Exercises',
    defaultMessage: 'Exercises'
  },
  templates: {
    id: 'Global_Templates',
    defaultMessage: 'Templates'
  },
  home: {
    id: 'Global_Home',
    defaultMessage: 'Home'
  },
  preview: {
    id: 'Global_Preview',
    defaultMessage: 'preview'
  },
  loadMore: {
    id: 'Global_LoadMore',
    defaultMessage: 'Load More'
  },
  audios: {
    id: 'Global_Audios',
    defaultMessage: 'audios'
  },
  images: {
    id: 'Global_Images',
    defaultMessage: 'images'
  },
  videos: {
    id: 'Global_Videos',
    defaultMessage: 'videos'
  },
  audioManager: {
    id: 'Global_AudioManager',
    defaultMessage: 'Audio Manager'
  },
  imageManager: {
    id: 'Global_ImageManager',
    defaultMessage: 'Image Manager'
  },
  videoManager: {
    id: 'Global_VideoManager',
    defaultMessage: 'Video Manager'
  },
  editUser: {
    id: 'Global_EditUserData',
    defaultMessage: 'Edit User Data:'
  },
  editTutor: {
    id: 'Global_EditTutorData',
    defaultMessage: 'Edit Tutor Data:'
  },
  searchBy: {
    id: 'Global_SearchBy',
    defaultMessage: 'Search By:'
  },
  play: {
    id: 'Global_Play',
    defaultMessage: 'Play'
  },
  show: {
    id: 'Global_Show',
    defaultMessage: 'Show'
  },
  size: {
    id: 'Global_Size',
    defaultMessage: 'Size'
  },
  duration: {
    id: 'Global_Duration',
    defaultMessage: 'Duration'
  },
  actions: {
    id: 'Global_Actions',
    defaultMessage: 'Actions'
  },
  title: {
    id: 'Global_Title',
    defaultMessage: 'Title'
  },
  group: {
    id: 'Global_Group',
    defaultMessage: 'Group'
  },
  language: {
    id: 'Global_Language',
    defaultMessage: 'Language'
  },
  tags: {
    id: 'Global_Tags',
    defaultMessage: 'Tags'
  },
  position: {
    id: 'Global_Position',
    defaultMessage: 'Position'
  },
  institution: {
    id: 'Global_Institution',
    defaultMessage: 'Institution'
  },
  tutor: {
    id: 'Global_Tutor',
    defaultMessage: 'Tutor'
  },
  teacher: {
    id: 'Global_Teacher',
    defaultMessage: 'Teacher'
  },
  speechTherapist: {
    id: 'Global_SpeechTherapist',
    defaultMessage: 'Speech therapist'
  },
  specialTeacher: {
    id: 'Global_SpecialTeacher',
    defaultMessage: 'Special education teacher'
  },
  parent: {
    id: 'Global_Parent',
    defaultMessage: 'Parent'
  },
  other: {
    id: 'Global_Other',
    defaultMessage: 'Other'
  },
  upload: {
    id: 'Global_Upload',
    defaultMessage: 'Upload'
  },
  cancel: {
    id: 'Global_Cancel',
    defaultMessage: 'Cancel'
  },
  delete: {
    id: 'Global_Delete',
    defaultMessage: 'Delete'
  },
  uploadAudio: {
    id: 'Global_UploadAudio',
    defaultMessage: 'Upload Audio'
  },
  uploadImage: {
    id: 'Global_UploadImage',
    defaultMessage: 'Upload Image'
  },
  uploadVideo: {
    id: 'Global_UploadVideo',
    defaultMessage: 'Upload Video'
  },
  wrongFileType: {
    id: 'Global_WrongFileType',
    defaultMessage: 'Wrong file type.'
  },
  clickOrDropFile: {
    id: 'Global_ClickOrDropFile',
    defaultMessage: 'Click or drag and drop a file.'
  },
  browserNotSupportFile: {
    id: 'Global_BrowserNotSupportFile',
    defaultMessage: 'Your browser does not support the file.'
  },
  pleaseSelectFile: {
    id: 'Global_PleaseSelectFile',
    defaultMessage: 'Please select a file.'
  },
  pleaseEnterTitleForFile: {
    id: 'Global_PleaseEnterTitleForFile',
    defaultMessage: 'Please enter a title for the file.'
  },
  pleaseEnterGroupForFile: {
    id: 'Global_PleaseEnterGroupForFile',
    defaultMessage: 'Please select a group name for the file.'
  },
  pleaseEnterLanguageForFile: {
    id: 'Global_PleaseEnterLanguageForFile',
    defaultMessage: 'Please select a language for the file.'
  },
  pleaseEnterTagsForFile: {
    id: 'Global_PleaseEnterTagsForFile',
    defaultMessage: 'Please enter tags for the file.'
  },

  results: {
    id: 'Global_Results',
    defaultMessage: 'results'
  },
  logout: {
    id: 'Global_Logout',
    defaultMessage: 'logout'
  },
  minutes: {
    id: 'Global_Minutes',
    defaultMessage: 'minutes'
  },
  seconds: {
    id: 'Global_Seconds',
    defaultMessage: 'seconds'
  },
  goodJob: {
    id: 'Global_GoodJob',
    defaultMessage: 'Good job'
  },
  redirecting: {
    id: 'Global_Redirecting',
    defaultMessage: 'Redirecting...'
  },
  loading: {
    id: 'Global_Loading',
    defaultMessage: 'Loading...'
  },
  settings: {
    id: 'Global_Settings',
    defaultMessage: 'settings'
  },
  reportAudio: {
    id: 'Global_ReportAudio',
    defaultMessage: 'report audio'
  },
  reportImage: {
    id: 'Global_ReportImage',
    defaultMessage: 'report image'
  },
  reportText: {
    id: 'Global_ReportText',
    defaultMessage: 'report text'
  },
  save: {
    id: 'Global_Save',
    defaultMessage: 'save'
  },
  previous: {
    id: 'Global_Previous',
    defaultMessage: 'previous'
  },
  next: {
    id: 'Global_Next',
    defaultMessage: 'next'
  },
  finish: {
    id: 'Global_Finish',
    defaultMessage: 'Finish'
  },
  check: {
    id: 'Global_Check',
    defaultMessage: 'check'
  },
  enterCorrectSentence: {
    id: 'Global_EnterCorrectSentence',
    defaultMessage: 'Enter the correct sentence here'
  },
  correctAnswerIs: {
    id: 'Global_CorrectAnswerIs',
    defaultMessage: 'Correct answer is'
  },
  start: {
    id: 'Global_Start',
    defaultMessage: 'start'
  },
  skip: {
    id: 'Global_Skip',
    defaultMessage: 'skip'
  },
  correct: {
    id: 'Global_Correct',
    defaultMessage: 'correct'
  },
  incorrect: {
    id: 'Global_Incorrect',
    defaultMessage: 'incorrect'
  },

  volume: {
    id: 'Global_Volume',
    defaultMessage: 'Volume'
  },

  normalSpeed: {
    id: 'Global_NormalSpeed',
    defaultMessage: 'Normal'
  },
  slowSpeed: {
    id: 'Global_SlowSpeed',
    defaultMessage: 'Slow'
  },
  normalStart: {
    id: 'Global_NormalStart',
    defaultMessage: 'Normal'
  },
  softStart: {
    id: 'Global_SoftStart',
    defaultMessage: 'Soft start'
  },
  extraSlowSpeed: {
    id: 'Global_ExtraSlowSpeed',
    defaultMessage: 'Very slow'
  },
  startSample: {
    id: 'Global_StartSample',
    defaultMessage: 'Listen to the sample'
  },
  pauseSample: {
    id: 'Global_PauseSample',
    defaultMessage: 'Stop sample'
  },
  startPractice: {
    id: 'Global_StartPractice',
    defaultMessage: 'Start Practice'
  },
  stopPractice: {
    id: 'Global_StopPractice',
    defaultMessage: 'Stop Practice'
  },

  voice: {
    id: 'Global_Voice',
    defaultMessage: 'Voice ON/OFF'
  },
  visualGuidance: {
    id: 'Global_VisualGuidance',
    defaultMessage: 'Visual guidance ON/OFF'
  },
  metronome: {
    id: 'Global_Metronome',
    defaultMessage: 'Metronome ON/OFF'
  },
  timer: {
    id: 'Global_Timer',
    defaultMessage: 'Timer ON/OFF'
  },
  tooFast: {
    id: 'Global_TooFast',
    defaultMessage: 'Too Fast'
  },
  tooSlow: {
    id: 'Global_TooSlow',
    defaultMessage: 'Too Slow'
  },
  nailedIt: {
    id: 'Global_NailedIt',
    defaultMessage: 'Correct'
  },
  instructions: {
    id: 'Global_Instructions',
    defaultMessage: 'Instructions'
  },
  location3: {
    id: 'Global_Location3',
    defaultMessage: 'location 3'
  },
  location4: {
    id: 'Global_Location4',
    defaultMessage: 'location 4'
  },
  location5: {
    id: 'Global_Location5',
    defaultMessage: 'location 5'
  },
  location6: {
    id: 'Global_Location6',
    defaultMessage: 'location 6'
  },
  location7: {
    id: 'Global_Location7',
    defaultMessage: 'location 7'
  },
  location8: {
    id: 'Global_Location8',
    defaultMessage: 'location 8'
  },
  confirmationModalMessage: {
    id: 'Global_ConfirmationModalMessage',
    defaultMessage: 'Do you want to save before leaving?'
  },
  shortDescriptionError: {
    id: 'Global_ShortDescriptionError',
    defaultMessage: 'Please enter the description'
  },
  titleError: {
    id: 'Global_TitleError',
    defaultMessage: 'Please enter the title'
  },
  correctAnswerError: {
    id: 'Global_CorrectAnswerError',
    defaultMessage: 'Please enter the correct answer'
  },
  imageError: {
    id: 'Global_ImageError',
    defaultMessage: 'Please select the image'
  },
  audioError: {
    id: 'Global_AudioError',
    defaultMessage: 'Please select the audio'
  },
  incorrectAnswerError: {
    id: 'Global_IncorrectAnswerError',
    defaultMessage: 'Please enter the incorrect answer'
  },
  topicError: {
    id: 'Global_TopicError',
    defaultMessage: 'Please select the topic'
  },
  settingsNumberOfWordsError: {
    id: 'Global_NumberOfWordsError',
    defaultMessage: 'only numbers'
  },
  formNotCompletedNotification: {
    id: 'Global_FormNotCompletedNotification',
    defaultMessage: 'Form is not completed'
  },
  formSaveSuccessNotification: {
    id: 'Global_FormSaveSuccessNotification',
    defaultMessage: 'Saved successfully'
  },
  selectTopicLabel: {
    id: 'Global_SelectTopicLabel',
    defaultMessage: 'Select Topic'
  },
  none: {
    id: 'Global_None',
    defaultMessage: 'None'
  },
  selectCorrectAnswerLabel: {
    id: 'Global_selectCorrectAnswerLabel',
    defaultMessage: 'Select correct answer'
  },
  selectIncorrectAnswerLabel: {
    id: 'Global_selectIncorrectAnswerLabel',
    defaultMessage: 'Select incorrect answer'
  },
  sentenceInputPlaceholder: {
    id: 'Global_SentenceInputPlaceholder',
    defaultMessage: 'Enter the Sentence'
  },
  letters: {
    id: 'Global_Letters',
    defaultMessage: 'Letters'
  },
  capitalLetters: {
    id: 'Global_CapitalLetters',
    defaultMessage: 'Capital letters'
  },
  upperLetters: {
    id: 'Global_UppercaseLetters',
    defaultMessage: 'Uppercase'
  },
  asIs: {
    id: 'Global_AsIs',
    defaultMessage: 'As is'
  },
  numberOfAnswers: {
    id: 'Global_NumberOfAnswers',
    defaultMessage: 'Default number of answers'
  },
  audioSpeed: {
    id: 'Global_AudioSpeed',
    defaultMessage: 'Speed of the audio'
  },
  maxPairsNumber: {
    id: 'Global_PairsNumber',
    defaultMessage: 'Number of maximum pairs'
  },
  practiceLength: {
    id: 'Global_PracticeLength',
    defaultMessage: 'Recommended length of practice'
  },
  minute1: {
    id: 'Global_Minute1',
    defaultMessage: '1 minute'
  },
  minute2: {
    id: 'Global_Minute2',
    defaultMessage: '2 minutes'
  },
  minute3: {
    id: 'Global_Minute3',
    defaultMessage: '3 minutes'
  },
  minute4: {
    id: 'Global_Minute4',
    defaultMessage: '4 minutes'
  },
  minute5: {
    id: 'Global_Minute5',
    defaultMessage: '5 minutes'
  },
  minute6: {
    id: 'Global_Minute6',
    defaultMessage: '6 minutes'
  },
  minute7: {
    id: 'Global_Minute7',
    defaultMessage: '7 minutes'
  },
  minute8: {
    id: 'Global_Minute8',
    defaultMessage: '8 minutes'
  },
  minute9: {
    id: 'Global_Minute9',
    defaultMessage: '9 minutes'
  },
  minute10: {
    id: 'Global_Minute10',
    defaultMessage: '10 minutes'
  },
  minute11: {
    id: 'Global_Minute11',
    defaultMessage: '11 minutes'
  },
  minute12: {
    id: 'Global_Minute12',
    defaultMessage: '12 minutes'
  },
  minute13: {
    id: 'Global_Minute13',
    defaultMessage: '13 minutes'
  },
  minute14: {
    id: 'Global_Minute14',
    defaultMessage: '14 minutes'
  },
  minute15: {
    id: 'Global_Minute15',
    defaultMessage: '15 minutes'
  },
  loadingSpeed: {
    id: 'Global_LoadingSpeed',
    defaultMessage: 'Next exercise loading speed'
  },
  fast: {
    id: 'Global_Fast',
    defaultMessage: 'Fast'
  },
  normal: {
    id: 'Global_Normal',
    defaultMessage: 'Normal'
  },
  slow: {
    id: 'Global_Slow',
    defaultMessage: 'Slow'
  },
  hideCorrect: {
    id: 'Global_HideCorrect',
    defaultMessage: 'Hide correct answer'
  },
  yes: {
    id: 'Global_Yes',
    defaultMessage: 'Yes'
  },
  no: {
    id: 'Global_No',
    defaultMessage: 'No'
  },
  enable: {
    id: 'Global_Enable',
    defaultMessage: 'Enable'
  },
  disable: {
    id: 'Global_Disable',
    defaultMessage: 'Disable'
  },
  sharing: {
    id: 'Global_Sharing',
    defaultMessage: 'Sharing'
  },
  private: {
    id: 'Global_Private',
    defaultMessage: 'Private'
  },
  public: {
    id: 'Global_Public',
    defaultMessage: 'Public'
  },
  autoValidation: {
    id: 'Global_AutoValidation',
    defaultMessage: 'Automatic validation'
  },
  timeline: {
    id: 'Global_Timeline',
    defaultMessage: 'Timeline'
  },
  randomOrder: {
    id: 'Global_RandomOrder',
    defaultMessage: 'Show in random order'
  },
  speechRecognition: {
    id: 'Global_SpeechRecognition',
    defaultMessage: 'Speech recognition'
  },
  on: {
    id: 'Global_On',
    defaultMessage: 'On'
  },
  off: {
    id: 'Global_Off',
    defaultMessage: 'Off'
  },
  correctAnswerAs: {
    id: 'Global_CorrectAnswerAs',
    defaultMessage: 'Show correct answer as:'
  },
  word: {
    id: 'Global_Word',
    defaultMessage: 'Word/Sentence'
  },
  dotted: {
    id: 'Global_Dotted',
    defaultMessage: 'Dotted'
  },
  nextButton: {
    id: 'Global_NextButton',
    defaultMessage: 'next button'
  },
  correctIncorrect: {
    id: 'Global_CorrectIncorrect',
    defaultMessage: 'correct & incorrect buttons'
  },
  auto1: {
    id: 'Global_AutoNext1',
    defaultMessage: 'auto next after 1 second'
  },
  auto2: {
    id: 'Global_AutoNext2',
    defaultMessage: 'auto next after 2 seconds'
  },
  auto3: {
    id: 'Global_AutoNext3',
    defaultMessage: 'auto next after 3 seconds'
  },
  listen: {
    id: 'Global_Listen',
    defaultMessage: 'Listen'
  },
  repeat: {
    id: 'Global_Repeat',
    defaultMessage: 'Repeat'
  },
  listenRepeat: {
    id: 'Global_ListenRepeat',
    defaultMessage: 'Listen and repeat'
  },
  close: {
    id: 'Global_Close',
    defaultMessage: 'close'
  },
  yourCorrectAnswer: {
    id: 'Global_YourCorrectAnswer',
    defaultMessage: 'Your answer is correct'
  },
  back: {
    id: 'Global_Back',
    defaultMessage: 'Back'
  },
  practice: {
    id: 'Global_Practice',
    defaultMessage: 'Practice'
  },
  freshStart: {
    id: 'Global_FreshStart',
    defaultMessage: 'Fresh start'
  },
  answersFirst: {
    id: 'Global_AnswersFirst',
    defaultMessage: 'Optional answers before the sentence'
  },
  audioFirst: {
    id: 'Global_AudioFirst',
    defaultMessage: 'Play audio before showing pictures'
  },
  audioAtTheSameTime: {
    id: 'Global_AudioAtTheSameTime',
    defaultMessage: 'Play audio at the same time with showing pictures'
  },
  selectPatient: {
    id: 'Global_SelectPatient',
    defaultMessage: 'Select a patient'
  },
  myPatients: {
    id: 'Global_MyPatients',
    defaultMessage: 'My patients'
  },
  createPatient: {
    id: 'Global_CreatePatient',
    defaultMessage: 'Create a new patient'
  },
  createPatientButton: {
    id: 'Global_CreatePatientButton',
    defaultMessage: 'Create new patient'
  },
  enterFirstName: {
    id: 'Global_EnterFirstName',
    defaultMessage: 'Enter patient\'s first name'
  },
  enterLastName: {
    id: 'Global_EnterLastName',
    defaultMessage: 'Enter patient\'s last name'
  },

  enterBirthDate: {
    id: 'Global_EnterBirthDate',
    defaultMessage: 'Enter patient\'s birth date'
  },

  enterGender: {
    id: 'Global_EnterGender',
    defaultMessage: 'Enter patient\'s gender'
  },

  male: {
    id: 'Global_Male',
    defaultMessage: 'Male'
  },

  female: {
    id: 'Global_Female',
    defaultMessage: 'Female'
  },

  maleVoice: {
    id: 'Global_MaleVoice',
    defaultMessage: 'Male Voice'
  },

  femaleVoice: {
    id: 'Global_FemaleVoice',
    defaultMessage: 'Female Voice'
  },

  nd: {
    id: 'Global_NoGender',
    defaultMessage: 'ND'
  },

  enterHobbies: {
    id: 'Global_EnterHobbies',
    defaultMessage: 'Enter patient\'s hobbies'
  },

  firstName: {
    id: 'Global_EnterTutorFirstName',
    defaultMessage: 'First name'
  },

  lastName: {
    id: 'Global_EnterTutorLastName',
    defaultMessage: 'Last name'
  },

  enterEmail: {
    id: 'Global_EnterEmail',
    defaultMessage: 'E-mail address'
  },

  phone: {
    id: 'Global_Phone',
    defaultMessage: 'Phone number'
  },
  user: {
    id: 'Global_User',
    defaultMessage: 'User'
  },
  fullName: {
    id: 'Global_FullName',
    defaultMessage: 'Name'
  },
  accessCode: {
    id: 'Global_AccessCode',
    defaultMessage: 'Access Code'
  },
  lastLogin: {
    id: 'Global_LastLogin',
    defaultMessage: 'Last Login'
  },
  accountActivity: {
    id: 'Global_ActiveTo',
    defaultMessage: 'Account Activity'
  },
  expired: {
    id: 'Global_Expired',
    defaultMessage: 'Expired'
  },
  underDevelopment: {
    id: 'Global_UnderDevelopment',
    defaultMessage: 'Sorry, this functionality is under development.'
  },
  noExercises: {
    id: 'Global_NoExercises',
    defaultMessage: 'There are no exercises in this section'
  },
  create: {
    id: 'Global_Create',
    defaultMessage: 'create'
  },

  practiceWords: {
    id: 'Global_PracticeWords',
    defaultMessage: 'Practice Words'
  },
  practiceSentences: {
    id: 'Global_PracticeSentences',
    defaultMessage: 'Practice Sentences'
  },

  paymentReminder: {
    id: 'Global_PaymentReminder',
    defaultMessage: 'Your training period is over.'
  },
  paymentLink: {
    id: 'Global_PaymentLink',
    defaultMessage: 'Show All Exercises'
  },
});
