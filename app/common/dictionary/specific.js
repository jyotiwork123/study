import { defineMessages } from 'react-intl';

export const templatesGridViewMessages = defineMessages({
  templatesGridViewTopSubtitle: {
    id: 'TemplatesGridView_topSubtitle',
    defaultMessage: 'Here you can see all the templates. You can create new exercise by selecting the template and clicking on CREATE button.'
  },
  template5Title: {
    id: 'TemplatesGridView_Template5Title',
    defaultMessage: 'Template 5'
  },
  template5Description: {
    id: 'TemplatesGridView_Template5Description',
    defaultMessage: 'You can use up to 8 words and one images. User must select the correct answer that will match to image.'
  },
  template5aTitle: {
    id: 'TemplatesGridView_Template5aTitle',
    defaultMessage: 'Template 5a'
  },
  template5aDescription: {
    id: 'TemplatesGridView_Template5aDescription',
    defaultMessage: 'You can use up to 8 words and one sentence. User must select the correct answer that will match with the sentence.'
  },
  template5bTitle: {
    id: 'TemplatesGridView_Template5bTitle',
    defaultMessage: 'Template 5b'
  },
  template5bDescription: {
    id: 'TemplatesGridView_Template5bDescription',
    defaultMessage: 'You can create topic based exercises. User will have to select the correct answer that will match with the image.'
  },
  template6Title: {
    id: 'TemplatesGridView_Template6Title',
    defaultMessage: 'Template 6'
  },
  template6Description: {
    id: 'TemplatesGridView_Template6Description',
    defaultMessage: 'Words in the sentence will be mixed. User must put the words into correct order.'
  },
  template7Title: {
    id: 'TemplatesGridView_Template7Title',
    defaultMessage: 'Template 7'
  },
  template7Description: {
    id: 'TemplatesGridView_Template7Description',
    defaultMessage: 'Use this template if you want the user to type some text.'
  },
  template8Title: {
    id: 'TemplatesGridView_Template8Title',
    defaultMessage: 'Template 8'
  },
  template8Description: {
    id: 'TemplatesGridView_Template8Description',
    defaultMessage: 'This template is designed for speech automation. You can create exercises that will play audio automatically. '
  },
  template9Title: {
    id: 'TemplatesGridView_Template9Title',
    defaultMessage: 'Template 9'
  },
  template9Description: {
    id: 'TemplatesGridView_Template9Description',
    defaultMessage: 'Use this exercise if you want to the user to match the phrases.'
  }
});

export const exercisesGridViewMessages = defineMessages({
  exercisesGridViewTopSubtitle1: {
    id: 'ExercisesGridView_topSubtitle1',
    defaultMessage: 'Here you can see all the exercises. You can edit or delete the exericses. If you would like to create a new exercise then go to Templates page.'
  },
  exercisesGridViewTopSubtitle2: {
    id: 'ExercisesGridView_topSubtitle2',
    defaultMessage: 'Here you can see the exercises that you have created. It is possible to filter the exercises according to their type and language level.'
  },
  exercisesGridViewTopSubtitle3: {
    id: 'ExercisesGridView_topSubtitle3',
    defaultMessage: 'Here you can see your favorite exercises. It is possible to filter the exercises according to their type and language level.'
  },
  exercisesGridViewTopSubtitle4: {
    id: 'ExercisesGridView_topSubtitle4',
    defaultMessage: 'Here you can see suggested exercises. It is possible to filter the exercises according to their type and language level.'
  },
  exercisesGridViewModalMessage: {
    id: 'ExercisesGridView_ModalMessage',
    defaultMessage: 'Are you sure you would like to delete this exercise?'
  },
  increaseSearch: {
    id: 'ExercisesGridView_IncreaseSearch',
    defaultMessage: 'Please increase your search parameters.'
  }
});

export const warnings = defineMessages({
  smallScreen: {
    id: 'Warnings_SmallScreenWarning',
    defaultMessage: 'Unfortunately, this screen is too small for these exercises. We support only computers and tablets.'
  },
  noAudioSupport: {
    id: 'Warnings_NoAudioSupport',
    defaultMessage: 'Unfortunately your browser doesn\'t support recording audio'
  },
  noAudioDevice: {
    id: 'Warnings_NoAudioDevice',
    defaultMessage: 'Please connect an audio recording device and try again'
  },
  attention: {
    id: 'Warnings_Attention',
    defaultMessage: 'Attention'
  }
});

export const exercisesPreview = defineMessages({
  back: {
    id: 'ExercisesPreview_Back',
    defaultMessage: 'back to exercises'
  },
  begin: {
    id: 'ExercisesPreview_Begin',
    defaultMessage: 'click to begin'
  },
  favourites: {
    id: 'ExercisesPreview_Favourites',
    defaultMessage: 'add to favourites'
  },
  removeFavourites: {
    id: 'ExercisesPreview_RemoveFavourites',
    defaultMessage: 'remove from favourites'
  },
  suggestions: {
    id: 'ExercisesPreview_Suggestions',
    defaultMessage: 'add to suggestions'
  },
  removeSuggestions: {
    id: 'ExercisesPreview_RemoveSuggestions',
    defaultMessage: 'remove from suggestions'
  },
  introduction: {
    id: 'ExercisesPreview_Introduction',
    defaultMessage: 'Introduction'
  },
  benefits: {
    id: 'ExercisesPreview_Benefits',
    defaultMessage: 'Benefits'
  },
  usefullness: {
    id: 'ExercisesPreview_Usefullness',
    defaultMessage: 'Usefullness'
  },
  selectTopic: {
    id: 'ExercisesPreview_SelectTopic',
    defaultMessage: 'Select topic'
  },
  checkResults: {
    id: 'ExercisesPreview_CheckResults',
    defaultMessage: 'Check results'
  },
  stutteringIntroduction: {
    id: 'ExercisesPreview_StutteringIntroduction',
    defaultMessage: 'SMOOTH SPEECH EXERCISES'
  },
  stutteringDescription1: {
    id: 'ExercisesPreview_StutteringDescription1',
    defaultMessage: '_'
  },
  stutteringDescription2: {
    id: 'ExercisesPreview_StutteringDescription2',
    defaultMessage: '_'
  },
  stutteringDescription3: {
    id: 'ExercisesPreview_StutteringDescription3',
    defaultMessage: '_'
  },
  stutteringDescription4: {
    id: 'ExercisesPreview_StutteringDescription4',
    defaultMessage: '_'
  },
  stutteringDescription5: {
    id: 'ExercisesPreview_StutteringDescription5',
    defaultMessage: '_'
  }
});

export const noContentSection = defineMessages({
  title: {
    id: 'NoContentSection_Title',
    defaultMessage: 'You haven\'t added any exercises to the favorites'
  },
  subTitle: {
    id: 'NoContentSection_SubTitle',
    defaultMessage: 'To add exercises to the favourites follow these steps:'
  },
  step1: {
    id: 'NoContentSection_Step_one',
    defaultMessage: 'Step 1 - Select the exercise that you like'
  },
  step2: {
    id: 'NoContentSection_Step_two',
    defaultMessage: 'Step 2 - Click on a Preview button'
  },
  step3: {
    id: 'NoContentSection_Step_three',
    defaultMessage: 'Step 3 - Select Add to favorites'
  },
  step4: {
    id: 'NoContentSection_Step_four',
    defaultMessage: 'Step 4 - This exercise is now added to the Favorites'
  }
});

export const noSuggestionSection = defineMessages({
  titlePatient: {
    id: 'NoSuggestionSection_PatientTitle',
    defaultMessage: 'You have no assignments yet.'
  },
  title: {
    id: 'NoSuggestionSection_Title',
    defaultMessage: 'You haven\'t suggested any exercises to this user'
  },
  subTitle: {
    id: 'NoSuggestionSection_SubTitle',
    defaultMessage: 'To add exercises to the suggestions follow these steps:'
  },
  step1: {
    id: 'NoSuggestionSection_Step_one',
    defaultMessage: 'Step 1 - Select the exercise that you like'
  },
  step2: {
    id: 'NoSuggestionSection_Step_two',
    defaultMessage: 'Step 2 - Click on a "Preview" button'
  },
  step3: {
    id: 'NoSuggestionSection_Step_three',
    defaultMessage: 'Select "Add to suggestions'
  },
  step4: {
    id: 'NoSuggestionSection_Step_four',
    defaultMessage: 'Step 4 - This exercise is now added to the Suggestions'
  }
});

export const promptTokenRefresh = defineMessages({
  description: {
    id: 'PromptTokenRefresh_Description',
    defaultMessage: 'Seems that you have been inactive for a while now. For security reasons you will be logged out automatically after the time below.'
  },
  continue: {
    id: 'PromptTokenRefresh_Continue',
    defaultMessage: 'Continue'
  },
  logOut: {
    id: 'PromptTokenRefresh_LogOut',
    defaultMessage: 'Log Out'
  }
});

export const exerciseCardMessages = defineMessages({
  edit: {
    id: 'ExerciseCardMessages_EditButton',
    defaultMessage: 'Edit'
  },
  delete: {
    id: 'ExerciseCardMessages_DeleteButton',
    defaultMessage: 'Delete'
  }
});

export const templateCardMessages = defineMessages({
  create: {
    id: 'TemplateCardMessages_CreateButton',
    defaultMessage: 'Create'
  }
});

export const template5aMessages = defineMessages({
  sentenceError: {
    id: 'Template5aMessages_SentenceError',
    defaultMessage: 'Please select the sentence'
  }
});

export const template5bMessages = defineMessages({
  selectTopicOption1: {
    id: 'Template5bMessages_SelectTopicOption1',
    defaultMessage: 'transport'
  },
  selectTopicOption2: {
    id: 'Template5bMessages_SelectTopicOption2',
    defaultMessage: 'health'
  },
  selectTopicOption3: {
    id: 'Template5bMessages_SelectTopicOption3',
    defaultMessage: 'food'
  },
  selectCorrectAnswerOption1: {
    id: 'Template5bMessages_selectCorrectAnswerOption1',
    defaultMessage: 'text'
  },
  selectCorrectAnswerOption2: {
    id: 'Template5bMessages_selectCorrectAnswerOption2',
    defaultMessage: 'topic'
  },
  selectCorrectAnswerOption3: {
    id: 'Template5bMessages_selectCorrectAnswerOption3',
    defaultMessage: 'subtopic'
  },
  selectIncorrectAnswerOption1: {
    id: 'Template5bMessages_selectIncorrectAnswerOption1',
    defaultMessage: 'text'
  },
  selectIncorrectAnswerOption2: {
    id: 'Template5bMessages_selectIncorrectAnswerOption2',
    defaultMessage: 'topic'
  },
  selectIncorrectAnswerOption3: {
    id: 'Template5bMessages_selectIncorrectAnswerOption3',
    defaultMessage: 'subtopic'
  }
});

export const template6Messages = defineMessages({
  originalTextError: {
    id: 'Template6Messages_originalTextError',
    defaultMessage: 'Please enter the original text'
  }
});

export const template7Messages = defineMessages({
  expectedAnswerError: {
    id: 'Template7Messages_expectedAnswerError',
    defaultMessage: 'Please select the expected answer'
  }
});

export const template8Messages = defineMessages({
  correctAnswerError: {
    id: 'Template8Messages_CorrectAnswerError',
    defaultMessage: 'Please select the correct answer'
  },
  textError: {
    id: 'Template8Messages_TextAnswerError',
    defaultMessage: 'Please enter the exercise text'
  }
});

export const template9Messages = defineMessages({
  wordError: {
    id: 'Template9Messages_WordError',
    defaultMessage: 'Please enter the word'
  },
  matchError: {
    id: 'Template9Messages_MatchError',
    defaultMessage: 'Please enter the match'
  }
});

export const headerTemplateMessages = defineMessages({
  saving: {
    id: 'HeaderTemplateMessages_SavingMessage',
    defaultMessage: 'Saving...'
  },
  save: {
    id: 'HeaderTemplateMessages_SaveMessage',
    defaultMessage: 'Save'
  }
});

export const confirmationModalMessages = defineMessages({
  yes: {
    id: 'ConfirmationModalMessages_YesButton',
    defaultMessage: 'Yes'
  },
  no: {
    id: 'ConfirmationModalMessages_NoButton',
    defaultMessage: 'No'
  }
});

export const templateTabsMessages = defineMessages({
  exercise: {
    id: 'TemplateTabsMessages_Exercise',
    defaultMessage: 'EXERCISE'
  },
  description: {
    id: 'TemplateTabsMessages_Description',
    defaultMessage: 'DESCRIPTION'
  },
  settings: {
    id: 'TemplateTabsMessages_Settings',
    defaultMessage: 'SETTINGS'
  }
});

export const exerciseTabsMessages = defineMessages({
  sentenceInputLabel: {
    id: 'ExerciseTabsMessages_SentenceInputLabel',
    defaultMessage: 'Sentence'
  },
  imageInputLabel: {
    id: 'ExerciseTabsMessages_ImageInputLabel',
    defaultMessage: 'Correct Answer'
  },
  imageInputPlaceholder: {
    id: 'ExerciseTabsMessages_ImageInputPlaceholder',
    defaultMessage: 'Enter the correct answer'
  },
  incorrectInputLabel: {
    id: 'ExerciseTabsMessages_IncorrectAnswerLabel',
    defaultMessage: 'Incorrect Answer'
  },
  incorrectInputPlaceholder: {
    id: 'ExerciseTabsMessages_IncorrectAnswerPlaceholder',
    defaultMessage: 'Enter the incorrect answer'
  },
  moreIncorrectInput: {
    id: 'ExerciseTabsMessages_MoreIncorrectInput',
    defaultMessage: 'Add More Incorrect Answers'
  }
});

export const exerciseTab6Messages = defineMessages({
  sentenceInputLabel: {
    id: 'ExerciseTab6Messages_SentenceInputLabel',
    defaultMessage: 'Original Text'
  },
  sentenceInputPlaceholder: {
    id: 'ExerciseTab6Messages_SentenceInputPlaceholder',
    defaultMessage: 'Enter the original text'
  },
  sentenceInputError: {
    id: 'ExerciseTab6Messages_SentenceInputError',
    defaultMessage: 'From 2 to 8 words'
  }
});

export const exerciseTab7Messages = defineMessages({
  sentenceInputLabel: {
    id: 'ExerciseTab7Messages_SentenceInputLabel',
    defaultMessage: 'Original Sentence'
  },
  expectedAnswerInputLabel: {
    id: 'ExerciseTab7Messages_ExpectedAnswerInputLabel',
    defaultMessage: 'Expected Answer'
  },
  expectedAnswerInputPlaceholder: {
    id: 'ExerciseTab7Messages_ExpectedAnswerInputPlaceholder',
    defaultMessage: 'Enter the expected answer'
  }
});

export const exerciseTab8Messages = defineMessages({
  selectTopicOption1: {
    id: 'ExerciseTab8Messages_SelectTopicOption1',
    defaultMessage: 'Months'
  },
  selectTopicOption2: {
    id: 'ExerciseTab8Messages_SelectTopicOption2',
    defaultMessage: 'Days'
  },
  selectTopicOption3: {
    id: 'ExerciseTab8Messages_SelectTopicOption3',
    defaultMessage: 'Numbers'
  },
  selectTopicOption4: {
    id: 'ExerciseTab8Messages_SelectTopicOption4',
    defaultMessage: 'Seasons'
  },
  selectSkippedLabel: {
    id: 'ExerciseTab8Messages_SkippedLabel',
    defaultMessage: 'Skip these words'
  },
  selectCorrectAnswerOption1: {
    id: 'ExerciseTab8Messages_SelectCorrectAnswerOption1',
    defaultMessage: 'lorem ipsum'
  },
  selectCorrectAnswerOption2: {
    id: 'ExerciseTab8Messages_SelectCorrectAnswerOption2',
    defaultMessage: 'lorem ipsum'
  },
  selectCorrectAnswerOption3: {
    id: 'ExerciseTab8Messages_SelectCorrectAnswerOption3',
    defaultMessage: 'lorem ipsum'
  },
  selectAudioProfileLabel: {
    id: 'ExerciseTab8Messages_SelectAudioProfile',
    defaultMessage: 'Audio'
  },
  selectAudioProfileOption1: {
    id: 'ExerciseTab8Messages_SelectAudioProfileOption1',
    defaultMessage: 'On'
  },
  selectAudioProfileOption2: {
    id: 'ExerciseTab8Messages_SelectAudioProfileOption2',
    defaultMessage: 'Off'
  },
  selectAudioProfileOption3: {
    id: 'ExerciseTab8Messages_SelectAudioProfileOption3',
    defaultMessage: 'Skip skipped words'
  },
  selectIconLabel: {
    id: 'ExerciseTab8Messages_IconLabel',
    defaultMessage: 'Icons'
  },
  selectIconOption1: {
    id: 'ExerciseTab8Messages_SelectIconOption1',
    defaultMessage: 'None'
  },
  selectIconOption2: {
    id: 'ExerciseTab8Messages_SelectIconOption2',
    defaultMessage: 'Listen'
  },
  selectIconOption3: {
    id: 'ExerciseTab8Messages_SelectIconOption3',
    defaultMessage: 'Repeat'
  },
  selectIconOption4: {
    id: 'ExerciseTab8Messages_SelectIconOption4',
    defaultMessage: 'Listen and repeat'
  },
  exerciseTextLabel: {
    id: 'ExerciseTab8Messages_ExerciseTextLabel',
    defaultMessage: 'Text in the exercise'
  },
  exerciseTextPlaceholder: {
    id: 'ExerciseTab8Messages_ExerciseTextPlaceholder',
    defaultMessage: 'Enter the text here'
  }
});

export const exerciseTab9Messages = defineMessages({
  selectTopicOption1: {
    id: 'ExerciseTab9Messages_SelectTopicOption1',
    defaultMessage: 'lorem ipsum'
  },
  selectTopicOption2: {
    id: 'ExerciseTab9Messages_SelectTopicOption2',
    defaultMessage: 'lorem ipsum'
  },
  selectTopicOption3: {
    id: 'ExerciseTab9Messages_SelectTopicOption3',
    defaultMessage: 'lorem ipsum'
  },
  selectCorrectAnswerOption1: {
    id: 'ExerciseTab9Messages_SelectCorrectAnswerOption1',
    defaultMessage: 'lorem ipsum'
  },
  selectCorrectAnswerOption2: {
    id: 'ExerciseTab9Messages_SelectCorrectAnswerOption2',
    defaultMessage: 'lorem ipsum'
  },
  selectCorrectAnswerOption3: {
    id: 'ExerciseTab9Messages_SelectCorrectAnswerOption3',
    defaultMessage: 'lorem ipsum'
  },
  inputWordLabel: {
    id: 'ExerciseTab9Messages_InputWordLabel',
    defaultMessage: 'Number, word or sentence'
  },
  inputWordPlaceholder: {
    id: 'ExerciseTab9Messages_InputWordPlaceholder',
    defaultMessage: 'Enter number, word or sentence'
  },
  inputMatchLabel: {
    id: 'ExerciseTab9Messages_InputMatchLabel',
    defaultMessage: 'Match'
  },
  inputMatchPlaceholder: {
    id: 'ExerciseTab9Messages_InputMatchPlaceholder',
    defaultMessage: 'Enter the match'
  }
});

export const imagePickerRightMessages = defineMessages({
  pick: {
    id: 'ImagePickerRightMessages_Pick',
    defaultMessage: 'Pick'
  },
  image: {
    id: 'ImagePickerRightMessages_Image',
    defaultMessage: 'Image'
  },
  audio: {
    id: 'ImagePickerRightMessages_Audio',
    defaultMessage: 'Audio'
  }
});

export const imagePickerMessages = defineMessages({
  selectedIcon: {
    id: 'ImagePickerMessages_SelectIcon',
    defaultMessage: 'Select an Image'
  },
  loadMore: {
    id: 'ImagePickerMessages_LoadMoreImages',
    defaultMessage: 'LOAD MORE'
  },
  selectButton: {
    id: 'ImagePickerMessages_Select',
    defaultMessage: 'SELECT'
  },
  cancelButton: {
    id: 'ImagePickerMessages_Cancel',
    defaultMessage: 'CANCEL'
  }
});

export const viewHandlePanelMessages = defineMessages({
  deleteBlock: {
    id: 'ViewHandlePanelMessages_DeleteBlock',
    defaultMessage: 'Delete This Block'
  },
  addBlock: {
    id: 'ViewHandlePanelMessages_AddBlock',
    defaultMessage: 'Add Next Block'
  }
});

export const descriptionTabMessages = defineMessages({
  pureTitleLabel: {
    id: 'DescriptionTabMessages_Title',
    defaultMessage: 'Title'
  },
  descriptionLabel: {
    id: 'DescriptionTabMessages_ShortDescription',
    defaultMessage: 'Description of the exercise (350-500 characters)'
  },
  descriptionPlaceholder: {
    id: 'DescriptionTabMessages_DescriptionPlaceholder',
    defaultMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a faucibus risus. Sed accumsan lacus nec elementum ullamcorper.'
  },
  descriptionInputError: {
    id: 'DescriptionTabMessages_ShortDescriptionError',
    defaultMessage: 'From 350 to 500 characters'
  },
  descriptionShortCommandLabel: {
    id: 'DescriptionTabMessages_DescriptionShortCommandLabel',
    defaultMessage: 'Short Command / instruction'
  },
  descriptionShortCommandPlaceholder: {
    id: 'DescriptionTabMessages_DescriptionShortCommandPlaceholder',
    defaultMessage: 'Active text input'
  },
  descriptionExerciseDescriptionLabel: {
    id: 'DescriptionTabMessages_DescriptionExerciseDescriptionLabel',
    defaultMessage: 'Instructions. How to use this exercise?'
  },
  descriptionExerciseDescriptionPlaceholder: {
    id: 'DescriptionTabMessages_DescriptionExerciseDescriptionPlaceholder',
    defaultMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a faucibus risus. Sed accumsan lacus nec elementum ullamcorper.'
  },
  descriptionExerciseUsefullnessLabel: {
    id: 'DescriptionTabMessages_DescriptionExerciseUsefullnessLabel',
    defaultMessage: 'Usefullness. How is this exercise usefull?'
  },
  descriptionExerciseUsefullnessPlaceholder: {
    id: 'DescriptionTabMessages_DescriptionExerciseUsefullnessPlaceholder',
    defaultMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a faucibus risus. Sed accumsan lacus nec elementum ullamcorper.'
  },
  descriptionExerciseBenefitsLabel: {
    id: 'DescriptionTabMessages_DescriptionExerciseBenefitsLabel',
    defaultMessage: 'Benefits. To who and on what conditions is this exercise beneficial?'
  },
  descriptionExerciseBenefitsPlaceholder: {
    id: 'DescriptionTabMessages_DescriptionExerciseBenefitsPlaceholder',
    defaultMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a faucibus risus. Sed accumsan lacus nec elementum ullamcorper.'
  }
});

export const thankYouPage = defineMessages({
  exerciseOver: {
    id: 'ThankYouPage_ExerciseOver',
    defaultMessage: 'EXERCISE IS OVER!'
  },
  repeatExercise: {
    id: 'ThankYouPage_RepeatExercise',
    defaultMessage: 'REPEAT THE EXERCISE'
  },
  showOtherExercises: {
    id: 'ThankYouPage_ShowOtherExercises',
    defaultMessage: 'SHOW OTHER EXERCISES'
  },
  summary: {
    id: 'ThankYouPage_Summary',
    defaultMessage: 'SUMMARY'
  },
  spent: {
    id: 'ThankYouPage_Spent',
    defaultMessage: 'You spent $xMinutes doing this exercise'
  },
  total: {
    id: 'ThankYouPage_Total',
    defaultMessage: 'Total time spent on doing this exercise is $xMinutes'
  },
  correctAnswers: {
    id: 'ThankYouPage_CorrectAnswers',
    defaultMessage: 'Correct answers'
  },
  correctTiming: {
    id: 'ThankYouPage_CorrectTiming',
    defaultMessage: 'Correct timing'
  },
  incorrectTiming: {
    id: 'ThankYouPage_IncorrectTiming',
    defaultMessage: 'Incorrect (too slow or too fast)'
  },
});

export const landingNotification = defineMessages({
  text: {
    id: 'LandingNotification_Text',
    defaultMessage: 'Kliki siia, kui oled huvitatud videokõne funktsionaalsuse arendamisest kõneravi.ee leheküljel'
  }
});
