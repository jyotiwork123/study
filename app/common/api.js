import axios from 'axios';
import { logout } from '../user/state/actions';

axios.interceptors.request.use((config) => config, (error) => Promise.reject(error));

axios.interceptors.response.use(
  (response) => {
    if (response.config.url === REFRESH_URL) {
      if (!response.data.result) {
        logout();
      }
    }
    return response;
  },
  (error) => {
    if (error.status === 401) {
      logout();
    }
    return Promise.reject(error);
  });
//rest
export const LOGIN_URL = 'https://login.cognuse.com/login';
export const EXPIRATION_URL = 'https://login.cognuse.com/api/ttl';
export const REFRESH_URL = 'https://login.cognuse.com/api/refresh';
export const PATIENTS_URL = 'https://api.cognuse.com/api/v1/core/patients';
export const GROUPS_URL = 'https://api.cognuse.com/api/v1/core/groups';
export const HOBBIES_URL = 'https://api.cognuse.com/api/v1/core/hobbies';
export const FAVORITES_URL = 'https://api.cognuse.com/api/v1/core/favorites';
export const USER_URL = 'https://api.cognuse.com/api/v1/core/users';

export const AUDIOS_URL = 'https://api.cognuse.com/api/v1/cpms/audios';

//graphql
export const GRAPHQL_URL = 'https://api.cognuse.com/api/v1/cpms/graphql';
export const GRAPHQL_URL_RESULTS = 'https://api.cognuse.com/api/v1/results-graphql/graphql';

export function apiConfig(token) {
  return (
    {
      headers: { Authorization: `Bearer ${token}` }
    }
  );
}
