import React, { Component } from 'react';

//redux
import { connect } from 'react-redux';
import { getPatients, editPatient } from '../../../patient/state/actions';
import { editProfile } from '../../../user/state/actions';

//router
import { Link } from 'react-router-dom';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//components
import InfoPopup from '../InfoPopup';
import UserDataDialog from '../UserDataDialog';
import TutorDataDialog from '../TutorDataDialog';

//styles, assets
import headerLogo from '../../../../images/konravi_logo_white.png';
import styles from './headerTemplate.css';

class HeaderTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTemplates: false,
      showPrompt: false,
      showUserDataDialog: false,
      showTutorDataDialog: false,
      visibleForTesting: 0
    };

    this.toggleShowPrompt = this.toggleShowPrompt.bind(this);
  }

  componentWillMount() {
    const { userRole } = this.props;
    this.setState({ showTemplates: userRole > 1 });
    this.props.getPatients();
  }

  toggleShowPrompt() {
    this.setState({ showPrompt: !this.state.showPrompt });
  }

  saveUserChanges(event, data) {
    this.props.editPatient(data);
    this.setState({ showUserDataDialog: false });
  }

  saveTutorChanges(event, data) {
    console.log('saveTutorChanges', data);
    this.props.editProfile(data);
    // this.setState({ showTutorDataDialog: false });
  }

  render() {
    const { title, handleBack, userId, logout, userRole, token, patients, patientId } = this.props;
    const { formatMessage } = this.props.intl;
    const { showTemplates, showPrompt, showUserDataDialog, showTutorDataDialog, visibleForTesting } = this.state;
    const buttonClasses = 'mdl-button mdl-layout__tab';
    const activeExerciseClass = title.toLowerCase() === formatMessage(globalMessages.exercises).toLowerCase() ? 'is-active' : '';
    const activeAudiosClass = title.toLowerCase() === formatMessage(globalMessages.audios).toLowerCase() ? 'is-active' : '';
    const activeImagesClass = title.toLowerCase() === formatMessage(globalMessages.images).toLowerCase() ? 'is-active' : '';
    const activeVideosClass = title.toLowerCase() === formatMessage(globalMessages.videos).toLowerCase() ? 'is-active' : '';
    const user = patients.filter(patientsEntry => patientsEntry._id === patientId);
    const activePatient = user[0] ? `: ${user[0].firstname} ${user[0].lastname}` : null;

    return (
      <header className={`mdl-layout__header ${styles.header}`}>
        <div className={`mdl-layout__header-row ${styles.headerRow}`}>
          <div className={`mdl-layout__tab ${styles.headerLogo}`} onClick={() => { this.setState({ visibleForTesting: this.state.visibleForTesting + 1 }); }}>
            <img src={headerLogo} alt="" className={`${styles.headerLogoImage}`} />
          </div>
          <nav className="mdl-navigation">
            <Link to="/" className={`${buttonClasses} ${styles.headerNavLinks}`}>
              {formatMessage(globalMessages.home)}
            </Link>
            <Link to="/practice" className={`${buttonClasses} ${styles.headerNavLinks} ${activeExerciseClass}`}>
              {formatMessage(globalMessages.exercises)}
            </Link>
            {
              userRole >= 10 &&
              <a href={`http://cpms.cognuse.com/login/${token}$$templates`} className={`${buttonClasses} ${styles.headerNavLinks}`}>
                {formatMessage(globalMessages.create)}
              </a>
            }
            
            {
              visibleForTesting > 5 && <Link to="/audios" className={`${buttonClasses} ${styles.headerNavLinks} ${activeAudiosClass}`}>
                {formatMessage(globalMessages.audios)}
              </Link>
            }
            {
              visibleForTesting > 5 && <Link to="/images" className={`${buttonClasses} ${styles.headerNavLinks} ${activeImagesClass}`}>
                {formatMessage(globalMessages.images)}
              </Link>
            }
            {/* {
              visibleForTesting > 5 && <Link to="/videos" className={`${buttonClasses} ${styles.headerNavLinks} ${activeVideosClass}`}>
                {formatMessage(globalMessages.videos)}
              </Link>
            } */}


            {/* <Link to="/audios" className={`${buttonClasses} ${styles.headerNavLinks} ${activeAudiosClass}`}>
              {formatMessage(globalMessages.audios)}
            </Link>
            <Link to="/images" className={`${buttonClasses} ${styles.headerNavLinks} ${activeImagesClass}`}>
              {formatMessage(globalMessages.images)}
            </Link>
            <Link to="/videos" className={`${buttonClasses} ${styles.headerNavLinks} ${activeVideosClass}`}>
              {formatMessage(globalMessages.videos)}
            </Link> */}
          </nav>
          <div className="mdl-layout-spacer" />
          {
            userRole >= 10 && activePatient &&
            <div className={`${styles.currentUser}`}>

              <div
                className={`${styles.userIcon}`}
                onClick={() => this.setState({ showUserDataDialog: true, showTutorDataDialog: false })}
                title={formatMessage(globalMessages.editUser)}
              >
                <i className="material-icons">person_outline</i>
              </div>

              {`${formatMessage(globalMessages.user)}`}
              <span className={styles.userName}>{activePatient}</span>
            </div>
          }
          <nav
            className={`mdl-navigation ${styles.headerSaveButton}`}
          // style={{ marginRight: '44px' }}
          // style={{ marginRight: '-40px' }}
          >
            <div
              className={`${buttonClasses} ${styles.headerNavLinks} ${styles.headerLogout}`}
              onClick={logout}
            >
              {formatMessage(globalMessages.logout)}
            </div>
          </nav>
          {/* <div
            className={`${buttonClasses} ${styles.headerNavLinks} ${styles.tutorIcon}`}
            onClick={() => this.setState({ showTutorDataDialog: true, showUserDataDialog: false })}
          >
            <i className="material-icons">person</i>
          </div> */}
        </div>
        {
          showPrompt &&
          <InfoPopup
            modalClassName={styles.modal}
            overlayClassName={styles.overlay}
            isOpen={showPrompt}
            onConfirm={this.toggleShowPrompt}
            contentLabel="results"
          >
            {formatMessage(globalMessages.underDevelopment)}
          </InfoPopup>
        }
        {
          showUserDataDialog && <UserDataDialog
            isOpen={this.state.showUserDataDialog}
            user={user[0]}
            onUpload={(event, data, locale) => this.saveUserChanges(event, data)}
            onCancel={() => this.setState({ showUserDataDialog: false })}
          />
        }
        {
          showTutorDataDialog && <TutorDataDialog
            isOpen={this.state.showTutorDataDialog}
            user={user[0]}
            onUpload={(event, data) => this.saveTutorChanges(event, data)}
            onCancel={() => this.setState({ showTutorDataDialog: false })}
          />
        }
      </header>
    );
  }
}

function mapStateToProps(state) {
  return {
    patients: state.Patient.patients,
    patientId: state.Patient.patientId
  };
}

export default connect(mapStateToProps, { getPatients, editPatient, editProfile })(injectIntl(HeaderTemplate));

// export default injectIntl(HeaderTemplate);
