import React, { Component } from 'react';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//redux
import { connect } from 'react-redux';

//utils
import {
  getImages
} from '../../../user/state/actions';
import { getImagesQuery } from '../../../common/queries';

//components
import Spinner from '../spinner';

//styles, assets
import styles from './imageTable.css';

class ImageTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      // images: null,
      sample: null,
      numberOfImages: 100
    };

    this.showMoreImages = this.showMoreImages.bind(this);

  }

  componentDidMount() {
    this.props.getImages(getImagesQuery(this.props.userLocale))
      .then(response =>
        this.setState({
          loaded: true,
        })
      );

    /**********************************
    ** Simulate Request (Fetch Images) 
    **********************************/
    // const response = [
    //   { 
    //     url: "https://dummyimage.com/1280x720/000/fff",
    //     title: "Dummy 1",
    //     size: "3.5 KB", 
    //     group: "Group-1", 
    //     tags: "Dummy, Image"
    //   },
    //   { 
    //     url: "https://dummyimage.com/640x480/000/fff",
    //     title: "Dummy 2",
    //     size: "5 KB", 
    //     group: "Group-1", 
    //     tags: "Dummy, Image"
    //   },
    //   { 
    //     url: "https://dummyimage.com/640x480/000/fff",
    //     title: "Dummy 3",
    //     size: "5 KB", 
    //     group: "Group-1", 
    //     tags: "Dummy, Image"
    //   }
    // ];

    // setTimeout(() => {
    //   this.setState({ 
    //     loaded: true, 
    //     images: response
    //   });
    // }, 1000);
    // /**********************************/
  }

  componentWillReceiveProps(props) {
    if (props.search === this.props.search &&
      props.filter === this.props.filter) {
      return;
    }

    /**********************************
    ** Simulate Request (Fetch Images) 
    **********************************/
    this.setState({ loaded: false });

    setTimeout(() => {
      this.setState({ loaded: true });
    }, 1000);
    /**********************************/
  }

  onShow(event, image) {
    this.setState({ sample: image });
  }

  onDelete(event, image) {
    /**********************************
    ** Simulate Request (Remove Image and Fetch Images) 
    **********************************/
    // this.setState({ loaded: false });

    // setTimeout(() => {
    //   this.setState({
    //     loaded: true,
    //     images: this.state.images.filter(value => value != image)
    //   });
    // }, 1000);
    /**********************************/
  }

  showMoreImages() {
    const { numberOfImages } = this.state;
    this.setState({ numberOfImages: numberOfImages + 100 });
  }

  render() {
    const { intl: { formatMessage }, images } = this.props;
    const { numberOfImages } = this.state;

    const isPossibleToShowMore = images.length > numberOfImages;
    const showMoreMarkup = isPossibleToShowMore ? (
      <div className={styles.moreContainer}>
        <button
          onClick={this.showMoreImages}
          className={'mdl-button mdl-js-button mdl-button--raised mdl-button--colored'}
        >
          {formatMessage(globalMessages.loadMore)}
        </button>
      </div>
    ) : null;


    return (this.state.loaded) ? (
      <div className={styles.horizontal}>
        {
          this.state.sample &&
          (
            <div
              className={styles.sample}
              onClick={(event) => this.setState({ sample: null })}
            >
              <div className={styles.parent}>
                <img
                  className={styles.image}
                  src={this.state.sample.url}
                  title={this.state.sample.title}
                />
              </div>
            </div>
          )
        }
        <table className={`mdl-data-table mdl-js-data-table ${styles.horizontal}`}>
          <thead>
            <tr>
              <th className={`mdl-data-table__cell--non-numeric ${styles.narrow}`}>
                {formatMessage(globalMessages.show)}
              </th>
              <th className={'mdl-data-table__cell--non-numeric'}>
                {formatMessage(globalMessages.title)}
              </th>
              <th>
                {formatMessage(globalMessages.size)}
              </th>
              <th className={'mdl-data-table__cell--non-numeric'}>
                {formatMessage(globalMessages.group)}
              </th>
              <th className={'mdl-data-table__cell--non-numeric'}>
                {formatMessage(globalMessages.tags)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric ${styles.narrow}`}>
                {formatMessage(globalMessages.actions)}
              </th>
            </tr>
          </thead>
          <tbody>
            {
              images && images.length && images.slice(0, numberOfImages).map((image, index) => (
                <tr key={index}>
                  {/* <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                    <i
                      className={`material-icons ${styles.show}`}
                      onClick={(event) => this.onShow(event, image)}
                    >
                      image
                      </i>
                  </td> */}
                  <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                    <img
                      alt={image.title || 'image'}
                      style={{ height: 30, border: '1px solid lightgrey', cursor: 'pointer' }}
                      src={image.url}
                      onClick={(event) => this.onShow(event, image)}
                    />
                  </td>
                  <td className={'mdl-data-table__cell--non-numeric'}>
                    {image.title || 'No title'}
                  </td>
                  <td>
                    {image.size || 'N/A'}
                  </td>
                  <td className={'mdl-data-table__cell--non-numeric'}>
                    {image.group || ' No group'}
                  </td>
                  <td className={'mdl-data-table__cell--non-numeric'} style={{ maxWidth: 100, overflowX: 'hidden', whiteSpace: 'normal' }}>
                    {/* {image.tags && image.tags.map(tag => tag.name + ' ')} */}
                  </td>
                  <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                    <i
                      className={`material-icons ${styles.delete}`}
                      onClick={(event) => this.onDelete(event, image)}
                    >
                      delete
                       </i>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
        {showMoreMarkup}
      </div>
    ) : <Spinner />;
  }
}

function mapStateToProps(state) {
  return {
    userId: state.User.currentUser.id,
    userRole: state.User.currentUser.role,
    userLocale: state.User.currentUser.locale,
    images: state.User.savedImages,
  };
}

export default connect(mapStateToProps, { getImages })(injectIntl(ImageTable));
