import React, { Component } from 'react';
import PropTypes from 'prop-types';

//styles
import styles from './sidebarSection.css';

class SidebarSection extends Component {
  static propTypes = {
    filters: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    modify: PropTypes.func.isRequired,
    defaultValue: PropTypes.string
  };

  constructor(props) {
    super(props);

    this.renderContent = this.renderContent.bind(this);
    this.renderCheckbox = this.renderCheckbox.bind(this);
    this.renderRadio = this.renderRadio.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate() {
    window.componentHandler.upgradeDom();
  }

  handleChange(e) {
    console.log(e.target.value, e.target.checked);
    this.props.modify(e.target.value, e.target.checked);
  }

  renderCheckbox(el, index) {
    const { filters } = this.props;
    const { label, value } = el;
    const id = `${index}-${value}`;
    const checkboxStyles = {
      display: filters[value] > 0 ? 'inline-block' : 'none'
    };

    return (
      <label
        className={`mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect is-upgraded ${styles.inputWrapper}`}
        htmlFor={id}
        key={index}
        style={checkboxStyles}
      >
        <input
          type="checkbox"
          id={id}
          className="mdl-checkbox__input"
          value={value}
          onChange={(e) => this.handleChange(e)}
        />
        <span className={`mdl-checkbox__label ${styles.label}`}>{label}</span>
        <span className={styles.exercisesAmount}>{filters[value]}</span>
      </label>
    );
  }

  renderRadio(el, index) {
    const { defaultValue } = this.props;
    const { label, value, name } = el;

    const id = `${index}-${value}`;
    return (
      <label
        className={`mdl-radio mdl-js-radio mdl-js-ripple-effect is-upgraded ${styles.inputWrapper} ${value === defaultValue ? 'is-checked' : ''}`}
        htmlFor={id}
        key={index}
      >
        <input
          type="radio"
          id={id}
          className="mdl-radio__button"
          name={name}
          defaultChecked={value === defaultValue}
          value={value}
          checked={value === defaultValue}
          onChange={(e) => this.handleChange(e)}
        />
        <span className={`mdl-radio__label ${styles.label}`}>{label}</span>
      </label>
    );
  }

  renderContent(type, options) {
    switch (type) {
      case 'checkbox':
        return options.map(this.renderCheckbox);
      case 'radio':
        return options.map(this.renderRadio);
      default:
        return options.map(this.renderRadio);
    }
  }

  render() {
    const { title, type, options, userRole, id } = this.props;
    // console.log('SidebarSection', this.props);

    if (userRole === 1 && id === 'access') {
      return null;
    }

    return (
      <div className={styles.section}>
        <h4 className={styles.title}>{title}</h4>
        {this.renderContent(type, options)}
      </div>
    );
  }
}

export default SidebarSection;
