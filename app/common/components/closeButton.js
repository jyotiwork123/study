import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CloseButton extends Component {
  static propTypes = {
    text: PropTypes.string,
    mainClassName: PropTypes.string,
    iconClassName: PropTypes.string,
    handleClose: PropTypes.func.isRequired,
  };

  static defaultProps = {
    text: '',
    mainClassName: '',
    iconClassName: ''
  };

  componentDidMount() {
    componentHandler.upgradeDom();
  }

  render() {
    const { text, mainClassName, iconClassName, handleClose } = this.props;

    return (
      <span className={mainClassName} onClick={handleClose}>
        {text}
        <i className={`material-icons ${iconClassName}`}>close</i>
      </span>
    );
  }
}

export default CloseButton;
