import React, { Component } from 'react';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//redux
import { connect } from 'react-redux';

//utils
import {
  getAudios
} from '../../../user/state/actions';
import { getAudioQuery } from '../../../common/queries';

//components
import Spinner from '../spinner';

//styles, assets
import styles from './audioTable.css';

class AudioTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      audios: null,
      sample: new Audio(),
    };
  }

  componentDidMount() {

    this.props.getAudios(getAudioQuery(this.props.userLocale))
      .then(response =>
        this.setState({
          loaded: true,
        })
      );
    /**********************************
    ** Simulate Request (Fetch Audios) 
    **********************************/
    // const response = [
    //   {
    //     url: "https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_700KB.mp3",
    //     title: "Dummy 1",
    //     duration: "00:12",
    //     group: "Group-1",
    //     language: "English",
    //     tags: "Dummy, Audio"
    //   },
    //   {
    //     url: "https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_5MG.mp3",
    //     title: "Dummy 2",
    //     duration: "00:46",
    //     group: "Group-1",
    //     language: "English",
    //     tags: "Dummy, Audio"
    //   }
    // ];

    // setTimeout(() => {
    //   this.setState({
    //     loaded: true,
    //     audios: response
    //   });
    // }, 1000);
    // /**********************************/

  }

  componentWillReceiveProps(props) {
    if (props.search === this.props.search &&
      props.filter === this.props.filter) {
      return;
    }

    /**********************************
    ** Simulate Request (Fetch Images) 
    **********************************/
    this.setState({ loaded: false });

    setTimeout(() => {
      this.setState({ loaded: true });
    }, 1000);
    /**********************************/
  }

  onPlay(event, audio) {
    const { audios, sample } = this.state;
    const { savedAudios } = this.props;

    if (sample == audio) {
      event.target.innerHTML = "play_arrow";
      audio.target = null;

      this.setState({ sample: null });
    } else {
      if (sample && sample.target) {
        sample.target.innerHTML = "play_arrow";
      }

      event.target.innerHTML = "stop";
      audio.target = event.target;

      this.setState({ sample: audio });
    }
  }

  onDelete(event, audio) {
    /**********************************
    ** Simulate Request (Remove Image and Fetch Images) 
    **********************************/
    this.setState({ loaded: false, sample: null });

    setTimeout(() => {
      this.setState({
        loaded: true,
        audios: this.state.audios.filter(value => value != audio)
      });
    }, 1000);
    /**********************************/
  }

  render() {
    const { search, filter, intl: { formatMessage }, savedAudios } = this.props;

    return (this.state.loaded) ? (
      <div className={styles.horizontal}>
        {
          this.state.sample &&
          (
            <audio className={styles.sample}
              tabIndex={`-1`}
              autoPlay={true}
              src={this.state.sample.url}
              name={this.state.sample.title}
              loop={false}
              controls={false}
              id={`audio-player`}>
              {formatMessage(globalMessages.browserNotSupportFile)}
            </audio>
          )
        }
        <table className={`mdl-data-table mdl-js-data-table ${styles.horizontal}`}>
          <thead>
            <tr>
              <th className={`mdl-data-table__cell--non-numeric ${styles.narrow}`}>
                {formatMessage(globalMessages.play)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric`}>
                {formatMessage(globalMessages.title)}
              </th>
              <th>
                {formatMessage(globalMessages.duration)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric`}>
                {formatMessage(globalMessages.group)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric`}>
                {formatMessage(globalMessages.language)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric`}>
                {formatMessage(globalMessages.tags)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric ${styles.narrow}`}>
                {formatMessage(globalMessages.actions)}
              </th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.audios && this.state.audios.length && this.state.audios.map((value, index) => {
                return (
                  <tr key={index}>
                    <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                      <i className={`material-icons ${styles.play}`}
                        onClick={(event) => this.onPlay(event, value)}>
                        play_arrow
                      </i>
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {value.title}
                    </td>
                    <td>
                      {value.duration}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {value.group}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {value.language}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {value.tags}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                      <i
                        className={`material-icons ${styles.delete}`}
                        onClick={(event) => this.onDelete(event, value)}>
                        delete
                      </i>
                    </td>
                  </tr>
                );
              })
            }
          </tbody>

          <tbody>
            {
              savedAudios && savedAudios.length && savedAudios.map((audio, index) => {
                return (
                  <tr key={index}>
                    <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                      <i
                        className={`material-icons ${styles.play}`}
                        onClick={(event) => this.onPlay(event, audio)}
                      >
                        play_arrow
                      </i>
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {audio.title}
                    </td>
                    <td>
                      {audio.duration}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {audio.group}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {audio.locale}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {audio.tags}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                      <i
                        className={`material-icons ${styles.delete}`}
                        onClick={(event) => this.onDelete(event, value)}
                      >
                        delete
                      </i>
                    </td>
                  </tr>
                );
              })
            }
          </tbody>

        </table>
      </div>
    ) : <Spinner />;
  }
}

function mapStateToProps(state) {
  return {
    userId: state.User.currentUser.id,
    userRole: state.User.currentUser.role,
    userLocale: state.User.currentUser.locale,
    savedAudios: state.User.savedAudios,
  };
}

export default connect(mapStateToProps, { getAudios })(injectIntl(AudioTable));
