import React, { Component } from 'react';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//styles
import styles from './metronome.css';


class Metronome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bpm: 90,
            volume: 0.5,
            playing: false,
            count: 0,
        };
        this.click1 = new Audio('https://daveceddia.com/freebies/react-metronome/click1.wav');
        this.click2 = new Audio('https://daveceddia.com/freebies/react-metronome/click1.wav');
        this.handleBPM = this.handleBPM.bind(this);
        this.handleVolume = this.handleVolume.bind(this);
        this.updateInterval = this.updateInterval.bind(this);
        this.startStop = this.startStop.bind(this);
        this.stop = this.stop.bind(this);
        this.playClick = this.playClick.bind(this);
    }

    componentWillUnmount() {
        this.stop();
    }

    updateInterval() {
        const bmpSpeed = 60 * 1000 / this.state.bpm;
        this.timer = setInterval(this.playClick, bmpSpeed);
    }

    handleBPM(event) {
        const bpm = event.target.value;
        if (this.state.playing) {
            clearInterval(this.timer);
            this.updateInterval();
            this.setState({
                count: 0,
                bpm
            });
        } else {
            this.setState({
                bpm
            });
        }
    }

    handleVolume(event) {
        const volume = event.target.value;
        // if (this.state.playing) {
        //     clearInterval(this.timer);
        //     this.updateInterval();
        //     this.setState({
        //         count: 0,
        //         volume
        //     });
        // } else {
        this.setState({
            volume: volume / 100
        });
        // }
    }

    playClick() {
        const { volume } = this.state;
        // console.log(volume);
        if (this.props.audible) {
            if (this.state.count === 0) {
                this.click2.volume = volume;
                this.click2.play();
            } else {
                this.click1.volume = volume;
                this.click1.play();
            }
            this.setState({
                count: this.state.count + 1
            });
        } else {
            this.setState({
                count: this.state.count + 1
            });
        }
    }

    startStop() {
        this.props.togglePractice();
        if (this.state.playing) {
            clearInterval(this.timer);
            this.setState({
                playing: false
            });
        } else {
            this.updateInterval();
            this.setState({
                count: 0,
                playing: true
            }, this.playClick);
        }
    }

    stop() {
        clearInterval(this.timer);
        this.setState({
            playing: false
        });
    }

    render() {
        const { audible, visual, playingSample } = this.props;
        const { count, playing } = this.state;
        const { formatMessage } = this.props.intl;

        return (
            <div className={`mdl-cell mdl-cell--12-col ${styles.container}`}>
                <div className={'mdl-cell mdl-cell--6-col'} style={{ textAlign: 'center' }}>
                    <button
                        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}
                        onClick={this.startStop}
                        disabled={playingSample}
                    >
                        {this.state.playing ? formatMessage(globalMessages.check) : formatMessage(globalMessages.startPractice)}
                    </button>
                </div>
                <div
                    className={'mdl-cell mdl-cell--6-col'}
                >
                    <div id='bpm-slider' style={{ width: '100%', textAlign: 'center' }}>


                        <div className={`mdl-cell mdl-cell--12-col ${styles.inputContainer}`}>
                            <div style={{ textAlign: 'center', width: 200 }}>Tempo</div>
                            <input
                                type='range'
                                min='30'
                                max='150'
                                value={this.state.bpm} onChange={this.handleBPM}
                                className={styles.slider}
                            // disabled={!audible}
                            />
                            <div style={{ textAlign: 'center', width: 200 }}>{this.state.bpm} BPM</div>
                        </div>
                        <div style={{ textAlign: 'justify' }} className={`mdl-cell mdl-cell--12-col ${styles.inputContainer}`}>
                            <span style={{ opacity: playing && visual && count % 2 === 1 ? 1 : 0 }} ><i className={'material-icons'}>stop</i></span>
                            <span style={{ opacity: playing && visual && count % 2 === 0 ? 1 : 0 }} ><i className={'material-icons'}>stop</i></span>
                        </div>
                        <div className={`mdl-cell mdl-cell--12-col ${styles.inputContainer}`}>
                            <div style={{ textAlign: 'center', width: 200 }}>{formatMessage(globalMessages.volume)}</div>
                            <input
                                type='range'
                                min='0'
                                max='100'
                                value={this.state.volume * 100} onChange={this.handleVolume}
                                className={styles.slider}
                            // disabled={!audible}
                            />
                            <div style={{ textAlign: 'center', width: 200 }}>{parseInt((this.state.volume * 100), 0)} %</div>
                        </div>
                    </div>
                    {/* <div style={{ textAlign: 'center' }}>{this.state.bpm} BPM</div> */}
                </div>
            </div>

        );
    }
}

export default injectIntl(Metronome);
