import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import CloseButton from '../closeButton';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//styles
import styles from './topBar.css';

class TopBar extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      showBar: true
    };

    this.closeBar = this.closeBar.bind(this);
  }

  componentWillMount() {
    const showBar = window.localStorage.getItem('slt2TopBarState');

    if (showBar !== null && !!showBar) {
      this.setState({ showBar: false });
    }
  }

  closeBar() {
    this.setState({ showBar: false });
    window.localStorage.setItem('slt2TopBarState', false);
  }

  render() {
    const { title, subtitle, intl: { formatMessage } } = this.props;

    if (!this.state.showBar) {
      return <div className={styles.plug} />;
    }
    return (
      <div className={`mdl-cell mdl-cell--12-col mdl-shadow--4dp ${styles.topBar}`}>
        <CloseButton
          mainClassName={styles.closeWrapper}
          iconClassName={styles.closeIcon}
          text={formatMessage(globalMessages.close)}
          handleClose={this.closeBar}
        />
        <div className={styles.title}>
          { title }
        </div>
        <div className={styles.subtitle}>
          {subtitle}
        </div>
      </div>
    );
  }
}

export default injectIntl(TopBar);
