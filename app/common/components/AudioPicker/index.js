import React, { Component } from 'react';
import Modal from 'react-modal';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//styles, assets
import styles from './audioPicker.css';

class AudioPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      file: null,
      audio: null,
      duration: '',
      title: '',
      group: '',
      language: '',
      size: 0,
      tags: [],
    };
  }


  onDragStart(event) {
    event.preventDefault();
  }

  onDragEnd(event) {
    event.preventDefault();
  }

  onDragOver(event) {
    event.preventDefault();
  }

  onDragEnter(event) {
    const dropzone = document.getElementById('audio-dropzone');

    dropzone.style.background = '#e1ffe1';
    dropzone.style.border = '2px dashed #afc4ff';

    event.preventDefault();
  }

  onDragLeave(event) {
    const dropzone = document.getElementById('audio-dropzone');

    dropzone.style.background = '#d6d9e0';
    dropzone.style.border = '2px solid #afc4ff';

    event.preventDefault();
  }

  onDrag(event) {
    event.preventDefault();
  }

  onDrop(event) {
    if (event.dataTransfer.items && event.dataTransfer.items[0]) {
      const item = event.dataTransfer.items[0];

      if (item.kind === 'file' && item.type.match(/^(audio\/)/g)) {
        const holder = document.getElementById('audio-holder');
        const picker = document.getElementById('audio-picker');
        const reader = new FileReader();

        holder.style.display = 'none';
        picker.style.display = 'block';

        const file = item.getAsFile();
        console.log('file', file);
        this.setState({ file });

        reader.onload = (event) => {
          picker.src = event.target.result;
          let duration = null;

          picker.addEventListener('loadedmetadata', () => {
            // Obtain the duration in seconds of the audio file (with milliseconds as well, a float value)
            let minutes = parseInt((picker.duration / 60), 0);
            let seconds = parseInt((picker.duration % 60), 0);
            if (minutes < 10) { minutes = '0' + minutes; }
            if (seconds < 10) { seconds = '0' + seconds; }
            duration = minutes + ':' + seconds;
            // console.log('picker', picker);
            console.log('duration on drop', duration);
            this.setState({ duration });

          }, false);

          picker.title = file.name;
          picker.name = file.name;
          picker.type = file.type;

            console.log('duration', duration);

          this.setState({ audio: event.target.result, file, size: file.size, fileName: file.name, fileType: file.type });
        };

        reader.readAsDataURL(file);
      } else {
        const { intl: { formatMessage } } = this.props;

        alert(formatMessage(globalMessages.wrongFileType));
      }
    }

    const dropzone = document.getElementById('audio-dropzone');

    dropzone.style.background = '#d6d9e0';
    dropzone.style.border = '2px solid #afc4ff';

    event.preventDefault();
  }

  onClick(event) {
    const dialog = document.createElement('input');
    const holder = document.getElementById('audio-holder');
    const picker = document.getElementById('audio-picker');
    const reader = new FileReader();

    dialog.name = 'audio';
    dialog.type = 'file';
    dialog.accept = 'audio/*';

    dialog.onchange = (event) => {
      if (event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        console.log('file2', file);

        this.setState({ file });


        if (file.type.match('audio.*')) {
          holder.style.display = 'none';
          picker.style.display = 'block';

          reader.onload = (event) => {
            picker.src = event.target.result;
            let duration = null;

            picker.addEventListener('loadedmetadata', () => {
              // Obtain the duration in seconds of the audio file (with milliseconds as well, a float value)
              let minutes = parseInt((picker.duration / 60), 0);
              let seconds = parseInt((picker.duration % 60), 0);
              if (minutes < 10) { minutes = '0' + minutes; }
              if (seconds < 10) { seconds = '0' + seconds; }
              duration = minutes + ':' + seconds;
              // console.log('picker', picker);
              console.log('duration', duration);
              this.setState({ duration });
            }, false);

            picker.title = file.name;
            picker.name = file.name;
            picker.type = file.type;


            this.setState({ audio: event.target.result, file, size: file.size, fileName: file.name, fileType: file.type });
          };

          reader.readAsDataURL(file);
        } else {
          const { intl: { formatMessage } } = this.props;

          alert(formatMessage(globalMessages.wrongFileType));
        }
      }
    };

    dialog.click();

    event.preventDefault();
  }

  onUpload(event, callback) {
    const { audio, file, fileName, fileType, title, group, language, size, duration, tags } = this.state;
    const { intl: { formatMessage }, groups } = this.props;

    if (!audio || audio.length === 0) {
      alert(formatMessage(globalMessages.pleaseSelectFile));
      return;
    }

    if (!title || this.state.title.length === 0) {
      alert(formatMessage(globalMessages.pleaseEnterTitleForFile));
      return;
    }

    // if (!group || group.length === 0) {
    //   alert(formatMessage(globalMessages.pleaseEnterGroupForFile));
    //   return;
    // }

    // if (group && groups.filter(entry => entry.name === group).length === 0) {
    //   alert(formatMessage(globalMessages.pleaseEnterGroupForFile));
    //   return;
    // }

    if (!language || language.length === 0) {
      alert(formatMessage(globalMessages.pleaseEnterLanguageForFile));
      return;
    }

    // if (!tags || tags.length === 0) {
    //   alert(formatMessage(globalMessages.pleaseEnterTagsForFile));
    //   return;
    // }


    callback(event, { audio, file, fileName, fileType, title, language, group, tags, size, duration });

    this.setState({ audio: null, file: null, title: '', group: '', size: 0, duration: '', tags: '' });

  }

  onCancel(event, callback) {
    const { audio, title, group, language, size, tags } = this.state;

    callback(event, { audio, title, group, language, size, tags });
  }

  onSelectChange(event, dataSet) {
    // if (dataSet.filter(dataEntry => {
    //   const name = dataEntry.name.toLowerCase();
    //   const value = event.target.value.toLowerCase();
    //   return (name.includes(value));
    // }).length > 0) {
    this.setState({ group: event.target.value });
    // } else {
    //   return;
    // }
  }

  addTag(addedTags) {
    console.log(addedTags);

    const tags = [];

    addedTags.split(',').forEach(tag => {
      // const newTag = { title: '' };
      // newTag.title = tag;
      // tags.push(newTag);
      tags.push(tag);
    }
    );

    console.log(tags);

    this.setState({ tags });
  }


  render() {
    const { isOpen, onUpload, onCancel, intl: { formatMessage }, groups, tags } = this.props;

    console.log(this.state);

    return (
      <Modal
        className={styles.modal}
        overlayClassName={styles.overlay}
        isOpen={isOpen}
        onRequestClose={(event) => this.onCancel(event, onCancel)}
        contentLabel={'UploadAudio'}
        shouldCloseOnOverlayClick
      >
        <div className={`mdl-grid ${styles.header}`}>
          <div className={'mdl-cell mdl-cell--12-col mdl-cell--16-col--tablet'}>
            <h3 className={styles.title}>
              {formatMessage(globalMessages.uploadAudio)}
            </h3>
          </div>
        </div>
        <div className={`mdl-grid ${styles.middle}`}>
          <div className={`mdl-cell mdl-cell--12-col mdl-cell--16-col--tablet ${styles.inner}`}>
            <div
              className={styles.dropzone}
              id={'audio-dropzone'}
              onDragStart={(event) => this.onDragStart(event)}
              onDragEnd={(event) => this.onDragEnd(event)}
              onDragOver={(event) => this.onDragOver(event)}
              onDragEnter={(event) => this.onDragEnter(event)}
              onDragLeave={(event) => this.onDragLeave(event)}
              onDrag={(event) => this.onDrag(event)}
              onDrop={(event) => this.onDrop(event)}
              onClick={(event) => this.onClick(event)}
            >
              <div className={styles.holder} id={'audio-holder'}>
                <i className={`material-icons ${styles.symbol}`}> cloud_upload </i>
                <p className={styles.instruction}>
                  {formatMessage(globalMessages.clickOrDropFile)}
                </p>
              </div>
              <audio
                className={styles.picker}
                tabIndex={'-1'}
                autoPlay={false}
                loop={false}
                controls
                id={'audio-picker'}
              >
                {formatMessage(globalMessages.browserNotSupportFile)}
              </audio>
            </div>
            <div className={styles.control}>
              <div className={`mdl-textfield mdl-js-textfield ${styles.input}`}>
                <p className={styles.label}> {formatMessage(globalMessages.title)} </p>
                <input
                  className={'mdl-textfield__input'}
                  type={'text'}
                  name={'text-field'}
                  id={'text-title'}
                  onChange={(event) => this.setState({ title: event.target.value })}
                />
              </div>
              {/* <div className={`mdl-textfield mdl-js-textfield ${styles.input}`}>
                <p className={styles.label}> {formatMessage(globalMessages.group)} </p>
                <input
                  className={'mdl-textfield__input'}
                  list={'groups'}
                  type={'text'}
                  name={'text-field'}
                  id={'text-group'}
                  value={this.state.group}
                  onChange={(event) => this.onSelectChange(event, groups)}
                />
                <datalist id='groups'>
                  {
                    groups.map(group => <option key={group._id} id={group._id}>{group.name}</option>)
                  }
                </datalist>
              </div> */}
              <div className={`mdl-textfield mdl-js-textfield ${styles.input}`}>
                <p className={styles.label}> {formatMessage(globalMessages.language)} </p>
                {
                  // TODO change to regular select later
                }
                <input
                  className={'mdl-textfield__input'}
                  list={'languages'}
                  type={'text'}
                  name={'text-field'}
                  id={'text-language'}
                  onChange={(event) => this.setState({ language: event.target.value.replace('Estonian', 'ee').replace('English', 'en').replace('Japanese', 'jp') })}
                />
                <datalist id='languages'>
                  <option>Estonian</option>
                  <option>English</option>
                  <option>Japanese</option>
                </datalist>
              </div>
              {/* <div className={`mdl-textfield mdl-js-textfield ${styles.input}`}>
                <p className={styles.label}> {formatMessage(globalMessages.tags)} </p>
                <input
                  list={'tags'}
                  className={'mdl-textfield__input'}
                  type={'text'}
                  name={'text-field'}
                  id={'text-tags'}
                  onKeyUp={(event) => this.addTag(event.target.value)}
                // onChange={(event) => this.addTag(event.target.value)}

                // onChange={(event) => this.setState({ tags: event.target.value })}
                />
                <datalist id='tags'>
                  {
                    tags.map(tag => <option key={tag._id} id={tag._id}>{tag.name}</option>)
                  }
                </datalist>
              </div> */}
            </div>
          </div>
        </div>
        <div className={`mdl-grid ${styles.bottom}`}>
          <div className={'mdl-cell mdl-cell--12-col mdl-cell--16-col--tablet'}>
            <button className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`} onClick={(event) => this.onUpload(event, onUpload)}>
              {formatMessage(globalMessages.upload)}
            </button>
            <button className={'mdl-button mdl-js-button mdl-button--raised'} onClick={(event) => this.onCancel(event, onCancel)}>
              {formatMessage(globalMessages.cancel)}
            </button>
          </div>
        </div>
      </Modal>
    );
  }
}

export default injectIntl(AudioPicker);
