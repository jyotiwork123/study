import React, { Component } from 'react';
import Modal from 'react-modal';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//redux
import { connect } from 'react-redux';
import { editTutor } from '../../../patient/state/actions';

//components

//styles
import styles from './tutorDataDialog.css';

class TutorDataDialog extends Component {

    constructor(props) {
        super(props);

        this.state = {
            position: 'teacher',
            firstName: '',
            lastName: '',
            // email: '',
            phone: ''
        };

        this.renderPosition = this.renderPosition.bind(this);
    }

    componentWillMount() {
    }

    componentDidMount() {
        window.componentHandler.upgradeDom();
    }

    onUpload(event, callback) {
        const { currentUserId } = this.props;
        const { position, firstName, lastName, phone } = this.state;
        this.setState({
            position: 'teacher',
            firstName: '',
            lastName: '',
            // email: '',
            phone: ''
        });
        callback(event, { currentUserId, position, firstName, lastName, phone });
    }

    onCancel(event, callback) {

        const { birthYear, gender, hobbies } = this.state;


        this.setState({
            position: '',
            firstName: '',
            lastName: '',
            // email: '',
            phone: ''
        });

        callback(event, { birthYear, gender, hobbies });
    }

    handleChange(e) {
        // console.log(e.target.name)
        this.setState({ position: e.target.name });
    }

    renderPosition(el, index) {
        const { defaultValue } = 'teacher';
        const { label, value, name } = el;
        const id = `${index}-${value}`;
        return (
            <div
                className={`mdl-radio mdl-js-radio mdl-js-ripple-effect is-upgraded ${styles.inputWrapper} ${name === this.state.position ? styles.selected : ''}`}
                key={index}
            >
                <input
                    type="radio"
                    id={id}
                    className="mdl-radio__button"
                    name={name}
                    defaultChecked={value === defaultValue}
                    value={value}
                    checked={value === defaultValue}
                    onChange={(e) => { this.handleChange(e); }}
                />
                <label htmlFor={id} className={`mdl-radio__label ${styles.label}`}>{label}</label>
            </div>
        );
    }

    render() {
        const { isOpen, user, onCancel, onUpload, intl: { formatMessage } } = this.props;

        const { position, institution, firstName, lastName, phone } = this.state;

        const options = [
            {
                label: `${formatMessage(globalMessages.teacher)}`,
                value: 'teacher',
                name: 'teacher',
            },
            {
                label: `${formatMessage(globalMessages.speechTherapist)}`,
                value: 'speech therapist',
                name: 'speech therapist',
            },
            {
                label: `${formatMessage(globalMessages.specialTeacher)}`,
                value: 'special education teacher',
                name: 'special education teacher',
            },
            {
                label: `${formatMessage(globalMessages.parent)}`,
                value: 'parent',
                name: 'parent',
            },
            {
                label: `${formatMessage(globalMessages.other)}`,
                value: 'other',
                name: 'other',
            }
        ]

        // console.log(user)

        return (
            <Modal
                className={styles.wrapper}
                overlayClassName={styles.overlay}
                isOpen={isOpen}
                onRequestClose={(event) => this.onCancel(event, onCancel)}
                contentLabel={'EditUser'}
                shouldCloseOnOverlayClick
            >
                <div className={styles.header}>{formatMessage(globalMessages.editTutor)}
                </div>
                <div className={styles.content}>
                    {/* <div className={styles.name}>
                        <h3>{`${user.firstname} ${user.lastname}`}</h3>
                    </div> */}
                    <div className={styles.leftSection}>
                        <div className='mdl-textfield mdl-js-textfield mdl-textfield--full-width'>
                            <span className={`${styles.modalLabel}`} style={{ color: 'rgba(0,0,0, 0.26)' }}>{`${formatMessage(globalMessages.position)}: `}</span>
                        </div>
                        <div className={styles.section}>
                            {
                                options.map(this.renderPosition)
                            }
                        </div>
                        {/* <div className='mdl-textfield mdl-js-textfield mdl-textfield--full-width'>
                            <span className={`${styles.modalLabel}`} style={{ color: 'rgba(0,0,0, 0.26)' }}>{`${formatMessage(globalMessages.institution)}: `}</span><br /><br />
                            <span
                                className={`${styles.modalLabel} ${styles.selected}`}
                            >{institution || 'N/A'}</span>
                        </div> */}
                    </div>
                    <div className={styles.rightSection}>
                        <div>
                            <div className={`mdl-textfield mdl-js-textfield mdl-textfield--full-width`}>
                                <input
                                    className={`mdl-textfield__input ${styles.input}`}
                                    id='firstName'
                                    value={firstName}
                                    onChange={(e) => {
                                        this.setState({ firstName: e.target.value });
                                    }}
                                />
                                <label htmlFor='firstName' className={`mdl-textfield__label ${styles.modalLabel}`}>{formatMessage(globalMessages.firstName)}</label>
                            </div>
                            <div className={`mdl-textfield mdl-js-textfield mdl-textfield--full-width`}>
                                <input
                                    className={`mdl-textfield__input ${styles.input}`}
                                    id='lastName'
                                    value={lastName}
                                    onChange={(e) => {
                                        this.setState({ lastName: e.target.value });
                                    }}
                                />
                                <label htmlFor='lastName' className={`mdl-textfield__label ${styles.modalLabel}`}>{formatMessage(globalMessages.lastName)}</label>
                            </div>
                            {/* <div className={`mdl-textfield mdl-js-textfield mdl-textfield--full-width`}>
                                <input
                                    className={`mdl-textfield__input ${styles.input}`}
                                    id='email'
                                    value={email}
                                    onChange={(e) => {
                                        this.setState({ email: e.target.value });
                                    }}
                                />
                                <label htmlFor='email' className={`mdl-textfield__label ${styles.modalLabel}`}>{formatMessage(globalMessages.enterEmail)}</label>
                            </div> */}
                            <div className={`mdl-textfield mdl-js-textfield mdl-textfield--full-width `}>
                                <input
                                    className={`mdl-textfield__input ${styles.input}`}
                                    id='phone'
                                    value={phone}
                                    onChange={(e) => {
                                        this.setState({ phone: e.target.value });
                                    }}
                                />
                                <label htmlFor='phone' className={`mdl-textfield__label ${styles.modalLabel}`}>{formatMessage(globalMessages.phone)}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`mdl-grid ${styles.bottom}`}>
                    <div className={'mdl-cell mdl-cell--12-col mdl-cell--16-col--tablet'}>
                        <button
                            className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}
                            onClick={(event) => this.onUpload(event, onUpload)}
                        >
                            {formatMessage(globalMessages.save)}
                        </button>
                        <button
                            className={'mdl-button mdl-js-button mdl-button--raised'}
                            onClick={(event) => this.onCancel(event, onCancel)}
                        >
                            {formatMessage(globalMessages.cancel)}
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        patients: state.Patient.patients,
        currentUserId: state.User.currentUser.id,
        loaded: state.Patient.patientsReceived
    };
}

export default connect(mapStateToProps, {
})(injectIntl(TutorDataDialog));

