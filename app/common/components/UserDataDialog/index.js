import React, { Component } from 'react';
import Modal from 'react-modal';

import axios from 'axios';
import { HOBBIES_URL, apiConfig } from '../../api';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//redux
import { connect } from 'react-redux';
import { editPatient } from '../../../patient/state/actions';
import { getHobbies } from '../../..//patient/state/actions';

//components

//styles
import styles from './userDataDialog.css';


class UserDataDialog extends Component {

    constructor(props) {
        super(props);

        this.state = {
            birthYear: '',
            gender: '',
            hobbies: '',
            hobbiesArray: []
        };

        this.handleHobbieInput = this.handleHobbieInput.bind(this);
    }

    componentWillMount() {
        const { user, getHobbies } = this.props;
        getHobbies(user.locale);
    }

    componentDidMount() {
        window.componentHandler.upgradeDom();
        const { user, hobbiesList, } = this.props;
        const fetchedHobbies = user.hobbies.map(hobbyId => (hobbiesList.filter(hobby => hobby._id === hobbyId))[0].title);
        this.setState({
            birthYear: user.birthyear || '',
            gender: user.gender || '',
            hobbiesArray: fetchedHobbies || [],
        });
    }

    onUpload(event, callback) {
        const { birthYear, gender, hobbiesArray } = this.state;
        const { patientId, user, hobbiesList, token } = this.props;
        const existingHobbies = [];
        const newHobbiesSendData = [];
        const newHobbies = [];

        // console.log('hobbiesList', hobbiesList);

        // const hobbiesArray = ['Raamatute lugemine', ]

        hobbiesArray.forEach(title => {
            if (hobbiesList.filter(hobby => hobby.title.toLowerCase() === title.toLowerCase()).length > 0) {
                existingHobbies.push(hobbiesList.filter(hobby => hobby.title.toLowerCase() === title.toLowerCase())[0]._id);
            } else {
                newHobbiesSendData.push({
                    title: title.charAt(0).toUpperCase() + title.slice(1),
                    locale: user.locale
                });
            }
        });

        // console.log('existingHobbies', existingHobbies);
        // console.log('newHobbiesSendData', newHobbiesSendData);

        if (newHobbiesSendData.length !== 0) {
            const fetchHobby = (hobby) => axios.post(`${HOBBIES_URL}`, hobby, apiConfig(token));
            const promiseArray = newHobbiesSendData.map(fetchHobby);

            Promise.all(promiseArray)
                .then((result) => {
                    result.forEach(newHobby => {
                        newHobbies.push(newHobby.data.hobbie._id);
                    });
                    // console.log('newHobby fetched', result);
                })
                .catch((err) => {
                    console.error(err);
                })
                .finally((result) => {                    
                    callback(event, { patientId, birthYear, gender, hobbiesArray: newHobbies.concat(existingHobbies) });
                });
        } else {
            // console.log('result', patientId, birthYear, gender, hobbiesArray: existingHobbies);
            callback(event, { patientId, birthYear, gender, hobbiesArray: existingHobbies });
        }
    }

    onCancel(event, callback) {
        const { birthYear, gender, hobbies } = this.state;

        this.setState({
            birthYear: '',
            gender: '',
            hobbies: '',
            hobbiesArray: []
        });
        callback(event, { birthYear, gender, hobbies });
    }

    handleBirthYearInput(event) {
        const value = event.target.value;
        const reg = /^\d+$/;
        // console.log('handleBirthYearInput', value, value.length <= 4, reg.test(value));

        if (value.length <= 4 && (reg.test(value) || value === '')) {
            this.setState({ birthYear: value });
        }
    }

    handleGenderSelect(event) {
        event.stopPropagation();
        // here below is temporary solution with replacing
        const value = event.target.value.replace('Mees', 'male').replace('Naine', 'female').replace('Ei avalda', 'nd');
        // console.log('handleGenderSelect', value.toLowerCase());
        this.setState({ gender: value.toLowerCase() });
    }

    handleHobbieInput(event) {
        const code = event.keyCode || event.which;
        const { value } = event.target;
        const { hobbiesArray } = this.state;

        if (code === 13 && !hobbiesArray.includes(value)) {
            hobbiesArray.push(value);
            this.setState({ hobbiesArray, hobbies: '' });
        }
    }

    handleHobbieSelect(event) {
        const { value } = event.target;
        const { hobbiesList } = this.props;
        const { hobbiesArray } = this.state;
        const existInList = hobbiesList.filter(hobby => hobby.title.toLowerCase() === value.toLowerCase()).length > 0;
        const added = hobbiesArray.filter(hobby => hobby.toLowerCase() === value.toLowerCase()).length > 0;
        if (existInList && !added) {
            hobbiesArray.push(value);
            this.setState({ hobbiesArray, hobbies: '' });
        } else {
            this.setState({ hobbies: value });
        }
        // existing && 
        // !hobbiesArray.includes(value)
        // ) {
        // hobbiesArray.push(value);
        // this.setState({ hobbiesArray });
        // }
    }

    removeHobby(e) {
        const { hobbiesArray } = this.state;
        const hobbyToRemove = e.target.getAttribute('name');

        // console.log('removeHobby', e.target.getAttribute('name'));
        this.setState({ hobbiesArray: hobbiesArray.filter(hobby => hobby !== hobbyToRemove) });
    }

    render() {
        const { isOpen, user, onCancel, onUpload, editPatient, intl: { formatMessage }, patientId, hobbiesList } = this.props;
        const { birthYear, gender, hobbies, hobbiesArray } = this.state;

        return (
            <Modal
                className={styles.wrapper}
                overlayClassName={styles.overlay}
                isOpen={isOpen}
                onRequestClose={(event) => this.onCancel(event, onCancel)}
                contentLabel={'EditUser'}
                shouldCloseOnOverlayClick
            >
                <div className={styles.header}>{formatMessage(globalMessages.editUser)}
                </div>
                <div className={styles.content}>
                    <div className={styles.leftSection}>
                        <div className='mdl-textfield mdl-js-textfield mdl-textfield--full-width'>
                            <p className={`${styles.basicInfoBlock}`}>
                                <span className={`${styles.modalLabel}`} style={{ color: 'rgba(0,0,0, 0.26)' }}>
                                    {`${formatMessage(globalMessages.user)}:  `}
                                </span>
                                <span style={{ marginLeft: 10 }} className={`${styles.modalLabel}`}>{`${user.firstname} ${user.lastname}`}</span>
                            </p>
                            <p className={`${styles.basicInfoBlock}`}>
                                <span className={`${styles.modalLabel}`} style={{ color: 'rgba(0,0,0, 0.26)', lineHeight: '45px' }}>
                                    {`${formatMessage(globalMessages.enterEmail)}:  `}
                                </span>
                                <span style={{ marginLeft: 10, lineHeight: '45px' }} className={`${styles.modalLabel}`}>{`${user.email}`}</span></p>
                            {
                                user.key && <p className={`${styles.basicInfoBlock}`}>
                                    <span className={`${styles.modalLabel}`} style={{ color: 'rgba(0,0,0, 0.26)' }}>
                                        {`${formatMessage(globalMessages.accessCode)}:  `}
                                    </span>
                                    <span style={{ marginLeft: 10 }} className={`${styles.modalLabel}`}>{`${user.key}`}</span>
                                </p>
                            }
                        </div>
                    </div>

                    <div className={styles.rightSection}>
                        {/* <div className={styles.leftSection}> */}
                        <div className='mdl-textfield mdl-js-textfield mdl-textfield--full-width'>
                            <input
                                className={`mdl-textfield__input ${styles.input}`}
                                id='birthYear'
                                value={birthYear}
                                onChange={(event) => { this.handleBirthYearInput(event); }}
                            />
                            <label htmlFor='birthYear' className={`mdl-textfield__label ${styles.modalLabel}`}>{formatMessage(globalMessages.enterBirthDate)}</label>
                        </div>
                        {/* </div> */}
                        {/* <div className={styles.rightSection}> */}
                        <div className='mdl-textfield mdl-js-textfield mdl-textfield--full-width'>
                            <input
                                className={`mdl-textfield__input ${styles.input}`}
                                list={'genders'}
                                id='gender'
                                value={globalMessages[gender] && formatMessage(globalMessages[gender])}
                                onChange={(event) => { this.handleGenderSelect(event); }}
                            />
                            <datalist id='genders'>
                                <option id='male'>{formatMessage(globalMessages.male)}</option>
                                <option id='female'>{formatMessage(globalMessages.female)}</option>
                                <option id='nd'>{formatMessage(globalMessages.nd)}</option>
                            </datalist>
                            <label htmlFor='gender' className={`mdl-textfield__label ${styles.modalLabel}`}>{formatMessage(globalMessages.enterGender)}</label>
                        </div>
                        {/* </div> */}
                        <div className='mdl-textfield mdl-js-textfield mdl-textfield--full-width'>

                            <input
                                className={`mdl-textfield__input ${styles.input}`}
                                tabIndex={0}
                                list={'hobbies'}
                                type={'text'}
                                name={'hobbies'}
                                id={'text-hobbies'}
                                value={hobbies}
                                // onKeyPress={this.enterPressed.bind(this)}
                                onKeyDown={this.handleHobbieInput}
                                // onKeyDown={(event) => { this.handleHobbieInput(event); }}
                                onChange={(event) => { this.handleHobbieSelect(event); }}
                            />
                            {<datalist id='hobbies'>
                                {
                                    hobbiesList.map(hobby => <option key={hobby._id} id={`${hobby._id}, `}>{hobby.title}</option>)
                                }
                            </datalist>
                            }
                            <label htmlFor='hobbies' className={`mdl-textfield__label ${styles.modalLabel}`}>
                                {formatMessage(globalMessages.enterHobbies)}

                            </label>
                            <div className={styles.hobbieCartWrapper}>
                                {
                                    hobbiesArray.map(hobby => <div className={styles.hobbieCart} key={hobby}>{`${hobby}`}
                                        <span
                                            name={hobby}
                                            className={styles.removeHobby}
                                            onClick={(e) => {
                                                this.removeHobby(e);
                                            }}
                                        >x</span>
                                    </div>)
                                }
                            </div>
                        </div>
                    </div>


                </div>
                <div className={`mdl-grid ${styles.bottom}`}>
                    <div className={'mdl-cell mdl-cell--12-col mdl-cell--16-col--tablet'}>
                        <button
                            className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}
                            disabled={birthYear === '' || gender === '' || hobbiesArray.length === 0}
                            onClick={(event) => this.onUpload(event, onUpload)}
                        >
                            {formatMessage(globalMessages.save)}
                        </button>
                        <button
                            className={'mdl-button mdl-js-button mdl-button--raised'}
                            onClick={(event) => this.onCancel(event, onCancel)}
                        >
                            {formatMessage(globalMessages.cancel)}
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        patientId: state.Patient.patientId,
        loaded: state.Patient.patientsReceived,
        hobbiesList: state.Patient.hobbiesList,
        token: state.User.currentUser.token
    };
}

export default connect(mapStateToProps, {
    getHobbies,
})(injectIntl(UserDataDialog));

