import React, {Component} from 'react';
import PropTypes from 'prop-types';

//components
import SidebarSection from '../SidebarSection';

//data
import {filterSettings} from '../../../exercises/data';

//styles
import styles from './sidebar.css';

class Sidebar extends Component {
  static propTypes = {
    filters: PropTypes.object.isRequired,
    modifySections: PropTypes.func.isRequired,
    modifyExercises: PropTypes.func.isRequired,
    modifyContent: PropTypes.func.isRequired,
    modifyAccess: PropTypes.func.isRequired,
    accessDefaultVal: PropTypes.string,
  };

  componentDidMount() {
    componentHandler.upgradeDom();
  }

  render() {
    const {
      modifySections,
      modifyExercises,
      modifyContent,
      modifyAccess,
      filters,
      locale,
      userRole,
      accessDefaultVal,
      contentDefaultVal,
    } = this.props;


    const handlers = {
      exercise: modifyContent,
      type: modifySections,
      languageDomain: modifyExercises,
      access: modifyAccess,
    };

    return (
        <div className={`mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet ${styles.sidebar}`}>
          <div className={`mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet ${styles.sidebarWrapper}`}>
            {filterSettings[locale].map((el, index) => {
              const defaultValue = el.id === 'access' ? accessDefaultVal : contentDefaultVal;
              // console.log('handler', handlers[el.id]);
              return (
                <SidebarSection
                  key={index}
                  type={el.type}
                  title={el.title}
                  options={el.options}
                  id={el.id}
                  modify={handlers[el.id] || modifySections}
                  defaultValue={defaultValue}
                  filters={filters}
                  userRole={userRole}
                />
              );
            })}
          </div>
        </div>
    );
  }
}

export default Sidebar;
