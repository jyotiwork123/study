import React, { Component } from 'react';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//components
import Spinner from '../spinner';

//styles, assets
import styles from './videoTable.css';

class VideoTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      videos: null,
      sample: null,
    };
  }

  componentDidMount() {
    /**********************************
    ** Simulate Request (Fetch Images) 
    **********************************/
    const response = [
      { 
        url: "https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4",
        title: "Dummy 1",
        duration: "00:05",
        group: "Group-1",
        language: "English",
        tags: "Dummy, Video"
      },
      { 
        url: "https://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_2mb.mp4",
        title: "Dummy 2",
        duration: "00:13",
        group: "Group-1",
        language: "English",
        tags: "Dummy, Video"
      }
    ];

    setTimeout(() => {
      this.setState({ 
        loaded: true, 
        videos: response
      });
    }, 1000);
    /**********************************/
  }

  componentWillReceiveProps(props) {
    if (props.search === this.props.search &&
        props.filter === this.props.filter) {
      return;
    }

    /**********************************
    ** Simulate Request (Fetch Images) 
    **********************************/
    this.setState({ loaded: false });
      
    setTimeout(() => {
      this.setState({ loaded: true });
    }, 1000);
    /**********************************/
  }

  onPlay(event, video) {
    this.setState({ sample: video });
  }
  
  onDelete(event, video) {
    /**********************************
    ** Simulate Request (Remove Image and Fetch Images) 
    **********************************/
    this.setState({ loaded: false });
        
    setTimeout(() => {
      this.setState({ 
        loaded: true, 
        videos: this.state.videos.filter(value => value != video)
      });
    }, 1000);
    /**********************************/
  }
  
  render() {
    const { search, filter, intl: { formatMessage } } = this.props;

    return (this.state.loaded) ? (
      <div className={styles.horizontal}>
        {
          this.state.sample &&
          (
            <div className={styles.sample}
              onClick={(event) => this.setState({ sample: null })}>
              <div className={styles.parent}>
              <video className={styles.video}
                onClick={(event) => event.preventDefault() }
                tabIndex={`-1`}
                autoPlay={false}
                src={this.state.sample.url}
                name={this.state.sample.title}
                loop={false}
                controls={true}
                id={`video-player`}>
                  {formatMessage(globalMessages.browserNotSupportFile)}
              </video>
              </div>
            </div>
          )
        }
        <table className={`mdl-data-table mdl-js-data-table ${styles.horizontal}`}>
          <thead>
            <tr>
              <th className={`mdl-data-table__cell--non-numeric ${styles.narrow}`}>
                {formatMessage(globalMessages.play)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric`}>
                {formatMessage(globalMessages.title)}
              </th>
              <th>
                {formatMessage(globalMessages.duration)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric`}>
                {formatMessage(globalMessages.group)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric`}>
                {formatMessage(globalMessages.language)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric`}>
                {formatMessage(globalMessages.tags)}
              </th>
              <th className={`mdl-data-table__cell--non-numeric ${styles.narrow}`}>
                {formatMessage(globalMessages.actions)}
              </th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.videos && this.state.videos.length && this.state.videos.map((value, index) => {
                return (
                  <tr key={index}>
                    <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                      <i className={`material-icons ${styles.play}`}
                        onClick={(event) => this.onPlay(event, value)}>
                          camera_roll
                      </i>
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {value.title}
                    </td>
                    <td>
                      {value.duration}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {value.group}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {value.language}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric`}>
                      {value.tags}
                    </td>
                    <td className={`mdl-data-table__cell--non-numeric ${styles.center}`}>
                      <i className={`material-icons ${styles.delete}`}
                        onClick={(event) => this.onDelete(event, value)}>
                          delete
                      </i>
                    </td>
                  </tr>
                );
              })
            }
          </tbody>
        </table>
      </div>
    ) : <Spinner />;
  }
}

export default injectIntl(VideoTable);