import React, { Component } from 'react';

//styles
import styles from './noExercises.css';


class NoExercises extends Component {
  render() {
    return (
      <div className={`mdl-shadow--4dp ${styles.topBar}`}>
        {this.props.children}
      </div>
    );
  }
}

export default NoExercises;
