import React, { Component } from 'react';

//redux
import { connect } from 'react-redux';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//utils
import {
  logout,
  getGroups,
  getTags,
  getImageUploadUrl,
  uploadFile,
  getImages,
  saveFileToDB
} from '../../../user/state/actions';
import { getQuery, getMutation, getImagesQuery } from '../../../common/queries';

//components
import HeaderTemplate from '../../components/HeaderTemplate';
import ImageTable from '../../components/ImageTable';
import ImagePicker from '../../components/ImagePicker';

//styles
import styles from './imageManager.css';

class ImageManager extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dialog: false,
      search: '',
      filter: 'title',
    };
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    // this.props.getGroups();
    // this.props.getTags(getQuery(null, 'tags'));

    // this.props.getImages(getImagesQuery(this.props.userLocale));


    // console.log(this.props);
  }


  setImage(event, data) {
    this.setState({ dialog: false });
    // console.log(this.convertBytes(data.size))

    let imageUrl = '';

    this.props.getImageUploadUrl(getMutation('getImageFileUploadUrl', data), data)
      .then(urls => {
        imageUrl = urls.url;
        this.props.uploadFile(urls.signed, data.file);
      })
      .then(uploadResult => {
        console.log('uploadResult', uploadResult);
        const payload = {
          url: imageUrl,
          title: data.title,
          size: this.convertBytes(data.size),
          id: data.file.name
        };

        console.log(uploadResult);
        console.log(payload);

        this.props.saveFileToDB(getMutation('createImage', payload), payload)
      });

    // console.log(data);
  }

  convertBytes(bytes) {
    const units = ['bytes', 'KB', 'MB', 'GB'];
    let l = 0;
    let n = parseInt(bytes, 10) || 0;
    while (n >= 1024 && ++l) {
      n /= 1024;
    }
    return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + units[l]);
  }

  render() {
    const { userId, userRole, token, logout, intl: { formatMessage }, groups, tags } = this.props;

    return (
      <div>
        <div className={`mdl-layout mdl-js-layout mdl-layout--fixed-header ${styles.background}`}>
          <HeaderTemplate
            title={formatMessage(globalMessages.images)}
            userId={userId}
            userRole={userRole}
            token={token}
            logout={logout}
          />
          <div className={`mdl-layout__content ${styles.content}`}>
            <div className={`mdl-grid ${styles.container}`}>
              <div className={`mdl-cell mdl-cell--12-col mdl-cell--16-col--tablet mdl-shadow--4dp ${styles.container}`}>
                <div className={`mdl-grid ${styles.header}`}>
                  <div className={'mdl-cell mdl-cell--12-col mdl-cell--16-col--tablet'}>
                    <h3 className={styles.title}>
                      {formatMessage(globalMessages.imageManager)}
                    </h3>
                  </div>
                  <div className={`mdl-cell mdl-cell--12-col mdl-cell--16-col--tablet ${styles.control}`}>
                    <div className={`mdl-textfield mdl-js-textfield ${styles.search}`}>
                      <i className={`material-icons ${styles.magnifier}`}>
                        search
                      </i>
                      <input
                        className={`mdl-textfield__input ${styles.input}`}
                        type={'text'}
                        name={'search-image'}
                        onChange={(event) => this.setState({ search: event.target.value })}
                      />
                    </div>
                    <span className={styles.label}>
                      {formatMessage(globalMessages.searchBy)}
                    </span>
                    <div className={styles.selection}>
                      <label className={`mdl-radio mdl-js-radio ${styles.radio}`} htmlFor={'filter-title'}>
                        <input
                          className={`mdl-radio__button ${styles.unselect}`}
                          type={'radio'}
                          name={'filter-option'}
                          value={'title'}
                          id={'filter-title'}
                          onClick={(event) => this.setState({ filter: event.target.value })}
                          defaultChecked
                        />
                        <span className={`mdl-radio__label ${styles.unselect}`}>
                          {formatMessage(globalMessages.title)}
                        </span>
                      </label>
                      <label className={`mdl-radio mdl-js-radio ${styles.radio}`} htmlFor={'filter-group'}>
                        <input
                          className={`mdl-radio__button ${styles.unselect}`}
                          type={'radio'}
                          name={'filter-option'}
                          value={'group'}
                          id={'filter-group'}
                          onClick={(event) => this.setState({ filter: event.target.value })}
                        />
                        <span className={`mdl-radio__label ${styles.unselect}`}>
                          {formatMessage(globalMessages.group)}
                        </span>
                      </label>
                      <label className={`mdl-radio mdl-js-radio ${styles.radio}`} htmlFor={'filter-tags'}>
                        <input
                          className={`mdl-radio__button ${styles.unselect}`}
                          type={'radio'}
                          name={'filter-option'}
                          value={'tags'}
                          id={'filter-tags'}
                          onClick={(event) => this.setState({ filter: event.target.value })}
                        />
                        <span className={`mdl-radio__label ${styles.unselect}`}>
                          {formatMessage(globalMessages.tags)}
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <div className={`mdl-grid mdl-grid--no-spacing ${styles.middle}`}>
                  <ImageTable
                    search={this.state.search}
                    filter={this.state.filter}
                  />
                </div>
                <div className={`mdl-grid ${styles.bottom}`}>
                  <button
                    className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}
                    onClick={(event) => this.setState({ dialog: true })}
                  >
                    {formatMessage(globalMessages.uploadImage)}
                  </button>
                </div>
              </div>
            </div>
            <ImagePicker
              isOpen={this.state.dialog}
              groups={groups}
              tags={tags}
              onUpload={(event, data) => this.setImage(event, data)}
              onCancel={(event, data) => this.setState({ dialog: false })}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userId: state.User.currentUser.id,
    userRole: state.User.currentUser.role,
    userLocale: state.User.currentUser.locale,
    token: state.User.currentUser.token,
    groups: state.User.groups,
    tags: state.User.tags
  };
}

export default connect(mapStateToProps, { logout, getGroups, getTags, getImageUploadUrl, uploadFile, getImages, saveFileToDB })(injectIntl(ImageManager));
