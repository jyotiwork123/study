import { STATE_LOCAL_NAME } from './user/state/constants';

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem(STATE_LOCAL_NAME);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(STATE_LOCAL_NAME, serializedState);
  } catch (err) {
    console.error(err);
  }
};
