import {
  filterByLanguage,
  shuffleArray,
  replaceWithBigger,
  getAudioUrl,
  mergeSettingsOptions,
  capitalise,
  setExerciseDelay,
  setAudioDelay,
  setTextCase,
  formatArrOfStr,
  transformSpecSymb
} from '../../../app/exercises/helpers';

import { exerciseSixSettings } from '../../../app/exercises/data';

describe('Exercises helpers ', () => {
  describe('filterByLanguage ', () => {
    it('should filter exercises by language domain', () => {
      const exercises = [
        {
          settings: {
            languageDomain: ['vocabulary', 'reading']
          }
        },
        {
          settings: {
            languageDomain: ['communication', 'grammar']
          }
        },
        {
          settings: {
            languageDomain: ['pronunciation', 'text']
          }
        },
        {
          settings: {
            languageDomain: ['writing', 'communication']
          }
        },
        {
          settings: {
            languageDomain: ['vocabulary', 'reading', 'communication', 'grammar', 'pronunciation', 'text', 'writing']
          }
        }
      ];
      const testCases = [
        { vocabulary: true },
        { pronunciation: true, vocabulary: true },
        { pronunciation: false, vocabulary: false, communication: true },
        { pronunciation: false, vocabulary: false, communication: false }
      ];

      const expectedResults = [
        [
          {
            settings: {
              languageDomain: ['vocabulary', 'reading']
            }
          },
          {
            settings: {
              languageDomain: ['vocabulary', 'reading', 'communication', 'grammar', 'pronunciation', 'text', 'writing']
            }
          }
        ],
        [
          {
            settings: {
              languageDomain: ['vocabulary', 'reading']
            }
          },
          {
            settings: {
              languageDomain: ['pronunciation', 'text']
            }
          },
          {
            settings: {
              languageDomain: ['vocabulary', 'reading', 'communication', 'grammar', 'pronunciation', 'text', 'writing']
            }
          }
        ],
        [
          {
            settings: {
              languageDomain: ['communication', 'grammar']
            }
          },
          {
            settings: {
              languageDomain: ['writing', 'communication']
            }
          },
          {
            settings: {
              languageDomain: ['vocabulary', 'reading', 'communication', 'grammar', 'pronunciation', 'text', 'writing']
            }
          }
        ],
        exercises
      ];
      for (let i = 0; i < testCases.length; i++) {
        expect(filterByLanguage(exercises, testCases[i])).toEqual(expectedResults[i]);
      }
    });
  });

  describe('shuffleArray ', () => {
    it('should shuffle array', () => {
      const testCases = [
        [1, 2, 3, 4, 5],
        [{ one: 1 }, { two: 2 }, { three: 3 }]
      ];
      for (let i = 0; i < testCases.length; i++) {
        expect(shuffleArray(testCases[i])).not.toEqual(testCases[i]);
        testCases[i].forEach((el) => {
          expect(shuffleArray(testCases[i])).toContainEqual(el);
        });
      }
    });
  });

  describe('replaceWithBigger ', () => {
    it('should replace links to smaller images with links to bigger images', () => {
      const link = 'https://s3.amazonaws.com/aphasia/images/small/boiled_egg_small.jpg';
      const expectedResult = 'https://s3.amazonaws.com/aphasia/images/boiled_egg.jpg';

      expect(replaceWithBigger(link)).toBe(expectedResult);
      expect(replaceWithBigger('')).toBeNull();
      expect(replaceWithBigger(JSON.stringify({ url: link }))).toBe(expectedResult);
      expect(replaceWithBigger(JSON.stringify({ url: null }))).toBe('');
    });
  });

  describe('getAudioUrl ', () => {
    it('should form audio link', () => {
      const testCase = JSON.stringify({ locale: 'et', file: 'arbuus' });
      const expectedResult = 'https://dvm38m4gcon5n.cloudfront.net/speech/et/arbuus';

      expect(getAudioUrl(testCase)).toBe(expectedResult);
    });
  });

  describe('mergeSettingsOptions ', () => {
    it('should create localised settings object', () => {
      const settings = {
        exerciseName: 'The order of words (template 6)',
        shortDescription: 'User has to put the words in the correct order. ',
        delay: 'Normal',
        access: 1,
        isValidated: true,
        recommendedLength: 10,
        letters: 'As is',
        shortCommand: 'Order the words',
        exerciseDescription: 'Put the words in the correct order ',
        usefullness: 'This exercise is useful by training the language automation.',
        benefits: 'This exercise trains the automatism of language. ',
        type: 'automatism',
        languageDomain: ['vocabulary', 'communication', 'reading']
      };

      const expectedAnswer = [
        {
          label: 'Recommended length of practice',
          options: ['1 minute',
            '2 minutes',
            '3 minutes',
            '4 minutes',
            '5 minutes',
            '6 minutes',
            '7 minutes',
            '8 minutes',
            '9 minutes',
            '10 minutes',
            '11 minutes',
            '12 minutes',
            '13 minutes',
            '14 minutes',
            '15 minutes'],
          value: '10 minutes'
        },
        {
          label: 'Next exercise loading speed',
          options: ['Slow', 'Normal', 'Fast'],
          value: 'Normal'
        },
        {
          label: 'Letters',
          options: ['Capital letters', 'Uppercase', 'As is'],
          value: 'As is'
        },
        {
          label: 'Automatic validation',
          options: ['Yes', 'No'],
          value: 'Yes'
        }
      ];

      expect(mergeSettingsOptions(settings, exerciseSixSettings.en)).toEqual(expectedAnswer);
    });
  });

  describe('capitalise ', () => {
    it('should capitalise a string', () => {
      expect(capitalise('abcd')).toBe('Abcd');
      expect(capitalise('ABCD')).toBe('Abcd');
      expect(capitalise('a bcd')).toBe('A bcd');
      expect(capitalise(' abcd')).toBe('Abcd');
      expect(capitalise(' a bcd')).toBe('A bcd');
    });
  });

  describe('setExerciseDelay ', () => {
    it('should return amount of milliseconds', () => {
      expect(setExerciseDelay('slow')).toBe(2000);
      expect(setExerciseDelay('aeglane')).toBe(2000);
      expect(setExerciseDelay('normal')).toBe(1000);
      expect(setExerciseDelay('keskmine')).toBe(1000);
      expect(setExerciseDelay('fast')).toBe(0);
      expect(setExerciseDelay('kiire')).toBe(0);
    });
  });

  describe('setAudioDelay ', () => {
    it('should return amount of milliseconds', () => {
      expect(setAudioDelay('slow')).toBe(500);
      expect(setAudioDelay('aeglane')).toBe(500);
      expect(setAudioDelay('normal')).toBe(200);
      expect(setAudioDelay('keskmine')).toBe(200);
      expect(setAudioDelay('fast')).toBe(100);
      expect(setAudioDelay('kiire')).toBe(100);
    });
  });

  describe('setTextCase ', () => {
    it('should return capitalisation value', () => {
      expect(setTextCase('capital letters')).toBe('capitalize');
      expect(setTextCase('algab suure tähega')).toBe('capitalize');
      expect(setTextCase('uppercase')).toBe('uppercase');
      expect(setTextCase('trükitähed')).toBe('uppercase');
      expect(setTextCase('fast')).toBe('none');
      expect(setTextCase('kiire')).toBe('none');
    });
  });

  describe('formatArrOfStr ', () => {
    it('should return capitalised or uppercase array of strings', () => {
      const arr = ['abc', 'Abc', 'aBC', 'ABC', 'a bc', null];
      const expectedCapitalise = ['Abc', 'Abc', 'Abc', 'Abc', 'A bc', ''];
      const expectedUppercase = ['ABC', 'ABC', 'ABC', 'ABC', 'A BC', ''];

      expect(formatArrOfStr(arr, 'capitalize')).toEqual(expectedCapitalise);
      expect(formatArrOfStr(arr, 'uppercase')).toEqual(expectedUppercase);
    });
  });

  describe('transformSpecSymb ', () => {
    it('should substitute Estonian symbols with English', () => {
      const str = 'äüõö ÄÜÕÖ';
      const expectedStr = 'auoo AUOO';

      expect(transformSpecSymb(str)).toBe(expectedStr);
    });
  });
});
