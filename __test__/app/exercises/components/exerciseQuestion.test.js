import TestUtils from 'react-addons-test-utils';
import React from 'react';
import { ExerciseQuestion } from '../../../../app/exercises/components/exerciseQuestion';
import { shallow } from 'enzyme';


describe('ExerciseQuestion ', () => {
  it('should render question text', () => {
    const questionText = 'test text';

    const wrapper = shallow(
      <ExerciseQuestion questionText={questionText} />
    );

    expect(wrapper.find('h2').text()).toEqual(questionText);
  });

  it('should contain heading', () => {
    const questionText = 'test text';

    const wrapper = shallow(
      <ExerciseQuestion questionText={questionText} />
    );

    expect(wrapper.contains(<h2>{questionText}</h2>)).toBeTruthy();
  });

  /*it('(snapshot) should render a spinner', () => {
    const tree = renderer.create(
      <ExerciseQuestion questionText="test" />
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });*/
});
