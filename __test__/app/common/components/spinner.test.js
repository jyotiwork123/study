import TestUtils from 'react-addons-test-utils';
import React from 'react';
import renderer from 'react-test-renderer';
import Spinner from '../../../../app/common/components/spinner';

window.componentHandler = {
  upgradeDom() {}
};
const expectedClass = 'my-class';

describe('spinner ', () => {
  it('should have declared class', () => {
    const spinner = TestUtils.renderIntoDocument(
      <Spinner spinnerClass={expectedClass} />
    );
    console.log(TestUtils.isCompositeComponent(spinner));
    const wrapper = TestUtils.findRenderedDOMComponentWithClass(spinner, 'spinner-wrapper');
    expect(wrapper.classList).toContain(expectedClass);
  });

  it('(snapshot) should render a spinner', () => {
    const tree = renderer.create(
      <Spinner spinnerClass={expectedClass} />
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
