# Development

git clone this repository

    npm install
    npm run start

Open http://localhost:8080/

# Deployment

Write access to slt2.cognuse.com S3 must be configured first and aws-cli installed.

    npm run deploy

# Localisation

Information for localisation is stored in

* i18n/translations.json
* app/exercises/data.js

## CI/CD

* Test deploy is triggered on every commit.
* Production deploy is triggered after a new [tag](https://gitlab.com/cognuse/slt/slt-ui/-/tags) has been created.

Shortcut to [environments](https://gitlab.com/cognuse/slt/slt-ui/-/environments)

